<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $article_category_id
 * @property string $title
 * @property string $slug
 * @property string $keywords
 * @property string $subcontent
 * @property string $content
 * @property string $image
 * @property string $image_dir
 * @property integer $views_count
 * @property integer $likes_count
 * @property integer $comments_count
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ArticleCategory $articleCategory
 * @property ArticleComment[] $articleComments
 */
class Article extends BaseModel
{

    /**
     * @var boolean     like_status     untuk status like artikel oleh para participant
     */
    public $like_status = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_category_id' => 'Article Category ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'keywords' => 'Keywords',
            'subcontent' => 'Subcontent',
            'content' => 'Content',
            'image' => 'Image',
            'image_dir' => 'Image Dir',
            'shares_count' => 'Shares Count',
            'views_count' => 'Views Count',
            'likes_count' => 'Likes Count',
            'comments_count' => 'Comments Count',
            'row_status' => 'Row Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }


    public static function search($keywords)
    {
        $query = static::get()->andWhere(['like', 'title', $keywords])->joinWith(['category']);
        return $query;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'article_category_id']);
    }

    public function getShareLink()
    {
        return BASE_URL . 'article/' . $this->category->slug . '/' . $this->slug;
    }
}
