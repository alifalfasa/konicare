<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_activity".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $participant_id
 * @property string $type
 * @property string $action
 * @property integer $point
 * @property integer $created_at
 */
class LogActivity extends \common\models\BaseModel
{

    public $updated_at;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_activity';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'type', 'action', 'participant_id'], 'required'],
            [['post_id', 'point', 'created_at', 'participant_id'], 'integer'],
            [['type', 'action'], 'string', 'max' => 45],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'type' => 'Type',
            'action' => 'Action',
            'point' => 'Point',
            'created_at' => 'Created At',
            'participant_id' => 'Created By',
        ];
    }

    public static function getTotalParticipantPoint( $participant_id )
    {
        $query = static::find()
            ->andWhere(['participant_id' => $participant_id])
            ->sum('point');
        return $query;
    }

    /**
     * Check acitivity participants
     *
     * @param      string   $type            Tipe Acitivity
     * @param      string   $action          Command action participant
     * @param      int      $participant_id  The participant identifier
     * @param      int      $post_id         Value dari primary key pada setiap table
     *
     * @return     object
     */
    public static function check( $type, $action, $post_id, $participant_id )
    {
        if($action === "read"){
            $today = strtotime(date('Y-m-d').' 00:00:00.000');
            $tomorrow = strtotime(date('Y-m-d', strtotime('+1 day')).' 00:00:00.000');
            $query = static::find()
            ->where(['between', 'created_at', $today, $tomorrow ])
            ->andWhere([
                'type' => $type,
                'participant_id' => $participant_id,
                'post_id' => $post_id,
                'action' => $action
            ]);
        } else {
            $query = static::find()->andWhere([
                'type' => $type,
                'participant_id' => $participant_id,
                'post_id' => $post_id,
                'action' => $action
            ]);
        }

        return $query;
    } 

    /**
     * Check acitivity participants
     * 
     * Note:
     * Point maksimal adalah 100 perhari
     * 
     * @param      string   $type            Tipe Acitivity
     * @param      string   $action          Command action participant
     * @param      int      $participant_id  The participant identifier
     * @param      int      $post_id         Value dari primary key pada setiap table
     * @param      int      $point           Point yang akan diberikan
     *
     * @return     boolean|array
     */
    public static function store($type, $action, $post_id, $participant_id, $point )
    {

        if ( empty($participant_id) ) return false;

        $checkMaxPoint = static::find()
            ->andWhere(['=', 'FROM_UNIXTIME(created_at,\'%Y-%m-%d\')', date('Y-m-d')])
            ->andWhere(['participant_id' => $participant_id])
            ->andWhere(['<>', 'type', 'product-redeem'])
            ->sum('point');

        // Check point hari ini tidak lebih dari 100 kecuali type product-redeem
        if ( $checkMaxPoint >= 100 and $type != 'product-redeem' )
        { 
            return [ 'status' => false, 'message' => 'Point anda sudah melebihi 100 untuk hari ini' ];
        }

        // var_dump($checkMaxPoint);exit;
        $model = new LogActivity();
        $model->type           = $type;
        $model->action         = $action;
        $model->post_id        = $post_id;
        $model->participant_id = $participant_id;
        $model->point          = $point;


        if ( $model->save() )
        {
            
            $participant = Participant::findOne($participant_id);
            if ( ($checkMaxPoint + $point) > 100 )
            {
                /**
                 * Kurangi point user, disebabkan point melebihi limit perhari ini
                 * Misal:
                 * baca 9 artikel dapat 90 (9 x 10)
                 * lalu ikut imunisasi, dapetlah 60
                 * maka
                 * 90 + 60 = 150
                 * 150 itu melebihi point pada hari ini
                 * maka harus di kurangi sampai 100
                 * jadi point user dikurangi 50 point,
                 * lalu datanya disimpan kedatabase dengan 'action' [decrease] 
                 */
                $participant->points = $participant->points + 
                    (100 - $checkMaxPoint)
                ;
                $point = 100 - ($checkMaxPoint + $point);
                $historyDecrease = new LogActivity();
                $historyDecrease->type           = 'points';
                $historyDecrease->action         = 'decrease';
                $historyDecrease->post_id        = 0;
                $historyDecrease->participant_id = $participant_id;
                $historyDecrease->point          = $point;

                $historyDecrease->save();
            } else {
                $participant->points = $participant->points + $point;
            }
            $participant->save();

            return true;
        }
        return [ 'error_message' => static::getError($model) ];

    }


    public static function remove($id)
    {
        $model = LogActivity::findOne( $id );
        if ( $model->delete() )
        {
            $participant = Participant::findOne($model->participant_id);
            $participant->points = $participant->points - $model->point;
            $participant->save();            
            
            return true;
        }

        return [ 'error_message' => static::getError($model) ];

    }
}
