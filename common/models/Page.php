<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $page_type
 * @property string $title
 * @property string $slug
 * @property string $subcontent
 * @property string $content
 * @property string $image
 * @property string $image_dir
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property PageTag[] $pageTags
 */
class Page extends \common\models\BaseModel
{


    public static function getData($type)
    {
        $model = static::fetch()->andWhere(['page_type' => $type])->one();
        if ( empty( $model ) )
        {
            throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);
        }
        return $model;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageTags()
    {
        return $this->hasMany(PageTag::className(), ['page_id' => 'id']);
    }
}
