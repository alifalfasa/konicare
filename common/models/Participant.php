<?php

namespace common\models;

use Yii;

use common\models\LogActivity;

/**
 * This is the model class for table "participant".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $usertype
 * @property string $fbuserid
 * @property string $fbusername
 * @property string $fbtoken
 * @property string $email
 * @property string $fullname
 * @property string $gender
 * @property string $birthdate
 * @property string $notelp
 * @property string $nohp
 * @property string $address
 * @property integer $regency_id
 * @property integer $province_id
 * @property string $country
 * @property string $zip
 * @property string $avatarimg
 * @property integer $user_status
 * @property integer $is_notif
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ForumPost[] $forumPosts
 * @property ForumTopic[] $forumTopics
 */
class Participant extends \common\models\BaseModel implements \yii\web\IdentityInterface
{


    public static $uploadFile = [
        'avatarimg' => [
            'path' => 'participant-avatarimg',
            'using' => 'manually',
            'resize' => [
                [
                    'prefix' => 'thumb_',
                    'size' => [50,50],
                ],
                [
                    'prefix' => 'normal_',
                    'size' => [200,200],
                ]
            ]
        ]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'usertype', 'email', 'fullname', 'birthdate', 'nohp', 'address', 'country'], 'required'],
            [['id', 'fbuserid', 'regency_id', 'province_id', 'user_status', 'is_notif', 'points', 'row_status', 'created_at', 'created_by', 'updated_at', 'updated_by', 'zip'], 'integer'],
            [['birthdate'], 'safe'],
            [['fullname'], 'alphabetsValidation'],
            [['username'], 'characterValidation'],
            [['username','email'], 'uniquenessValidation','on'=>array('insert')],
            [['username', 'email'], 'string', 'max' => 100],
            ['email', 'email'],
            [['password'], 'string', 'max' => 120],
            [['usertype'], 'string', 'max' => 4],
            [['fbusername', 'fullname', 'country', 'zip', 'avatarimg'], 'string', 'max' => 64],
            [['avatarimg'], 'fileImageValidation'],
            [['fbtoken'], 'string', 'max' => 320],
            [['avatarimg'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 1024 * 1024 * 2, 'tooBig' => 'The "{file}" {attribute} is too big. Its size cannot exceed 2MB'],
            // [['avatarimg'], 'default', 'bundakonicare-no_avatar.jpg'],
            // [['points'], 'default', 0],
            [['notelp', 'nohp'], 'string', 'max' => 16],
            [['address'], 'string', 'max' => 128],
        ];
    }


    public function fileImageValidation($attribute, $params)
    {
        if ( empty($_FILES['Participant']) ) return true;

        $file = $_FILES['Participant'];
        $imageSize = $file['size']['avatarimg'];

        if ( empty($imageSize) ) return true;

        // Check image file
        if ( $file['type']['avatarimg'] != 'image/jpeg' && 
             $file['type']['avatarimg'] != 'image/jpg'  && 
             $file['type']['avatarimg'] != 'image/png' ) {

            $this->addError( $attribute, $this->getAttributeLabel($attribute) . ' File image harus jpg, jpeg atau png' );
            
            return false;

        }

        // Check file size, file size allowed is 2MB
        if ( $imageSize > 2097152 )
        {
            $this->addError( $attribute, $this->getAttributeLabel($attribute) . ' File size maximal adalah 2MB' );
            
            return false;
        }

        return true;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'usertype' => 'Usertype',
            'fbuserid' => 'Fbuserid',
            'fbusername' => 'Fbusername',
            'fbtoken' => 'Fbtoken',
            'email' => 'Email',
            'fullname' => 'Fullname',
            'gender' => 'Gender',
            'birthdate' => 'Birthdate',
            'notelp' => 'Notelp',
            'nohp' => 'Nohp',
            'address' => 'Address',
            'regency_id' => 'Regency ID',
            'province_id' => 'Province ID',
            'country' => 'Country',
            'zip' => 'Zip',
            'avatarimg' => 'Avatarimg',
            'user_status' => 'User Status',
            'is_notif' => 'Is Notif',
            'row_status' => 'Row Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $participant = new Participant();
        $participant->username  = $this->username;
        $participant->usertype  = $this->usertype;
        $participant->email     = $this->email;
        $participant->gender    = $this->gender;
        $participant->fullname  = $this->fullname;
        $participant->birthdate = $this->birthdate;
        $participant->notelp    = $this->notelp;
        $participant->nohp      = $this->nohp;
        $participant->address   = $this->address;
        $participant->regency_id  = $this->regency_id;
        $participant->province_id = $this->province_id;
        $participant->country     = $this->country;
        $participant->zip         = $this->zip;
        $participant->fbuserid    = isset($this->fbuserid) ? $this->fbuserid : 0;
        $participant->row_status  = isset($this->row_status) ? $this->row_status : 1 ;

        $participant->setPassword($this->password);
        $participant->generateAuthKey();
        if ( $participant->save() )
        {
            $participant->id = $participant->id;
            return $participant;
        } else {
            return false;
        }
    }


    /**
     * Sets the birthdate.
     *
     * @param      string  $value  dimana formatnya adalah (dd-mm-yyyy)
     *
     * @return     string   Output birthdate (yyyy-mm-dd)
     */
    public function setBirthdate( $value )
    {
        $birthdate = explode('-', $value);
        return $birthdate[2] . '-' . $birthdate[1] . '-' . $birthdate[0];
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'row_status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'row_status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'row_status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegency()
    {
        return $this->hasOne(Regency::className(), ['id' => 'regency_id']);
    }


    public function getAvatar($version = '')
    {
        if ( $this->avatarimg == 'bundakonicare-no_avatar.jpg' )
        {
            return BASE_URL . 'img/bundakonicare-no_avatar.jpg';
        } else {
            return BASE_URL . 'media/participant-avatarimg/' . $this->id . '/' . $version . $this->avatarimg;
        }
    }


    public function updatePointFromActivity()
    {
        $participantAcitivityPoint = LogActivity::getTotalParticipantPoint( $this->id );
        if ( $this->points != $participantAcitivityPoint )
        {
            $this->points = $participantAcitivityPoint;
            if ( $this->save() )
            {
                return $participantAcitivityPoint;
            } else {
                return $this->getErrors();
            }
        } else {
            return $participantAcitivityPoint;
        }
    }
}
