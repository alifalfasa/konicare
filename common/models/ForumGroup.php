<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "forum_group".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property integer $seq
 * @property integer $row_status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Forum[] $forums
 */
class ForumGroup extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_group';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'icon' => 'Icon',
            'seq' => 'Seq',
            'row_status' => 'Row Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForums()
    {
        return $this->hasMany(Forum::className(), ['forum_group_id' => 'id']);
    }
}
