<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "loc_regencies".
 *
 * @property string $id
 * @property string $province_id
 * @property string $name
 *
 * @property LocDistricts[] $locDistricts
 * @property LocProvinces $province
 */
class Regency extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loc_regencies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'province_id', 'name'], 'required'],
            [['id'], 'string', 'max' => 4],
            [['province_id'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 255],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocProvinces::className(), 'targetAttribute' => ['province_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'province_id' => 'Province ID',
            'name' => 'Name',
        ];
    }

    public static function getData($provinceId)
    {
        return static::find()
            ->where(['=', 'province_id', $provinceId])
            ->orderBy('name')
            ->asArray()
            ->all();
    }

    public static function detail($regencyId)
    {
        return static::find()->where(['=', 'id', $regencyId])->asArray()->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocDistricts()
    {
        return $this->hasMany(LocDistricts::className(), ['regency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(LocProvinces::className(), ['id' => 'province_id']);
    }
}
