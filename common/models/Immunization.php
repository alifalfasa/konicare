<?php

namespace common\models;

use Yii;

use common\models\ImmunizationSchedule;

/**
 * This is the model class for table "immunization".
 *
 * @property integer $id
 * @property integer $participant_id
 * @property string $children_name
 * @property string $birthdate
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Participant $participant
 */
class Immunization extends \common\models\BaseModel
{

    public $gender_options = [ 0 => 'Pria', 1 => 'Wanita' ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'immunization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['participant_id', 'children_name', 'gender', 'birthdate'], 'required'],
            [['participant_id', 'gender', 'row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            // [['children_name'], 'checkDuplicateName'],
            [['birthdate'], 'safe'],
            [['children_name'], 'string', 'max' => 50],
            [['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Participant::className(), 'targetAttribute' => ['participant_id' => 'id']],
        ];
    }


    public function checkDuplicateName( $attribute, $params)
    {
        $query = static::find()
            ->where( [ 'children_name' => $this->children_name ] )
            ->andWhere( [ '=', 'participant_id', $this->participant_id ] )
            ->count();
        if ($query) {
            $this->addError($attribute, 'Your children has been stored before. Please change your children name');
        }
    }


    public static function getChildrens( $participant_id )
    {
        return Immunization::fetch()
            ->andWhere(['=', 'participant_id', $participant_id])
            ->orderBy('birthdate ASC')
            ->all();
    }


    public static function getChildrenCount( $participant_id )
    {
        return Immunization::fetch()->andWhere(['=', 'participant_id', $participant_id])->count();
    }


    public function getGender()
    {
        $flag = $this->gender;
        return $this->gender_options[ $flag ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(Participant::className(), ['id' => 'participant_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchedules()
    {
        return $this->hasMany(ImmunizationSchedule::className(), ['immunization_id' => 'id']);
    }    
}
