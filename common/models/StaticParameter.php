<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "static_parameter".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class StaticParameter extends \common\models\BaseModel
{
    
    public static function getValue( $name )
    {
        return static::fetchOne([ 'name' => $name ])->value;
    }
}
