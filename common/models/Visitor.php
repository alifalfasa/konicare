<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "visitor".
 *
 * @property integer $id
 * @property string $ip
 * @property string $date
 * @property integer $previous_visit
 * @property integer $row_status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class Visitor extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visitor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip', 'date', 'previous_visit'], 'required'],
            [['date'], 'safe'],
            [['previous_visit', 'row_status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['ip', 'via'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'date' => 'Date',
            'previous_visit' => 'Previous Visit',
            'row_status' => 'Row Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
