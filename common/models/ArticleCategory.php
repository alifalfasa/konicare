<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $keywords
 * @property string $description
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Article[] $articles
 */
class ArticleCategory extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_category';
    }

    public static function getAll()
    {
        $datas = static::fetch()->select('id,name,description')->asArray()->all();
        // $result = [];
        // foreach( $data as $datas )
        // {
        //     $result[] = ['id']
        // }
        return $datas;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['article_category_id' => 'id']);
    }
}
