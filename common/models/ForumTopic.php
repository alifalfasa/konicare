<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "forum_topic".
 *
 * @property integer $id
 * @property integer $forum_id
 * @property integer $participant_id
 * @property string $name
 * @property integer $post_count
 * @property integer $view_count
 * @property integer $is_sticky
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ForumPost[] $forumPosts
 * @property Forum $forum
 * @property Participant $participant
 */
class ForumTopic extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['forum_id', 'participant_id', 'name', 'slug'], 'required'],
            [['id', 'forum_id', 'participant_id', 'post_count', 'view_count', 'is_sticky', 'row_status', 'created_at', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 180],
            ['name', 'uniquenessValidation'],
            [['forum_id'], 'exist', 'skipOnError' => true, 'targetClass' => Forum::className(), 'targetAttribute' => ['forum_id' => 'id']],
            [['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Participant::className(), 'targetAttribute' => ['participant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'forum_id' => 'Forum ID',
            'participant_id' => 'Participant ID',
            'name' => 'Topic name',
            'post_count' => 'Post Count',
            'view_count' => 'View Count',
            'is_sticky' => 'Is Sticky',
            'row_status' => 'Row Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }


    public static function search($keywords)
    {
        $query = static::get()
            ->andWhere(['LIKE', 'forum_topic.name', $keywords])
            ->joinWith([
                'participant' => function($query){
                    $query->select('id, username');
                }, 
                'forum' => function($query){
                    $query->select('id, name, forum_group_id, slug')->with('group');
                }, 'post' =>function($query){
                    $query->select('id, content');
                }
            ])
            ->orderBy('forum_topic.created_at DESC, forum_topic.view_count DESC');
        return $query;
    }


    public static function detail($slug)
    {
        return static::get()
            ->andWhere(['slug' => $slug])
            ->with(['forum' => function($query){ 
                $query->with('group'); 
            }]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(ForumPost::className(), ['id' => 'post_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForumPosts()
    {
        return $this->hasMany(ForumPost::className(), ['topic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForum()
    {
        return $this->hasOne(Forum::className(), ['id' => 'forum_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(Participant::className(), ['id' => 'participant_id']);
    }
}
