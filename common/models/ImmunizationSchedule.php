<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "immunization_schedule".
 *
 * @property integer $id
 * @property integer $immunization_id
 * @property integer $immunization_category_id
 * @property string $date
 * @property string $clinic_name
 * @property string $note
 * @property integer $had_done
 * @property integer $row_status
 * @property integer $created_at
 *
 * @property Immunization $immunization
 * @property ImmunizationCategory $immunizationCategory
 */
class ImmunizationSchedule extends \common\models\BaseModel
{

    public $birthdate;

    private $hadDoneOptions = [ 1 => 'Yes', 0 => 'No'];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'immunization_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['immunization_id', 'immunization_category_id', 'date', 'had_done'], 'required'],
            [['immunization_id', 'immunization_category_id', 'had_done', 'row_status', 'created_at'], 'integer'],
            [['date'], 'safe'],
            [['note'], 'string'],
            [['clinic_name'], 'string', 'max' => 55],
            [['immunization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Immunization::className(), 'targetAttribute' => ['immunization_id' => 'id']],
            [['immunization_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ImmunizationCategory::className(), 'targetAttribute' => ['immunization_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'immunization_id' => 'Immunization ID',
            'immunization_category_id' => 'Immunization Category ID',
            'date' => 'Date',
            'clinic_name' => 'Clinic Name',
            'note' => 'Note',
            'had_done' => 'Had Done',
            'row_status' => 'Row Status',
            'created_at' => 'Created At',
        ];
    }


    public function getHadDone()
    {
        $flag = $this->had_done;
        return $this->hadDoneOptions[ $flag ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImmunization()
    {
        return $this->hasOne(Immunization::className(), ['id' => 'immunization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImmunizationCategory()
    {
        return $this->hasOne(ImmunizationCategory::className(), ['id' => 'immunization_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(Participant::className(), ['id' => 'participant_id'])->via('immunization');
    }


    public function setHepatitis()
    {
        $birthdate = $this->birthdate;
        $category  = 1;

        $date  = new \DateTime( $birthdate );
        $date  = $date->modify( '+0 day' )->format( 'Y-m-d');
        $query = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+2 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+3 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+4 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    
    public function setPolio()
    {
        $birthdate = $this->birthdate;
        $category  = 2;

        // $date  = new \DateTime( $birthdate );
        // $date  = $date->modify( '+0 day' )->format( 'Y-m-d');
        // $query = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+2 month' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+3 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+4 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+4 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+18 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setBCG()
    {
        $birthdate = $this->birthdate;
        $category  = 3;

        $date  = new \DateTime( $birthdate );
        $date  = $date->modify( '+1 day' )->format( 'Y-m-d');
        $query = $this->buildQuery( $category, $date );

        return $query;

    }

    public function setDTP()
    {
        $birthdate = $this->birthdate;
        $category  = 4;

        $date  = new \DateTime( $birthdate );
        $date  = $date->modify( '+2 month' )->format( 'Y-m-d');
        $query = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+3 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+4 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+18 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+5 year' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setHib()
    {
        $birthdate = $this->birthdate;
        $category  = 5;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+2 month' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+3 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+4 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+15 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setPCV()
    {
        $birthdate = $this->birthdate;
        $category  = 6;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+2 month' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+4 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+6 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+12 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setRotavirus()
    {
        $birthdate = $this->birthdate;
        $category  = 13;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+2 month' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+4 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+6 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setInfluenza()
    {
        $birthdate = $this->birthdate;
        $category  = 7;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+6 month' );
        $query  = $this->buildQuery( $category, $date->format( 'Y-m-d') );

        $date   = $date->modify( '+1 year' );
        $query .= $this->buildQuery( $category, $date->format( 'Y-m-d') );

        $date   = $date->modify( '+1 year' );
        $query .= $this->buildQuery( $category, $date->format( 'Y-m-d') );

        $date   = $date->modify( '+1 year' );
        $query .= $this->buildQuery( $category, $date->format( 'Y-m-d') );

        $date   = $date->modify( '+1 year' );
        $query .= $this->buildQuery( $category, $date->format( 'Y-m-d') );

        $date   = $date->modify( '+1 year' );
        $query .= $this->buildQuery( $category, $date->format( 'Y-m-d') );

        return $query;

    }

    public function setCampak()
    {
        $birthdate = $this->birthdate;
        $category  = 8;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+9 month' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+18 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+6 year' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setMMR()
    {
        $birthdate = $this->birthdate;
        $category  = 9;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+15 month' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+5 year' );
        $query .= $this->buildQuery( $category, $date->format( 'Y-m-d') );

        // $date   = $date->modify( '+1 year' );
        // $query .= $this->buildQuery( $category, $date->format( 'Y-m-d') );

        return $query;

    }

    public function setTifold()
    {
        $birthdate = $this->birthdate;
        $category  = 10;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+24 month' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        // $date   = new \DateTime( $birthdate );
        // $date   = $date->modify( '+3 year' )->format( 'Y-m-d');
        // $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+5 year' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+8 year' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setHepatitisA()
    {
        $birthdate = $this->birthdate;
        $category  = 11;

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+2 year' )->format( 'Y-m-d');
        $query  = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+30 month' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    public function setVarisela()
    {
        $birthdate = $this->birthdate;
        $category  = 12;

        $date  = new \DateTime( $birthdate );
        $date  = $date->modify( '+1 year' );
        $query = $this->buildQuery( $category, $date->format( 'Y-m-d') );

        return $query;

    }

    public function setHPV()
    {
        $birthdate = $this->birthdate;
        $category  = 14;

        $date  = new \DateTime( $birthdate );
        $date  = $date->modify( '+10 year' )->format( 'Y-m-d');
        $query = $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+12 year' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        $date   = new \DateTime( $birthdate );
        $date   = $date->modify( '+13 year' )->format( 'Y-m-d');
        $query .= $this->buildQuery( $category, $date );

        return $query;

    }

    protected function buildQuery( $category, $date )
    {
        return "INSERT INTO `immunization_schedule` VALUES (NULL, '$this->immunization_id', '$category', '$date', NULL, NULL, '0', '1', $this->created_at);";
    }
}
