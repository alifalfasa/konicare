<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "forum_post".
 *
 * @property integer $id
 * @property integer $topic_id
 * @property integer $forum_id
 * @property integer $participant_id
 * @property string $content
 * @property string $ip_address
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property ForumTopic $topic
 * @property Forum $forum
 */
class ForumPost extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_id', 'forum_id', 'participant_id', 'ip_address', 'content'], 'required'],
            [['id', 'topic_id', 'forum_id', 'participant_id', 'row_status', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['ip_address'], 'string', 'max' => 18],
            [['topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => ForumTopic::className(), 'targetAttribute' => ['topic_id' => 'id']],
            [['forum_id'], 'exist', 'skipOnError' => true, 'targetClass' => Forum::className(), 'targetAttribute' => ['forum_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'topic_id' => 'Topic ID',
            'forum_id' => 'Forum ID',
            'participant_id' => 'Participant ID',
            'content' => 'Content',
            'ip_address' => 'Ip Address',
            'row_status' => 'Row Status',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    public static function participantPost($participantId)
    {
        $query = static::get()->andWhere(['participant_id' => $participantId]);
        if ( is_integer( $participantId ) )
        {
            return $query->count();
        } else {

            $connection = Yii::$app->db;
            $connection->createCommand('SET sql_mode = ""')->execute();

            $query = $query->select('participant_id, count(id) as count')->groupBy('participant_id')->asArray()->all();
            return $query;
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(ForumTopic::className(), ['id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForum()
    {
        return $this->hasOne(Forum::className(), ['id' => 'forum_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(Participant::className(), ['id' => 'participant_id']);
    }
}
