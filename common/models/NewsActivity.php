<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_activity".
 *
 * @property integer $id
 * @property string $type
 * @property string $title
 * @property string $slug
 * @property string $keywords
 * @property string $subcontent
 * @property string $content
 * @property string $image
 * @property string $image_dir
 * @property integer $views_count
 * @property integer $shares_count
 * @property integer $likes_count
 * @property integer $comments_count
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class NewsActivity extends \common\models\BaseModel
{
    const TYPE_NEWS = 'news';
    const TYPE_ACTIVITY = 'activity';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_activity';
    }


    public static function search($keywords)
    {
        $query = static::get()->andWhere(['LIKE', 'title', $keywords]);
        return $query;
    }

    public static function getType($type)
    {
        $query = static::get()->andWhere(['type' => $type]);
        return $query;
    }
}
