<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "immunization_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ImmunizationSchedule[] $immunizationSchedules
 */
class ImmunizationCategory extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'immunization_category';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImmunizationSchedules()
    {
        return $this->hasMany(ImmunizationSchedule::className(), ['immunization_category_id' => 'id']);
    }
}
