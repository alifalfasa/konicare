<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "post_comment".
 *
 * @property integer $id
 * @property integer $participant_id
 * @property integer $post_id
 * @property integer $type
 * @property string $content
 * @property integer $total_likes
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Participant $participant
 */
class PostComment extends \common\models\BaseModel
{

    public $type_options = [ 'Article', 'News Activity' ];

    const ARTICLE = 0;
    const NEWS_ACTIVITY = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['participant_id', 'post_id', 'type', 'content'], 'required'],
            [['participant_id', 'post_id', 'type', 'total_likes', 'row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['content'], 'string'],
            [['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Participant::className(), 'targetAttribute' => ['participant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'participant_id' => 'Participant ID',
            'post_id' => 'Post ID',
            'type' => 'Type',
            'content' => 'Content',
            'total_likes' => 'Total Likes',
            'row_status' => 'Row Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function getType()
    {
        return $this->type_options[ $this->type ];
    }

    public static function data( $type, $post_id )
    {
        $query = static::fetch()->andWhere( [ 'type' => $type, 'post_id' => $post_id, 'post_comment.row_status' => 1 ] )->joinWith('participant');
        return $query;
    }

    public static function all( $type, $post_id )
    {
        return static::fetch()->andWhere( [ 'type' => $type, 'post_id' => $post_id ] )->all();
    }


    public static function getCount( $type, $post_id )
    {
        return static::fetch()->andWhere( [ 'type' => $type, 'post_id' => $post_id ] )->count();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(Participant::className(), ['id' => 'participant_id']);
    }
}
