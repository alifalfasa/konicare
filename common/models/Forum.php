<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "forum".
 *
 * @property integer $id
 * @property integer $forum_group_id
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property integer $seq
 * @property integer $post_count
 * @property integer $topic_count
 * @property integer $last_postid
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ForumGroup $forumGroup
 * @property ForumPost[] $forumPosts
 * @property ForumTopic[] $forumTopics
 */
class Forum extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'forum_group_id' => 'Forum Group ID',
            'name' => 'Name',
            'description' => 'Description',
            'icon' => 'Icon',
            'seq' => 'Seq',
            'post_count' => 'Post Count',
            'topic_count' => 'Topic Count',
            'last_postid' => 'Last Postid',
            'row_status' => 'Row Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(ForumGroup::className(), ['id' => 'forum_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(ForumPost::className(), ['forum_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(ForumTopic::className(), ['forum_id' => 'id']);
    }
}
