<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "loc_provinces".
 *
 * @property string $id
 * @property string $name
 *
 * @property LocRegencies[] $locRegencies
 */
class Province extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loc_provinces';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocRegencies()
    {
        return $this->hasMany(LocRegencies::className(), ['province_id' => 'id']);
    }
}
