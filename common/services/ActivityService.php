<?php

namespace common\services;

use Yii;

use yii\helpers\StringHelper, yii\helpers\ArrayHelper;

use common\models\Article,
	common\models\PostComment,
	common\models\Participant,
	common\models\LogActivity;

/**
 * Class for activity service.
 * 
 * @method like()
 * @method share()
 * @method comment()
 * @method commentLike()
 * @method read()
 * 
 */
class ActivityService 
{
	
	/**
	 * @var int  		$participantId 		Participant ID 
	 * @var object 		$model 				Model yang digunakan untuk merecord activity
	 * @var string		$prefixName 		Nama prefix berdasrkan nama Table
	 */	
	protected $participantId;
	protected $modelInstance;
	protected $prefixName;

	public function __construct( $model, $participant_id )
	{
		$this->modelInstance = $model;
		$this->prefixName 	 = $model->tableName();
		$this->participantId = $participant_id;
	}


	/**
	 * 
	 * @param 	id 		primary key pada setiap post
	 * 
	 * @uses 	/common/models/LogActivity:check()		Memeriksa data atifitas
	 * @uses 	/common/models/LogActivity:store()  	Menyimpan data aktifitas
	 * @uses 	/common/models/LogActivity:remove() 	Menghapus aktifitas

	 */
	public function like($id)
	{

		$model = $this->modelInstance;

		$log = LogActivity::check( $this->prefixName, 'like', $id, $this->participantId )->one();

		// Periksa log aktifitas dan participant login
		if ( empty($log) && !empty($this->participantId) )
		{

			// nanti dibuka lagi kalau point rewards udah dimulai
			$storeActivity = LogActivity::store( 
				$this->prefixName, 
				'like', 
				$id, 
				$this->participantId, 
				5 
			);
			if ( $storeActivity === true ) 
			{
				
				$model::updateAll([ 'likes_count' => new \yii\db\Expression('likes_count + 1') ], 'id = ' . $id);

				return ['like' => true];

			} else {
				return $storeActivity;
			}

		} elseif ( !empty($log) && !empty($this->participantId) ) {

			// Jika log aktifitasnya ada maka di remove 

			LogActivity::remove( $log->id );

			$model::updateAll([ 'likes_count' => new \yii\db\Expression('likes_count - 1') ], 'id = ' . $id);

			return ['unlike' => true];	

		}

		return false;

	}


	public function unlike($id)
	{

		$model = $this->modelInstance;

		$log = LogActivity::check( $this->prefixName, 'like', $id, $this->participantId )->one();

		// Periksa log aktifitas dan participant login
		if ( !empty($log) && !empty($this->participantId) ) {

			// Jika log aktifitasnya ada maka di remove 

			LogActivity::remove( $log->id );

			$model::updateAll([ 'likes_count' => new \yii\db\Expression('likes_count - 1') ], 'id = ' . $id);

			return ['unlike' => true];	

		}

		return false;
	}

	
	/**
	 * 
	 * @param 	id 		primary key pada setiap post
	 * 
	 * @uses 		/common/models/LogActivity:check()

	 */
	public function share($id)
	{

		$model = $this->modelInstance;

		$log = LogActivity::check( $this->prefixName, 'share', $id, $this->participantId )->one();

		if ( empty($log) && !empty($this->participantId) )
		{
			$model::updateAll([ 'shares_count' => new \yii\db\Expression('shares_count + 1') ], 'id = ' . $id);
			LogActivity::store( $this->prefixName, 'share', $id, $this->participantId, 10 );

			return true;
		}

		return false;

	}

	/**
	 * @uses 		/common/models/LogActivity:check()
	 */
	public function comment($id)
	{ 

		$model = $this->modelInstance;

		$log = LogActivity::check( $this->prefixName, 'comment', $id, $this->participantId )->one();

		if ( empty($log) && !empty($this->participantId) )
		{
			LogActivity::store( $this->prefixName, 'comment', $id, $this->participantId, 5 );
			
		}

		$model::updateAll([ 'comments_count' => new \yii\db\Expression('comments_count + 1') ], 'id = ' . $id);

		return true;

	}
	

	/**
	 * @uses 		/common/models/LogActivity:check()
	 */
	public function read($id)
	{
		$model = $this->modelInstance;

		$log = LogActivity::check( $this->prefixName, 'read', $id, $this->participantId )->one();

		if ( empty($log) && !empty($this->participantId) )
		{

			$model::updateAll([ 'views_count' => new \yii\db\Expression('views_count + 1') ], 'id = ' . $id);

			LogActivity::store( $this->prefixName, 'read', $id, $this->participantId, 10 );

			return true;
		}

		return false;

	}


	/**
	 * @param      $post_id  	Variable ini adalah primary key dari table post_comment
	 * 
	 * @package    \common\models\PostComment
	 * 
	 * @uses 	/common/models/LogActivity:check()		Memeriksa data atifitas
	 * @uses 	/common/models/LogActivity:store()  	Menyimpan data aktifitas
	 * @uses 	/common/models/LogActivity:remove() 	Menghapus aktifitas
	 * 
	 * @return     boolean
	 */
	public function commentLike( $post_id )
	{
		
		$log = LogActivity::check( $this->prefixName, 'comment-like', $post_id, $this->participantId )->one();
		
		if ( $log === null )
		{
			$store = LogActivity::store( $this->prefixName, 'comment-like', $post_id, $this->participantId, 0 );

			$postComment = PostComment::fetchOne( $post_id );
			$postComment->total_likes = $postComment->total_likes + 1;
			$postComment->save();
			
			return ['like' => true];

		} else {

			LogActivity::remove( $log->id );

			$postComment = PostComment::fetchOne( $post_id );
			$postComment->total_likes = $postComment->total_likes - 1;
			$postComment->save();

			return ['unlike' => $store];	

		}
		return false;
	}


	public function getIdsByData( $data, $action )
	{

        $dataIds = ArrayHelper::getColumn($data, 'id');

        $dataActivities = LogActivity::check( $this->prefixName, $action, $dataIds, $this->participantId )->all();
        $dataActivitiesIds = ArrayHelper::index($dataActivities, 'post_id');

        return $dataActivitiesIds;
	}
}