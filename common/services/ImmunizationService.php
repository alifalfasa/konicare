<?php

namespace common\services;

use Yii;

use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;

use common\models\Immunization,
	common\models\ImmunizationSchedule,
	common\models\LogActivity,
	common\models\Participant;

/**
 * ImmunizationService class
 */
class ImmunizationService
{
	

	public $participantId;

	const SAVE_POINT = 60;

	public function __construct( $participantId )
	{
		$this->participantId = $participantId;
	}


	public function remove( $id )
	{
		$model = Immunization::getOne( $id );

		$schedule = ImmunizationSchedule::deleteAll(
			'immunization_id = :immunization_id', 
			[ ':immunization_id' => $id ] 
		);
		
		if ( $model->delete( $id ) ) 
		{
			return [ 'status' => true, 'message' => 'Success'];
		}

		return [ 'status' => false, 'message' => $model->getErrors() ];
	}


	public function save( $data = [])
	{	

		$immunization = $data[ 'Immunization' ];
		$birthdate    = $immunization['birthdate'];

		/**
		$age = date_diff(date_create($birthdate), date_create('now'))->y;
		if ( $age > 6 )
		{
			return [
				'status' => false,
				'message' => 'The age doesn\'t more than 6 years',
				// 'data' => $immunization
			];
		}
		*/

		$immunization[ 'participant_id' ] = $this->participantId; 

		$model = new Immunization();
		$save  = Immunization::saveData( $model, [ 'Immunization' => $immunization ] );

		if ( $save['status'] === true )
		{

			// Apakah dia pernah mendapatkan point imunisasi?
			$log = LogActivity::check( $model->tableName(), 'save', 0, $this->participantId )->one();
			if ( empty( $log ) )
			{
				LogActivity::store( $model->tableName(), 'save', 0, $this->participantId, self::SAVE_POINT );
			}

			$this->setSchedules( $save['id'], $birthdate );
		}

		return $save;
	}


	public function setSchedules($immunizationId, $birthdate)
	{
		$connection = Yii::$app->db;
		$transaction = $connection->beginTransaction();

		$model = new ImmunizationSchedule;
		$model->birthdate = $birthdate;
		$model->immunization_id = $immunizationId;
		$model->created_at = strtotime('now');

		try {

		    $connection->createCommand($model->setHepatitis())->execute();
		    $connection->createCommand($model->setPolio())->execute();
		    $connection->createCommand($model->setBCG())->execute();
		    $connection->createCommand($model->setDTP())->execute();
		    $connection->createCommand($model->setHib())->execute();
		    $connection->createCommand($model->setPCV())->execute();
		    $connection->createCommand($model->setRotavirus())->execute();
		    $connection->createCommand($model->setInfluenza())->execute();
		    $connection->createCommand($model->setCampak())->execute();
		    $connection->createCommand($model->setTifold())->execute();
		    $connection->createCommand($model->setMMR())->execute();
		    $connection->createCommand($model->setVarisela())->execute();
		    $connection->createCommand($model->setHepatitisA())->execute();
		    // $connection->createCommand($sql2)->execute();
		    $transaction->commit();

		} catch (\Exception $e) {
		    $transaction->rollBack();
		    throw $e;
		} catch (\Throwable $e) {
		    $transaction->rollBack();
		    throw $e;
		}
	}

	public function getAllSchedules( $year, $month )
	{
		$participantId = $this->participantId;

		$date = $year . '-' . $month;

		$schedules = ImmunizationSchedule::find()
			->innerJoinWith( [
				'immunization' => function( $query ) use ($participantId) {
					$query->andWhere( [ 'participant_id' => $participantId ] );
				},
				'immunizationCategory'
			])
			->andWhere( ['like', 'date', $date] )
			// ->limit(10)
			->all();

		$result = [];
		foreach ( $schedules as $schedule ) {

			$immunization = $schedule->immunization;

			if ( !isset($result[ $schedule->immunization_id ]) )
			{

				$result[ $schedule->immunization_id ] = [
					'immunization_id' => $immunization->id,
					'name' => $immunization->children_name,
					'categories' => [
						[
							'id' => $schedule->immunizationCategory->id,
							'name' => $schedule->immunizationCategory->name
						]
					]
				];
			} else {

					$result[$schedule->immunization_id]['categories'][] = [
						'id' => $schedule->immunizationCategory->id,
						'name' => $schedule->immunizationCategory->name
					];
			}

		}


		// Final output, remove immunization id
		$serializes = [];
		foreach( $result as $serialize )
		{
			$serializes[] = $serialize;
		}
		return $serializes;

	}

}