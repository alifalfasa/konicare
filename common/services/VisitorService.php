<?php

namespace common\services;

use Yii;
use common\models\Visitor;

class VisitorService 
{
	

	public static function record($via = 'web')
	{

    	$request = Yii::$app->getRequest();
    	
        $ip = $request->getUserIP();

        $visitor   = Visitor::findOne([ 'ip' => $ip, 'date' => date('Y-m-d')]);
        
        if ( empty( $visitor ) ) {
        	
        	$data['Visitor'] = [
        		'ip' => $ip,
        		'previous_visit' => strtotime('now'),
        		'via' => $via,
        		'date' => date('Y-m-d')
        	];
        	$save = Visitor::saveData( new Visitor(), $data );

        } else {
        	$data['Visitor']['previous_visit'] = strtotime('now');
        	Visitor::saveData( $visitor, $data );
        }

	}


}