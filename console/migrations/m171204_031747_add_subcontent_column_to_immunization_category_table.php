<?php

use yii\db\Migration;

/**
 * Handles adding subcontent to table `immunization_category`.
 */
class m171204_031747_add_subcontent_column_to_immunization_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('immunization_category', 'subcontent', $this->string(255)->after('name'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('immunization_category', 'subcontent');
    }
}
