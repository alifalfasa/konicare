<?php

use yii\db\Migration;

/**
 * Handles the creation of table `participant_redeem`.
 */
class m171027_094503_create_participant_redeem_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('participant_redeem', [
            'id' => $this->primaryKey(),
            'participant_id' => $this->integer()->notNull(),
            'product_redeem_id' => $this->integer()->notNull(),
            'address' => $this->string(250)->notNull(),
            'created_at' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('participant_redeem');
    }
}
