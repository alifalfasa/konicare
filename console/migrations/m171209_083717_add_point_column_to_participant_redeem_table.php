<?php

use yii\db\Migration;

/**
 * Handles adding point to table `participant_redeem`.
 */
class m171209_083717_add_point_column_to_participant_redeem_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('participant_redeem', 'point', $this->integer()->after('address')->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('participant_redeem', 'point');
    }
}
