<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cronjob`.
 */
class m171030_045649_create_cronjob_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cronjob', [
            'id' => $this->primaryKey(),
            'action' => $this->string(50)->notNull(),
            'date' => $this->date()->notNull(),
            'created_at' => $this->integer()
        ]);
    }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cronjob');
    }
}
