<?php

use yii\db\Migration;

class m170727_070206_add_field_thumbnail_newsactivity extends Migration
{
    public function up()
    {
        $this->addColumn(
            'news_activity', 
            'thumbnail', 
            $this->string(150)->after('image_dir')->notNull()
        );
        $this->addColumn(
            'news_activity', 
            'thumbnail_dir', 
            $this->string(255)->after('thumbnail')->notNull()
        );
    }

    public function down()
    {

        $this->dropColumn( 'news_activity', 'thumbnail' );
        $this->dropColumn( 'news_activity', 'thumbnail_dir' );
        
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
