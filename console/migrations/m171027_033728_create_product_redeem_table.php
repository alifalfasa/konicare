<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_redeem`.
 */
class m171027_033728_create_product_redeem_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_redeem', [
            'id' => $this->primaryKey(),
            'product_name' => $this->string(100)->notNull(),
            'volume' => $this->integer(),
            'price' => $this->integer()->notNull(),
            'stock' => $this->integer()->notNull(),
            'point' => $this->integer()->notNull(),
            'row_status' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_redeem');
    }
}
