<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_redeem_date`.
 */
class m180515_100122_create_product_redeem_date_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_redeem_date', [
            'id' => $this->primaryKey(),
            'redeem_date' => $this->date()->notNull(),
            'row_status' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_redeem_date');
    }
}
