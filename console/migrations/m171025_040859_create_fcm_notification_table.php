<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fcm_notification`.
 */
class m171025_040859_create_fcm_notification_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('fcm_notification', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer(),
            'type' => $this->string(45),
            'title' => $this->string(150)->notNull(),
            'body' =>  $this->string(200),
            'message' => $this->text(),
            'participant_id' => 'INT(32) DEFAULT \'0\'  COMMENT "Jika valuenya 0 maka message akan kekirim ke semua participant"',
            'created_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('fcm_notification');
    }
}
