<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fcm_device`.
 */
class m171025_040638_create_fcm_device_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('fcm_device', [
            'id' => $this->primaryKey(),
            'device_token' => $this->string(255)->notNull(),
            'participant_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('fcm_device');
    }
}
