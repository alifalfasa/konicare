# Welcome to Bunda Konicare Project

Pada project ini dibuat menggunakan Yii2 Framework dan CMSnya menggunakan template AdminLTE. Tidak hanya itu untuk frontend sudah di buatkan template based on foundation css framework

Template arsitektur ini memiliki 7 bagian: api, backend, common, console, frontend, environments dan media.


-- 

The template is designed to work in a team development environment. It supports
deploying the application in different environments.

Documentation is at [docs/guide/README.md](docs/guide/README.md).

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-advanced/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-advanced/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-advanced.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-advanced)

# DIRECTORY STRUCTURE

	media			
		/{model_name}   contains file uploads and the name depends on your model
    api
		components/		contains user definition functions
        config/			contains shared configurations
        mail/			contains view files for e-mails
        models/  		contains model classes used in both backend and frontend
        tests/   		contains tests for common classes
        console
        config/  		contains console configurations
        controllers/ 	contains console controllers (commands)
        migrations/  	contains database migrations
        models/  		contains console-specific model classes
        runtime/ 		contains files generated during runtime
    common
		components/		contains user definition functions
        config/			contains shared configurations
        mail/			contains view files for e-mails
        models/  		contains model classes used in both backend and frontend
        tests/   		contains tests for common classes
        console
        config/  		contains console configurations
        controllers/ 	contains console controllers (commands)
        migrations/  	contains database migrations
        models/  		contains console-specific model classes
        runtime/ 		contains files generated during runtime
    backend
		components/		contains user definition functions
        assets/  		contains application assets such as JavaScript and CSS
        config/  		contains backend configurations
        controllers/ 	contains Web controller classes
        models/  		contains backend-specific model classes
        runtime/ 		contains files generated during runtime
        tests/   		contains tests for backend application
        views/   		contains view files for the Web application
        web/ 			contains the entry script and Web resources
    frontend
		components/		contains user definition functions
        assets/  		contains application assets such as JavaScript and CSS
        config/  		contains frontend configurations
        controllers/ 	contains Web controller classes
        models/  		contains frontend-specific model classes
        runtime/ 		contains files generated during runtime
        tests/   		contains tests for frontend application
        views/   		contains view files for the Web application
        web/ 			contains the entry script and Web resources
        widgets/ 		contains frontend widgets
        vendor/  		contains dependent 3rd-party packages
    environments/		contains environment-based overrides

## Fitur Yang Tertanam Di Aplikasi

### 1. Firebase Cloud Messaging 
Untuk push notifikasi ke mobile apps, seperti mengirim *latest article*, jadwal imunisasi.

### 2. CronJob
Cron job berfungsi untuk memeriksa apakah ada artikel terbaru atau ada jadwal imunisasi, bila ada maka akan ada push notifikasi menggunakan FCM Google.

### 3. Facebook OAuth 2
Fitur ini digunakan untuk API dan Frontend

### 4. Recaptcha
Recaptcha digunakan pada saat participant/user membuat topic forum baru.

### 5. Email SMTP
Email notifikasi digunakan ketika participant lupa password, register dan reset password.


## Getting Started

To running this project we have any somethings you need to know, so that you feel joy. 

### 1.	Git
Clone project ada di [http://artemis.microad.co.id/konicare/konicare.git](http://artemis.microad.co.id/konicare/konicare.git "Konicare Project ")

### 2.  Create your database
Dari awal pembuatan database tidap pernah menggunakan migration, jadi bisa di *import* dari database staging atau production.

Adanya migration yang dibuat *(/konicare/console/migrations/)*, itu bukan dari awal pertamakali development, jadi dipertengahan development baru dibuat migration bila ada perubahan.

Lebih baik melanjutkan migration yang sudah dibuat bila ada perubahan mengenai **database** 

### 3.  Configuration

Semua konfigurasi untuk inisialisasi ada pada folder *(/konicare/environments)*
> Sangat penting untuk melihat-lihat folder tersebut, karna ini akan digunakan pada inisialisasi pertama kali

Bila *environment* sudah siap maka bisa dilanjut dengan inisialisasi

`php init`

Nanti anda akan diberikan petunjuk untuk instalisasinya.

Semua inisialisasi akan di generate dan memasukan file ke dalam folder masing-masing. Sehingga menghasilkan 

-----------------------
	...
	api
		config/
			params-local.php
			main-local.php
			...
		web/
			index.php
	common
		config/
			params-local.php    ===> PROJECT name
			main-local.php      ===> DATABASE configurations 
			static.json
			...
	backend
		config/
			params-local.php
			main-local.php
			...
		web/
			index.php
	frontend
		config/
			params-local.php
			main-local.php
			...
		web/
			index.php

Todo: `common/config/static.json` harus bisa di write (755)

## POSTMAN
Collection untuk resource-resouce API

[https://www.getpostman.com/collections/700bd1b59ffd5ab4fb80]()

## Time to running

You can access backend on [http://localhost/konicare](http://localhost/konicare) and you should be redirected to register page, caused you don't have user account.

## References

- [http://www.yiiframework.com/doc-2.0/index.html](http://www.yiiframework.com/doc-2.0/index.html "Yii2 Framework")
- [http://demos.krajee.com/widget-details/datepicker](http://demos.krajee.com/widget-details/datepicker)
- [https://github.com/kartik-v/yii2-widget-datetimepicker](https://github.com/kartik-v/yii2-widget-datetimepicker)
- [http://twig.sensiolabs.org/doc/2.x/](http://twig.sensiolabs.org/doc/2.x/)
- [https://github.com/yiisoft/yii2-twig/tree/master/docs/guide](https://github.com/yiisoft/yii2-twig/tree/master/docs/guide)
- [https://getcomposer.org/](https://getcomposer.org/)
- [https://almsaeedstudio.com/preview](https://almsaeedstudio.com/preview)
