<?php
return [

    'auth/login/<type:[0-9a-zA-Z\-]+>' => 'auth/login',
    'participant/register' => 'auth/register',
    'location/regency/<provinceId:\d+>' => 'location/regency',
    'location/regency/<provinceId:[0-9]+>/<regencyId:[0-9]+>' => 'location/regency',
    'article/get-comment/<article_id:\w+>' => 'article/get-comment',
    'news-activity/get-comment/<news_activity_id:\w+>' => 'news-activity/get-comment',
    'forum/topic/<slug:[0-9a-zA-Z\-]+>' => 'forum/topic-detail',
    'change-password' => 'participant/change-password',
    'search/<keywords:[0-9a-zA-Z\- ]+>' => 'site/search',
    'page/<type:[0-9a-zA-Z\-]+>' => 'site/page',
    'forums' => 'forum/get-all',
    'forum/<forum_id:[0-9]+>' => 'forum/get',
    'contact-us' => 'site/contact-us',

    // Default route \\
    '<controller:[0-9a-zA-Z\-]+>/<id:\d+>' => '<controller>/view',
    '<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>/<id:\d+>' => '<controller>/<action>',
    '<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>' => '<controller>/<action>',

];