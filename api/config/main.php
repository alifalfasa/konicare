<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$projectName = 'api' . $params['project']['firstname'] . $params['project']['lastname'];

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'component' => [
            'class' => 'api\components\BaseComponent',
        ],
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => false,
            'cookieValidationKey' => sha1($projectName),
            'csrfParam' => '_csrf-' . $projectName,
            'baseUrl'=>$params['baseUrl'].'/',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                // var_dump($response);exit;
                if ($response->format === 'json') {

                    $response->data = [
                        'status' => [
                            'code' => $response->statusCode,
                            'name' => $response->statusText,
                        ],
                        'data' => $response->data,
                    ];

                }
            },
        ],
        'cache'  => [
            'class'  => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass'  => 'api\models\Auth',
            'enableAutoLogin'  => false, // Don't forget to set Auto login to false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $params['baseUrl'].'/api',
            'enablePrettyUrl'  => true,
            'showScriptName'  => false,
            'rules' => require_once('routes.php'),
        ],
    ],
    'params' => $params,
];
