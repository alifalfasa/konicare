<?php

namespace api\models;

use Yii;

use api\models\ParticipantRedeem;

use common\models\LogActivity;

/**
 * This is the model class for table "product_redeem".
 *
 * @property integer $id
 * @property string $product_name
 * @property integer $volume
 * @property integer $price
 * @property integer $stock
 * @property integer $point
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ProductRedeem extends \common\models\BaseModel
{

    /**
     * Pick up function
     * Check all validation if participant want to redeem any product
     *
     * @param  integer  $id             The identifier
     * @param  integer  $participantId  The participant identifier
     *
     * @return  array
     */
    public static function pickUp( $id, $participantId, $address )
    {
        $product = parent::fetchOne( $id );

        if ( $product->stock <= 0 )
        {
            return [ 
                'success' => false,
                'error' => [
                    'message' => 'Stok produk "' . $product->product_name . '" tidak tersedia'
                ]
            ];
        }

        // Whether participant get permission to redeem or not. 
        $checkParticipant = ParticipantRedeem::isAllowed( $product, $participantId );
        if ( $checkParticipant[ 'success' ] === false )
            return $checkParticipant;

        $participantRedeem = new ParticipantRedeem();
        $participantRedeem->participant_id     = $participantId;
        $participantRedeem->product_redeem_id  = $id;
        $participantRedeem->created_at         = strtotime('now');
        $participantRedeem->address            = $address;
        $participantRedeem->point              = $product->point;
        if ( $participantRedeem->save() )
        {
            
            $product->stock = $product->stock - 1;
            $product->save();

            $activity = LogActivity::store(
                'product-redeem',
                'redeem',
                $participantRedeem->id,
                $participantId,
                -$product->point
            );

            return [ 'success' => true, 'callback' => $activity ];
        } else {
            return [ 
                'success' => false, 
                'error' => [
                    'message' => $participantRedeem->getErrors()
                ]
            ];
        }

    }


    public static function pickUpCoupon( $id, $quantity, $participantId, $address )
    {
        $product = parent::fetchOne( $id );

        // Whether participant get permission to redeem or not. 
        $checkParticipant = ParticipantRedeem::isEnough( $product, $quantity, $participantId );
        if ( $checkParticipant[ 'success' ] === false )
            return $checkParticipant;

        $result = [ 'success' => true ];

        for( $i = 0; $i < $quantity; $i++)
        {
            $participantRedeem = new ParticipantRedeem();
            $participantRedeem->participant_id     = $participantId;
            $participantRedeem->product_redeem_id  = $id;
            $participantRedeem->created_at         = strtotime('now');
            $participantRedeem->address            = $address;
            $participantRedeem->point              = $product->point;
            // $participantRedeem->product_code       = $product_code;
            if ( $participantRedeem->save() )
            {
                $product_code = dechex(date('d') . date('m') . str_pad($participantRedeem->id, 5, '0', STR_PAD_LEFT));
                $participantRedeem->product_code = $product_code;
                $participantRedeem->save();
                $activity = LogActivity::store(
                    'product-redeem',
                    'redeem',
                    $participantRedeem->id,
                    $participantId,
                    -$product->point
                );
                $result['callback'][] = $activity;
                // return [ 'success' => true, 'callback' => $activity ];
            } else {
                $result['success'] = false;
                $result['error']['message'][] = $participantRedeem->getErrors();
                // return [ 
                //     'success' => false, 
                //     'error' => [
                //         'message' => $participantRedeem->getErrors()
                //     ]
                // ];
            }
        }
        return $result;
    }
}
