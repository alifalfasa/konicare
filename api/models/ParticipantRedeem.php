<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "participant_redeem".
 *
 * @property integer $id
 * @property integer $participant_id
 * @property integer $product_redeem_id
 * @property integer $address
 * @property integer $created_at
 */
class ParticipantRedeem extends \common\models\BaseModel
{

    public $updated_at;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participant_redeem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['participant_id', 'product_redeem_id', 'address', 'point', 'created_at'], 'required'],
            [['participant_id', 'product_redeem_id', 'point', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'participant_id' => 'Participant ID',
            'product_redeem_id' => 'Product Redeem ID',
            'created_at' => 'Created At',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ProductRedeem::className(), ['id' => 'product_redeem_id']);
    }


    /**
     * Determines if allowed.
     * Rules:
     * 1. If participant point is enough to redeem
     * 2. If participant never redeem product
     * 3. Participant not redeem product yet in current month
     * 4. If particpant had redeem product at last month, 
     * they can allow for redeem product.
     * 
     * @param  object  $productRedeemId  The product redeem
     * @param  integer  $participantId    The participant identifier
     *
     * @return     array   True if allowed, False otherwise.
     */
    public static function isAllowed( $product, $participantId )
    {
        // Check participant point
        $participant = Participant::find()
            ->select('points')
            ->where(['id' => $participantId])
            ->one();
        if ( $participant->points < $product->point )
        {
            return [
                'success' => false,
                'error' => [
                    'message' => 'Maaf point anda tidak cukup untuk produk ' . $product->product_name
                ]
            ];
        }

        $coupon1 = ProductRedeem::find()
            ->where(['=', 'product_name', 'Coupon Bunda Konicare Season 1'])
            ->asArray()
            ->one();
        $coupon2 = ProductRedeem::find()
            ->where(['=', 'product_name', 'Coupon Bunda Konicare Season 2'])
            ->asArray()
            ->one();
        $query = static::find()
            ->andWhere([ 
                'participant_id' => $participantId
            ])
            ->andWhere([ 'not in', 'product_redeem_id', [$coupon1['id'], $coupon2['id']] ])
            ->orderBy('id DESC')
            ->one();

        // Check if participant never redeem product before.
        if ( empty( $query ) )
        {
            return [ 'success' => true ];

        } else {

            $created_at = date( 'Y-m-d', $query->created_at );
            $currentMonth = date( 'Y-m-d' );

            // Whether they had been redeem point at the same product in the same current periode/day or not.
            if ( $created_at == $currentMonth )
            {
                return [ 
                    'success' => false, 
                    'error' => [ 
                        'message' => 'Anda sudah melakukan redeem pada produk di periode ini.'
                    ]
                ];
            } else {

                return [
                    'success' => true
                ];
            }

        }

    }

    public static function isEnough( $product, $quantity, $participantId )
    {
        // Check participant point
        $participant = Participant::find()
            ->select('points')
            ->where(['id' => $participantId])
            ->one();
        if ( $participant->points < ($quantity * $product->point) )
        {
            return [
                'success' => false,
                'error' => [
                    'message' => ['Maaf point anda tidak cukup untuk ' . $quantity . ' pcs ' . $product->product_name]
                ]
            ];
        }

        return [ 'success' => true ];

    }
}
