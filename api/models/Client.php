<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $name
 * @property string $secret_key
 * @property integer $row_status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class Client extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }
    
    public static function identify( $name, $secret )
    {
    	$client = static::fetch()->andWhere([
    		'name' => $name,
    		'secret_key' => $secret
    	])->one();

    	return $client;
    }
}
