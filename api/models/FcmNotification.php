<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "fcm_notification".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $type
 * @property string $title
 * @property string $body
 * @property string $message
 * @property integer $participant_id
 * @property string $created_at
 */
class FcmNotification extends \common\models\BaseModel
{

    public $updated_at;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fcm_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'participant_id'], 'integer'],
            [['title', 'created_at'], 'required'],
            [['message'], 'string'],
            [['created_at'], 'safe'],
            [['type'], 'string', 'max' => 45],
            [['title'], 'string', 'max' => 150],
            [['body'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'type' => 'Type',
            'title' => 'Title',
            'body' => 'Body',
            'message' => 'Message',
            'participant_id' => 'Paticipant ID',
            'created_at' => 'Created At',
        ];
    }
}
