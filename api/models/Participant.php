<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "participant".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $usertype
 * @property string $fbuserid
 * @property string $fbusername
 * @property string $fbtoken
 * @property string $email
 * @property string $fullname
 * @property string $gender
 * @property string $birthdate
 * @property string $notelp
 * @property string $nohp
 * @property string $address
 * @property integer $regency_id
 * @property integer $province_id
 * @property string $country
 * @property string $zip
 * @property string $avatarimg
 * @property integer $user_status
 * @property integer $is_notif
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ForumPost[] $forumPosts
 * @property ForumTopic[] $forumTopics
 */
class Participant extends \common\models\Participant
{

	protected $province_name, $regency_name;
	
	public function updateFilter() 
	{
		return [ 
			'fullname',
			'notelp',
			'birthdate',
			'nohp',
			'address',
			'regency_id',
			'province_id',
			'zip',
			'points'
		];
	}
}
