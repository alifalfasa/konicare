<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $keyword
 * @property string $subcontent
 * @property string $content
 * @property string $image
 * @property string $main_image
 * @property string $highlight_image
 * @property string $highlight_title
 * @property string $highlight_description
 * @property integer $highlight_status
 * @property string $tvc
 * @property string $store_url
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Product extends \common\models\BaseModel
{

}
