<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "cronjob".
 *
 * @property integer $id
 * @property string $action
 * @property string $date
 * @property integer $created_at
 */
class Cronjob extends \common\models\BaseModel
{

    public $updated_at;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cronjob';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'date'], 'required'],
            [['date'], 'safe'],
            [['created_at'], 'integer'],
            [['action'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'date' => 'Date',
            'created_at' => 'Created At',
        ];
    }
}
