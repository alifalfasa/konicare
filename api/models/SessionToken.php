<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "session_token".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $participant_id
 * @property string $access_token
 * @property string $ip
 * @property integer $expired_date
 * @property integer $created_at
 */
class SessionToken extends \common\models\BaseModel
{
    public $created_by, $updated_at;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'session_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'access_token', 'ip', 'expired_date'], 'required'],
            [['client_id', 'participant_id', 'expired_date', 'created_at'], 'integer'],
            [['access_token'], 'string', 'max' => 150],
            [['ip'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'participant_id' => 'Participant ID',
            'access_token' => 'Access Token',
            'ip' => 'Ip',
            'expired_date' => 'Expired Date',
            'created_at' => 'Created At',
        ];
    }
}
