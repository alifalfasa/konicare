<?php

namespace api\models;

use Yii;

class Auth extends \common\models\BaseModel implements \yii\web\IdentityInterface
{

    public $username;
    public $password;
    public $rememberMe;
    public $created_by;
    public $updated_at;
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'session_token';
    }
 
    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $query = static::find()
            ->where(['>=', 'expired_date', strtotime('NOW')])
            ->andWhere(['=', 'access_token', $token])
            ->one();

        return $query;
    }
 
    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }
 
    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
 
    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
 
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }



    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getParticipant();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('username', null);
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return $this->getParticipant();
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getParticipant()
    {
        if ($this->_user === null) {
            $this->_user = Participant::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Store participants access token
     *
     * @param      int       $clientId       The client identifier
     * @param      int|null  $participantId  The participant identifier
     *
     * @return     array     access token, expired_date
     */
    public static function saveAccessToken( $clientId, $participantId = null )
    {
        $access_token = Yii::$app->request->getCsrfToken();

        // Note: Dulunya perhari tokennya, tapi ada kendala di developer mobile yang tidak dapat merefreshtoken ketika expired, sebab itu di set sampai dua tahun
        $expired_date = strtotime('+2 years'); 
        
        $session['SessionToken'] = [
            'username' => Yii::$app->request->post('username'),
            'password' => Yii::$app->request->post('password'),
            'client_id' => $clientId,
            'participant_id' => $participantId,
            'access_token' => $access_token,
            'ip' => Yii::$app->request->userIP,
            'expired_date' => $expired_date,
        ];
        $save = SessionToken::saveData( new SessionToken(), $session);
        if ( $save['status'] == false )
        {
            return [ 'error' => $save['message'] ];
        }
        return [ 'access_token' => 'Bearer ' . $access_token, 'expired_date' => date('Y-m-d H:i:s', $expired_date) ];

    }
}