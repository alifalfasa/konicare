<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "fcm_device".
 *
 * @property integer $id
 * @property string $device_token
 * @property integer $participant_id
 */
class FcmDevice extends \common\models\BaseModel
{

    public $created_at, $updated_at;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fcm_device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_token', 'participant_id'], 'required'],
            [['participant_id'], 'integer'],
            [['device_token'], 'string', 'max' => 255],
            [['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Participant::className(), 'targetAttribute' => ['participant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_token' => 'Device Token',
            'participant_id' => 'Participant ID',
        ];
    }

    public static function getDetail( $device_token, $participant_id )
    {
        return static::find()
            ->andWhere(['=', 'device_token', $device_token])
            ->andWhere(['=', 'participant_id', $participant_id])
            ->one();
    }

    public static function getAllTokens()
    {
        return static::find()->select(['device_token'])->all();
    }

}
