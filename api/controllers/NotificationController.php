<?php
namespace api\controllers;

use Yii;

use api\models\FcmNotification as Notification;

use yii\web\NotFoundHttpException,
    yii\helpers\ArrayHelper;

/**
 * Notification controller
 */

class NotificationController extends BaseController
{


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'send' => ['POST']
        ];
    }

    public function actionList()
    {
        $this->checkLogin();

        $notifications = Notification::find()
            ->andWhere(['in', 'participant_id', [ $this->user->id, 0 ] ])
            ->orderBy('created_at DESC');

        $pagination = $this->component->pagination($notifications, 10);

        $notifications   = $notifications 
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->all();

        $result = [];
        foreach( $notifications as $notification )
        {
            $result[] =[
                'post_id' => $notification->post_id,
                'type' => $notification->type,
                'title' => $notification->title,
                'body' => $notification->body,
                'created_at' => date('Y-m-d H:i:s', $notification->created_at)
            ];
        }
        return [ 'pagination' => $pagination, 'notifications' => $result ];
    }


}
