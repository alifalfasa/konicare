<?php
namespace api\controllers;

use Yii;

use common\models\Message;

use common\models\NewsActivity;
use common\models\Article;
use common\models\ForumTopic;
use common\models\Page;

use frontend\services\SearchService,
    common\services\ActivityService;

/**
 * Site controller
 */
class SiteController extends BaseController
{

    public $modelClass = 'backend\models\Page';


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'contact-us' => ['POST']
        ];
    }


    public function actionSendMessage()
    {

    	$post = Yii::$app->request->post();
    	$data['Message'] = [ 
    		'name' 		  => $post['name'],
            'title'       => $post['title'],
    		'email' 	  => $post['email'],
    		'content' 	  => $post['content'],
    		'read_status' => 0,
    		'row_status'  => 1,
    	];

        $model = new Message();
    	$save = Message::saveData( new Message(), $data );

    	return [ 'message' => $save ];
    }

    /**
     * Homepage of mobile apps
     * 
     * @return  json  ( The response just appear only 3 post. 1 Activity, 2 Article and 3 Forum )
     * 
     */
    public function actionHome()
    {

        // News Activity Section
        $newsActivity = NewsActivity::fetch()->one()->toArray();
        $this->component->parseImage($newsActivity, ['thumbnail']);
        $activityResult = [
            'id' => $newsActivity[ 'id' ],
            'type' => 'Aktifitas',
            'category' => '',
            'title' => $newsActivity[ 'title' ],
            'slug' => $newsActivity[ 'slug' ],
            'content' => $newsActivity[ 'subcontent'],
            'created_at' => date( 'd F Y', $newsActivity[ 'created_at' ] ),
            'image' => $newsActivity[ 'thumbnail_original' ],
        ];


        // Article Section
        $article = Article::fetch()->joinWith( 'category' )->asArray()->one();

        $activityService = new ActivityService( new Article, isset($this->user) ? $this->user->id : null );

        $likeStatus = $activityService->getIdsByData( [$article], 'like' );

        $this->component->parseImage($article, ['image']);
        $articleResult = [
            'id' => (int) $article[ 'id' ],
            'type' => 'Artikel',
            'category' => $article[ 'category' ][ 'name' ],
            'title' => $article[ 'title' ],
            'slug' => $article[ 'slug' ],
            'content' => $article[ 'subcontent'],
            'created_at' => date( 'd F Y', $article[ 'created_at' ] ),
            'image' => $article[ 'image_thumb' ],      
            'like_count' => $article[ 'likes_count' ],
            'like_status' => isset( $likeStatus[ $article['id'] ] ) ? true : false
        ];
        

        // Forum Section
        $forumTopic = ForumTopic::fetch()->andWhere([ '=', 'forum_topic.row_status', 1 ])->innerJoinWith(['post', 'forum'])->one();
        $forumResult = [
            'id' => $forumTopic->id,
            'type' => 'Forum',
            'category' => $forumTopic['forum']->name,
            'title' => $forumTopic->name,
            'slug' => $forumTopic->slug,
            'content' => $forumTopic->post->content,
            'created_at' => date( 'd M Y', $forumTopic->created_at ),
            'image' => ''
        ];

        $result = [
            $activityResult,
            $articleResult,
            $forumResult
        ];
        return $result;
    }


    public function actionPage( $type )
    {
        $page = Page::getData( $type )->toArray();
        $this->component->parseImage($page, ['image']);
        
        return $page;
    }


    public function actionSearch( $keywords )
    {
        $result = SearchService::result( $keywords );

        // $article = $result[ 'articles' ];
        $this->component->parseImage($result[ 'articles' ], ['image']);
        $this->component->parseImage($result[ 'newsActivities' ], ['image']);

        return $result;
    }


    /**
     * Contact Us Resource
     * 
     * @link  http://base.url/contact-us
     */
    public function actionContactUs()
    {
        $model = new \common\models\Message;

        $post['Message'] = Yii::$app->request->post();
        if ( !empty($post['Message']) )
        {
            $model->subject = 'Pesan: Halaman Kami';
            $saveModel = $model::saveData($model, $post);
            if ( $saveModel['status'] == true )
            {
                return [ 'success' => true ];
            }
            return [ 'error' => [ 'message' => $saveModel['message'] ] ];
        }

    }


    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        return [
            'message' => $exception->getMessage()
        ];
    }
}
