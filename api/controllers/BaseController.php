<?php
namespace api\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;

use api\models\Auth;
use api\models\Participant;

/**
 * Site controller
 */
class BaseController extends ActiveController
{

    public $user;
    public $modelClass = 'api\models\Auth';
    public $component;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['login'],
        ];
        return $behaviors;
    }

	public function init()
	{
        parent::init();

        $headers = Yii::$app->request->getHeaders();
        if ( $headers['authorization'] )
        {
            $token = str_replace('Bearer ', '', $headers['authorization']);
            $session = Auth::findIdentityByAccessToken($token);
            if ( !empty($session) )
            {
                $this->user = Participant::fetch()->andWhere(['=', 'id', $session->participant_id])->one();
                $this->component = Yii::$app->component;
            }

            \common\services\VisitorService::record('mobile');

        }
	}

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        return $result;
    }

    public function checkLogin()
    {
        if (empty($this->user))
        {
            throw new \yii\web\HttpException(403, 'You don’t have permission to access this resource');
        }
    }
}
