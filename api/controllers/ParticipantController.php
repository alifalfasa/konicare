<?php
namespace api\controllers;

use Yii;

use api\models\Participant;
use api\models\ParticipantRedeem;

use common\models\LogActivity;

use common\services\ActivityService;

use common\services\AuthService;

use api\models\Auth;

/**
 * Site controller
 */

class ParticipantController extends BaseController
{


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'info' => ['get'],
            'register' => ['post'], 
            'update-profile'   => ['post'], 
            'change-password' => ['post']
        ];
    }


    /**
     * Participant Info
     * 
     * @link  https://bundakonicare.com/api/participant/info
     *
     * @return     array  
     */
    public function actionInfo()
    {
        $this->checkLogin();

        $userId = $this->user->id;

        $participant = Participant::fetch()
            ->andWhere([ 'participant.id' => $userId ])
            ->joinWith('province')
            ->joinWith('regency')
            ->one();
            // var_dump($participant->points);
        $participantAcitivityPoint = $participant->updatePointFromActivity();
        
        $regency  = !empty($participant->regency) ? $participant->regency->name : '';
        $province = !empty($participant->province) ? $participant->province->name : '';

        $couponCount = ParticipantRedeem::find()
                        ->andWhere(['participant_id' => $userId])
                        ->andWhere(['product_redeem.product_name' => 'Coupon Bunda Konicare Season 2'])
                        ->joinWith('product')
                        ->count();

        $result = [
            'id'            => $participant->id,
            'username'      => $participant->username,
            'email'         => $participant->email,
            'fullname'      => $participant->fullname,
            'gender'        => $participant->gender,
            'birthdate'     => $participant->birthdate,
            'nohp'          => $participant->nohp,
            'zip'           => $participant->zip,
            'avatar'        => $participant->getAvatar(),
            'regency_id'    => $participant->regency_id,
            'regency_name'  => $regency,
            'province_id'   => $participant->province_id,
            'province_name' => $province,
            'points'        => $participantAcitivityPoint,
            'couponCount'   => $couponCount
        ];

        return $result;
    }


    /**
     * Participant Update Profile 
     * 
     * @link  https://bundakonicare.com/api/participant/update/profile
     * 
     * @return     array  
     */
    public function actionUpdateProfile()
    {
        $this->checkLogin();

        $model = Participant::findOne($this->user->id);

        $post  = $this->component->updateFilter( $model );
        $save  = Participant::saveData($model, $post);

        if ( $save[ 'status' ] == true )
        {
            return [
                'username' => $model->username,
                'email' => $model->email,
                'fullname' => $model->fullname,
                'birthdate' => $model->birthdate,
                'notelp' => $model->notelp,
                'nohp' => $model->nohp,
                'address' => $model->address,
                'regency_id' => $model->regency_id,
                'province_id' => $model->province_id,
                'zip' => $model->zip,
                'avatar' => [ 
                    'original' => $model->getAvatar(),
                    'thumbnail' => $model->getAvatar( 'thumb' ),
                ]
            ];
        } else {

            Yii::$app->response->statusCode = 422;

            return [
                'error' => [
                    'message' => $save['message']
                ]
            ];
        }
    }


    public function actionChangeProfilePhoto()
    {

        // Abort every post parameter
        $post = Yii::$app->request->post();
        if ( count($post) ) throw new \yii\web\HttpException(422, 'Paramater post not allowed');
        
        // Check if avatarimg is not defined
        if ( empty($_FILES['avatarimg']) )  throw new \yii\web\HttpException(422, 'avatarimg is not defined');

        $data[ 'Participant' ][ 'avatarimg' ] = $this->user->avatarimg;
        $model = Participant::findOne( $this->user->id );

        $imageFile = $_FILES['avatarimg'];
        $_FILES['Participant'] = [
            'name' => [ 'avatarimg' => $imageFile[ 'name' ] ],
            'type' => [ 'avatarimg' => $imageFile[ 'type' ] ],
            'tmp_name' => [ 'avatarimg' => $imageFile[ 'tmp_name' ] ],
            'error' => [ 'avatarimg' => $imageFile[ 'error' ] ],
            'size' => [ 'avatarimg' => $imageFile[ 'size' ] ],
        ];

        $save = Participant::saveData( $model, $data );
        if ( $save[ 'status' ] == true )
        {
            $model = Participant::findOne($this->user->id);
            return [
                'avatar' => [ 
                    'original' => $model->getAvatar(),
                    'thumbnail' => $model->getAvatar( 'thumb' ),
                ]
            ];
        }
        return $model;
    }


    public function actionChangePassword()
    {
        $this->checkLogin();

        $parameterAllowed = [ 'old_password', 'new_password', 're_password' ];
        $post = $this->component->formFilter( 
            Yii::$app->request->post(), 
            $parameterAllowed 
        );
        
        $model = Participant::findOne( $this->user->id );

        if ( $model->validatePassword( $post['old_password'] ) ) 
        {

            if ( $post[ 'new_password' ] === $post[ 're_password' ] )
            {

                $model->setPassword( $post[ 'new_password' ] );
                $model->save();
                
                return [
                    'success' => [
                        'message' => 'Password berhasil diubah'
                    ]
                ];

            }

            Yii::$app->response->statusCode = 422;

            return [ 
                'error' => [
                    'message' => 'Password baru dan ulangi password tidak sama'
                ] 
            ];
        } 

        Yii::$app->response->statusCode = 422;

        return [
            'error' => [
                'message' => 'Password lama yang anda masukan salah'
            ]
        ];
    }
}
