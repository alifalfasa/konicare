<?php
namespace api\controllers;

use Yii;

use common\models\Forum,
    common\models\ForumGroup,
    common\models\ForumTopic,
    common\models\ForumPost,
    common\components\Functions,
    frontend\services\ForumService;

use yii\helpers\ArrayHelper;

/**
 * Forum controller
 */

class ForumController extends BaseController
{

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'topic' => ['GET'],
            'topic-detail' => ['GET'],
        ];
    }


    /**
     * Get all forum which have provided
     * 
     * @link http://base.url/forums
     *
     */
    public function actionGetAll()
    {
        $datas = ForumService::index();

        $groupIconDir = Yii::$app->params[ 'baseUrl' ] . '/media/forum-group-icon/';
        $result = [];
        foreach( $datas[ 'groups' ] as $group )
        {
            $forums = [];
            foreach( $group->forums as $forum )
            {
                $forums[] = [
                    'forum_id' => $forum->id,
                    'name' => $forum->name,
                    'description' => $forum->description,
                    'topic_count' => $forum->topic_count,
                    'post_count' => $forum->post_count,
                ];
            }
            $result[] = [
                'name' => $group->name,
                'icon' => $groupIconDir . $group->id . '/' . $group->icon,
                'group' => $forums
            ];
        }

        return $result;
    }


    /**
     * Get All Topics By Forum Id
     * 
     * @link  http://base.url/forum/10?page=1
     * 
     * @param  integer  forum_id  
     *
     */
    public function actionGet( $forum_id )
    {
        // Get Topic by forum Id
        $topics = ForumTopic::get()
            ->andWhere( ['forum_topic.forum_id' => $forum_id] )
            ->joinWith( ['participant'] )
            ->orderBy('forum_topic.updated_at DESC');

        $pagination = $this->component->pagination($topics, 6);
        $topics = $topics
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->all();
        $topicIds  = ArrayHelper::getColumn($topics, 'id');


        // Get First Post on topic
        $subQuery  = ForumPost::get()
            ->select('MIN(id)')
            ->andWhere(['topic_id' => $topicIds])
            ->groupBy('topic_id');

        $lastPosts = ForumPost::get()
            ->andWhere(['forum_post.id' => $subQuery])
            ->orderBy('forum_post.id ASC')
            ->all();
        $lastPosts = ArrayHelper::index($lastPosts, 'topic_id');

        // Serialized result
        $result = [];            
        foreach ( $topics as $topic ) {
            $result[] = [
                'topic' => $topic->name,
                'slug' => $topic->slug,
                'created_by' => $topic->participant->username,
                'content' => substr( $lastPosts[ $topic->id ]->content, 0, 150 ),
                'time' => Functions::timeElapsed($topic->created_at) . date( ' H:i A' )
            ];
        }

        return ['pagination' => $pagination, 'topics' => $result ];
    }


    /**
     * Get Topic Forum by slug
     * 
     * @link  http://base.url/forum/topic/tips-atasi-anak-susah-makan?page=1
     * 
     * @param  string  $slug   The slug of topic
     *
     */
    public function actionTopicDetail( $slug )
    {
        $topic = ForumTopic::detail( $slug )->one();
        $posts = ForumPost::get()
            ->andWhere( ['topic_id' => $topic->id] )
            ->joinWith( ['participant' => function($query){
                $query->select('id, username, avatarimg, created_at');
            }] );

        $pagination = $this->component->pagination( $posts, ForumService::MAX_DATA );

        $posts = $posts
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->orderBy('id ASC');

        $result = [];
        foreach( $posts->all() as $post )
        {
            $result[] = [
                'content' => $post->content,
                'created_at' => date( 'M d, Y H:i A', $post->created_at ),
                'participant' => [
                    'username' => $post->participant->username,
                    'avatar' => $post->participant->getAvatar('thumb_'),
                    'member_since' => date( 'd M Y', $post->participant->created_at ),
                ]
            ];
        }

        return [ 'pagination' => $pagination, 'topic' => $topic->name, 'posts' => $result ];
    }


}
