<?php
namespace api\controllers;

use Yii;

use common\models\ArticleCategory;
use yii\helpers\ArrayHelper;

/**
 * ArticleCategory controller
 */

class ArticleCategoryController extends BaseController
{

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
        ];
    }

    public function actionGet()
    {
    	$model = ArticleCategory::getAll();
    	
    	return $model;
    }

}
