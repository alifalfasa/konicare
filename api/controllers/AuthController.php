<?php
namespace api\controllers;

use Yii;
use api\models\Client;
use api\models\Auth;
use api\models\Participant;
use api\services\AuthService;

use frontend\services\FacebookService;

/**
 * Site controller
 */
class AuthController extends BaseController
{

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [ 'login' => ['post'] ];
    }

    /**
     * Login Authentication
     * 
     * @method POST
     * 
     * @param app_name      Nama aplikasi
     * @param app_secret    Secret Key pada client yang menggunakan API
     * @param username      Username participant
     * @param password      Password participant
     * 
     * @link  https://bundakonicare.com/api/auth/login
     * 
     * @return  json        access_token, expired_date
     */
    public function actionLogin($type)
    {
        $authService = new AuthService();

        $post = Yii::$app->request->post();
        $client = Client::identify( $post['app_name'], $post['app_secret'] );

        // 1. Check client
        if ( empty( $client ) )
        {
            Yii::$app->response->statusCode = 401;
            Yii::$app->response->statusText = 'Unauthorized';   

            return [ 'error' => ['message' => 'Client does not exist'] ];
        }

        $access_token = Yii::$app->request->getCsrfToken();
        
        $authService->setClientId( $client['id'] );

        // 2. Client Login 
        if ( $type == 'client' )
        {
            $authService->saveToken();

            return $authService->response();
            
        } elseif ( $type == 'participant' ) {
    
            // 3. Participant Login
            $auth = new Auth();

            if ( $auth->load( [ 'Auth' => $post ] ) && $auth->login() ) 
            {
                $participant = $auth->getParticipant();
                
                $authService->setParticipant( $participant );
                $authService->saveToken();

                return $authService->response();

            } else {

                Yii::$app->response->statusCode = 401;
                Yii::$app->response->statusText = 'Unauthorized';   

                return [ 'error' => ['message' => 'Incorrect username or password'] ];

            }

        } elseif ( $type == 'facebook' ) {

            if ( !empty( $post['facebook_token'] ) )
            {
                // Get user from facebook by token
                $facebook = new FacebookService();
                $user = $facebook->getUserByToken( $post['facebook_token'] );
                
                if ( !empty( $user['email']) )
                    $conditions = [ 'email' => $user['email' ] ];
                else
                    $conditions = [ 'fbuserid' => $user['id' ] ];

                $participant = Participant::fetch()
                    ->andWhere($conditions)
                    ->one();

                // Bila data facebook_id tidak ada di database
                if ( empty( $participant ) )
                {
                    $result = [
                        'is_register' => false,
                        'participant' => [
                            'fbuserid' => $user['id'],
                            'username' => \common\components\Functions::makeSlug($user['name'], null, '_') . rand(0,999),
                            'fullname' => $user['name'],
                            'email' => isset($user['email']) ? $user['email'] : null
                        ]
                    ];

                    return $result;
                    
                } else {

                    $authService->setParticipant( $participant );
                    $authService->saveToken();

                    return $authService->response();
                
                }

            } else {
                return [ 'error' => ['message' => 'facebook_token is not defined'] ];
            }
        }

    }


    /**
     * Participant Register
     * 
     * @link  https://bundakonicare.com/api/participant/register
     *
     * @return     array
     */
    public function actionRegister()
    {
        $post['Participant'] = Yii::$app->request->post();
        
        $participant = new Participant();
        $data['Participant'] = $post;
        $participant->load($post);
        $save = $participant->signup();
        if ( $save != false )
        {
            $prefixName = $participant->tableName();
            \common\models\LogActivity::store( $prefixName, 'register', $save->id, $save->id, 25 );
            \common\models\LogActivity::store( $prefixName, 'first-login', $save->id, $save->id, 25 );

            $authService = new AuthService();
            $authService->setClientId( 1 );
            $authService->setParticipant( $save );
            $authService->saveToken();

            return $authService->response();   

        } else {

            Yii::$app->response->statusCode = 422;

            return [
                'error' => [
                    'message' => Participant::getError($participant)
                ]
            ];
        }

    }
}
