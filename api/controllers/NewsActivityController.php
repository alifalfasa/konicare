<?php
namespace api\controllers;

use Yii;

use common\models\NewsActivity, 
    common\models\PostComment, 
    common\components\Functions,
    common\services\ActivityService;

use yii\web\NotFoundHttpException;

/**
 * NewsActivity controller
 */

class NewsActivityController extends BaseController
{

    private $activityService;

    public function init()
    {
        parent::init();
        $this->activityService = new ActivityService( new NewsActivity, isset($this->user) ? $this->user->id : null );
    }


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'get-comment'  => ['GET'],
            'detail'  => ['GET'],
            'comment' => ['POST']
        ];
    }


    /**
     * Action List
     * 
     * @uses    api/components/BaseComponent/pagination()
     *
     */
    public function actionLists()
    {
        $news_activity   = NewsActivity::get();

        $pagination      = $this->component->pagination($news_activity, 6);

        $news_activity   = $news_activity 
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->asArray()
            ->all();

        $this->component->parseImage($news_activity, ['thumbnail']);
        
        $result = [];
        foreach( $news_activity as $activity )
        {
            $result[] = [
                'id' => $activity['id'],
                'type' => $activity['type'],
                'title' => $activity['title'],
                'image_normal' => $activity['thumbnail_original'],
                'image_thumb' => $activity['thumbnail_original'],
            ];
        }
        
        return ['pagination' => $pagination, 'news_activity' => $result ];
    }


    public function actionDetail($id)
    {

        $news_activity = NewsActivity::get()
            ->select('id, title, slug, content, image, image_dir, created_at')
            ->andWhere(['=', 'id', $id])
            ->asArray()
            ->one();

        if ( empty($news_activity) )
        {
            throw new NotFoundHttpException();
        }
        
        $this->activityService->read($id);

        $news_activity['time']       = Functions::timeElapsed($news_activity['created_at']);
        $news_activity['created_by'] = 'Admin Konicare';

        $this->component->parseImage($news_activity, ['image']);

        return ['news_activity' => $news_activity];
    }


    public function actionComment()
    {
        
        $this->checkLogin();

        $post['PostComment'] = Yii::$app->request->post();
        $post['PostComment']['type']  = PostComment::NEWS_ACTIVITY;
        $post['PostComment']['participant_id'] = $this->user->id;

        $save = PostComment::saveData( new PostComment(), $post );
        if ( $save['status'] == true )
        {
            $this->activityService->comment($post['PostComment']['post_id']);
        } else {
            return [
                "error" => [ 
                    'message' => $save['message']
                ]
            ];
        }

        return $save;
    }
    
    
    public function actionGetComment( $news_activity_id )
    {
        
        $comments = PostComment::get( PostComment::NEWS_ACTIVITY, $news_activity_id );
        $pagination = $this->component->pagination( $comments, 6 );

        $comments   = $comments 
            ->with([
                'participant' => function (\yii\db\ActiveQuery $query) {
                    $query->select('id, username, fullname');
                }
            ])
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->asArray()
            ->all();

        $commentLikesIds = $this->activityService->getIdsByData( $comments, 'comment-like' );
        $result = [];
        foreach( $comments as $comment )
        {

            $likes = isset( $commentLikesIds[ $comment['id'] ] ) ? true : false;
            $result[] = $comment + [ 'like_status' => $likes ];

        }
        return [ 'pagination' => $pagination, 'comments' => $result ];
    }


    public function actionLike($id)
    {
        $newsActivity = NewsActivity::fetchOne($id);

        $saveActivity = $this->activityService->like($newsActivity->id);

        return true;
    }


    public function actionUnlike($id)
    {
        $newsActivity = NewsActivity::fetchOne($id);

        $saveActivity = $this->activityService->unlike($newsActivity->id);

        return true;
    }
}
