<?php
namespace api\controllers;

use Yii;

use common\models\Article, 
    common\models\PostComment,
    common\models\ArticleCategory,
    common\models\LogActivity,
    common\components\Functions,
    common\services\ActivityService;

use yii\web\NotFoundHttpException,
    yii\helpers\ArrayHelper;

/**
 * Article controller
 */

class ArticleController extends BaseController
{

    private $activityService;

    public function init()
    {
        parent::init();
        $this->activityService = new ActivityService( new Article, isset($this->user) ? $this->user->id : null );
    }


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'get-category'  => ['GET'],
            'get-comment'  => ['GET'],
            'detail'  => ['GET'],
            'comment' => ['POST']
        ];
    }


    /**
     * Get Categories
     * 
     * link  http://base.url/article/get-categories
     *
     * @return    json
     */
    public function actionGetCategories()
    {
        $categories = ArticleCategory::get();

        $categories   = $categories 
            ->select('id, name, image, image_dir')
            ->orderBy('name')
            ->asArray()
            ->all();

        $this->component->parseImage($categories, ['image']);

        return ['categories' => $categories ];
    }


    /**
     * Action List
     * 
     * @uses    api/components/BaseComponent/pagination()
     *
     */
    public function actionLists()
    {
        $articles = Article::fetch();

        if ( !empty($_GET[ 'category_id' ]) )
        {
            $articles = $articles->andWhere( [ 
                'article_category_id' => $_GET[ 'category_id' ] 
            ] );
        }

        $pagination = $this->component->pagination($articles, 6);

        $articles   = $articles 
            ->joinWith('category')
            ->orderBy('created_at DESC')
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->all();

        $datas = ArrayHelper::toArray($articles);

        $this->component->parseImage($datas, ['image']);

        // Get all article id that has user liked
        $likeStatus = $this->activityService->getIdsByData( $datas, 'like' );

        $result = [];
        foreach( $articles as $key => $value )
        {
            $datas[$key]['share_link'] = $articles[$key]->getShareLink();
            $datas[$key]['created_at'] = date('d F Y', $articles[$key]['created_at']);
            $datas[$key]['updated_at'] = date( 'd F Y');
            $datas[$key]['like_status'] = isset( $likeStatus[ $datas[$key]['id'] ] ) ? true : false;
        }
        
        return ['pagination' => $pagination, 'articles' => $datas ];
    }


    public function actionDetail($id)
    {

        $article = Article::fetch()->andWhere(['=', 'article.id', $id])
            ->joinWith('category')
            ->one();

        if ( empty($article) )
            throw new NotFoundHttpException();
        
        // convert from object to array
        $data = ArrayHelper::toArray($article,[
            'common\models\Article' => [
                'id',
                'title',
                'slug',
                'content',
                'image',
                'image_dir',
                'created_at'
            ]
        ]);

        $this->activityService->read($id);

        // Get user like status
        $likeStatus = $this->activityService->getIdsByData( [$data], 'like' );

        // $data['time']       = Functions::timeElapsed($data['created_at']);
        $data['created_at'] = date( 'd F Y', $data['created_at'] );
        $data['created_by'] = 'Admin Konicare';
        $data['share_link'] = $article->getShareLink();
        $data['like_status'] = isset( $likeStatus[ $data['id'] ] ) ? true : false;

        $this->component->parseImage($data, ['image']);
        
        return ['article' => $data];
    }


    public function actionLike($id)
    {
        $article = Article::fetchOne($id);

        $saveActivity = $this->activityService->like($article->id);

        return true;
    }


    public function actionUnlike($id)
    {
        $article = Article::fetchOne($id);

        $saveActivity = $this->activityService->unlike($article->id);

        return true;
    }


    public function actionShare($id)
    {
        $article = Article::fetchOne($id);

        $saveActivity = $this->activityService->share($article->id);

        return true;        
    }

    
    /**
     * Post Comment Article
     *
     * @return     json     Callback from PostComment model
     */
    public function actionComment()
    {
        
        $this->checkLogin();

        $post['PostComment'] = Yii::$app->request->post();
        $post['PostComment']['type']  = PostComment::ARTICLE;
        $post['PostComment']['participant_id'] = $this->user->id;

        $save = PostComment::saveData( new PostComment(), $post );
        if ( $save['status'] == true )
        {
            $this->activityService->comment($post['PostComment']['post_id']);
        } else {
            return [
                "error" => [ 
                    'message' => $save['message']
                ]
            ];
        }

        return $save;
    }
    

    /**
     * Get Comment List Based On Article Id
     *
     * @param      int  $article_id  The article identifier
     *
     * @uses    api/components/BaseComponent/pagination()
     * 
     * @return     json   the result contains pagination and comments
     */
    public function actionGetComment( $article_id )
    {
        
        $comments = PostComment::data( PostComment::ARTICLE, $article_id );
        $pagination = $this->component->pagination( $comments, 6 );

        $comments   = $comments 
            ->with([
                'participant' => function (\yii\db\ActiveQuery $query) {
                    $query->select('id, username, fullname, avatarimg');
                }
            ])
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->all();


        $result = [];
        foreach( $comments as $comment )
        {
            
            $participant = $comment['participant'];
            $result[] =  [
                'content' => $comment->content,
                'time' => Functions::timeElapsed($comment['created_at']),
                'participant' => [
                    'name' => $participant->username,
                    'avatar' => $participant->getAvatar('thumb_')
                ]
            ];

        }
        return [ 'pagination' => $pagination, 'comments' => $result ];
    }


    public function actionCommentLike( $id )
    {
        return $this->activityService->commentLike( $id );
    }
}
