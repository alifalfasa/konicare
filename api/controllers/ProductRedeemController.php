<?php
namespace api\controllers;

use Yii;

use api\models\ProductRedeem,
    backend\models\ProductRedeemDate,
    api\models\ParticipantRedeem,
    common\models\LogActivity,
    common\components\Functions,
    common\services\ActivityService;

use yii\web\NotFoundHttpException,
    yii\helpers\ArrayHelper;

/**
 * ProductRedeem controller
 */

class ProductRedeemController extends BaseController
{


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'history'  => ['GET'],
            'lists'  => ['GET'],
            'pick-up' => ['POST']
        ];
    }


    /**
     * List product can be redeem
     * 
     * @link  get: http://base.url/product-redeem/lists
     */
    public function actionLists()
    {

        // Get history redeem on this current day
        // $histories = ParticipantRedeem::find()
        //     ->andWhere( ['=', 'FROM_UNIXTIME(created_at,\'%Y-%m-%d\')', date('Y-m-d')] )
        //     ->andwhere( [ '=', 'participant_id', $this->user->id ] )
        //     ->all();
        // $productRedeemIds = ArrayHelper::getColumn( $histories, 'product_redeem_id' );

        $coupon1 = ProductRedeem::find()
            ->where(['=', 'product_name', 'Coupon Bunda Konicare Season 1'])
            ->asArray()
            ->one();

        $coupon2 = ProductRedeem::find()
            ->where(['=', 'product_name', 'Coupon Bunda Konicare Season 2'])
            ->asArray()
            ->one();

        $disabled_kupon = ProductRedeem::find()
            ->where(['=', 'product_name', 'Kupon tidak tersedia'])
            ->asArray()
            ->one();

        // array_push($productRedeemIds, $coupon1['id']);
        // Only show product has not been redeemed yet
        $products = ProductRedeem::fetch()
            ->andWhere([ 'not in', 'id', [$coupon2['id']] ])
            ->andWhere([ '>', 'stock', 0 ])
            ->andWhere([ '=', 'row_status', ProductRedeem::STATUS_ACTIVE ])
            ->orderBy('product_name')
            ->asArray()
            ->all();

        array_unshift($products, $disabled_kupon);

        /* $allowedDate = ProductRedeemDate::fetch()
            ->andWhere(['redeem_date' => date('Y-m-d')])
            ->andWhere(['row_status' => ProductRedeemDate::STATUS_ACTIVE])
            ->one();
            
        if ( !$allowedDate )
        {
            $products = [];
        } */
        // $products = [];
        return [ 'products' => $products ];
    }


    /**
     * Participant can only redeem product at the end of month
     * 
     * @param  product_redeem_ids  json  bulk of product redeem id
     * 
     * @link  post: http://base.url/product-redeem/pick-up
     * 
     * @return  json array
     */
    public function actionPickUp()
    {
        $this->checkLogin();

        // NANTI KALAU REDEEM POINTNYA UDAH DI TENTUIN
        // DI APUS AJA GAN script dibawah
        /*Yii::$app->response->statusCode = 422;
        
        return [ 
            'success' => false, 
            'error' => [
                'message' => 'Belum ada periode redeem point'
            ]
        ];*/

        $allowedDate = ProductRedeemDate::fetch()
            ->andWhere(['redeem_date' => date('Y-m-d')])
            ->andWhere(['row_status' => ProductRedeemDate::STATUS_ACTIVE])
            ->one();
            
        if ( $allowedDate )
        {
            $address          = Yii::$app->request->post('address');
            $productRedeemIds = Yii::$app->request->post('product_redeem_ids');
            $decodeIds = json_decode($productRedeemIds);

            $errorMessage = $successMessage = '';
            $successResult = $callback = [];
            $succesStatus = true;
            foreach( $decodeIds as $productRedeemId )
            {
                $pickUp = ProductRedeem::pickUp( $productRedeemId, $this->user->id, $address );
                if ( $pickUp['success'] == false )
                {

                    // Kumpulkan error message
                    $errorMessage .= $pickUp['error']['message'] . "\n";
                    $succesStatus = false;

                } else {

                    // $successMessage .= 'Product berhasil diredeem';
                    $callback[] = $pickUp['callback'];

                }
            }

            $errorResult = !empty($errorMessage) ? 
                [ 'error' => [ 'message' => $errorMessage] ] : 
                [ 'error' => [ 'message' => ''] ];

            $successResult = [
                // 'success' => [
                //     'message' => $successMessage
                // ],
                'success' => $succesStatus
            ];

            $callback = [
                'callback' => $callback
            ];
            return array_merge( $successResult, $errorResult, $callback );

        }

        Yii::$app->response->statusCode = 422;
        
        return [ 
            'success' => false, 
            'error' => [
                'message' => 'Redeem produk hanya bisa dilakukan pada tanggal yang telah ditentukan'
            ]
        ];
    }

    public function actionPickUpCoupon()
    {
        /* REMOVE THIS ON EVENT WITH COUPON */
        Yii::$app->response->statusCode = 422;
        
        return [ 
            'success' => false, 
            'error' => [
                'message' => 'Gagal redeem kupon.'
            ]
        ];
        /* - - = = - - */
        $this->checkLogin();

            $address          = '-';
            $productRedeemId = Yii::$app->request->post('product_redeem_ids');
            $quantity = Yii::$app->request->post('quantity');
            $decodeIds = json_decode($productRedeemId);

            $errorMessage = $successMessage = '';
            $successResult = $callback = [];
            $succesStatus = true;
            // for( $i = 0; $i < $quantity; $i++)
            // {
                $pickUp = ProductRedeem::pickUpCoupon( $productRedeemId, $quantity, $this->user->id, $address );
                // var_dump($pickUp);
                // exit();
                if ( $pickUp['success'] == false )
                {
                    // Kumpulkan error message
                    if(count($pickUp['error']['message']) > 0) {
                        foreach ($pickUp['error']['message'] as $msg) {
                            $errorMessage .= $msg . "\n";
                        }
                    }
                    $succesStatus = false;

                } else {

                    // $successMessage .= 'Product berhasil diredeem';
                    foreach ($pickUp['callback'] as $cb) {
                        $callback[] = $cb;
                    }

                }
            // }

            $errorResult = !empty($errorMessage) ? 
                [ 'error' => [ 'message' => $errorMessage] ] : 
                [ 'error' => [ 'message' => ''] ];

            $successResult = [
                // 'success' => [
                //     'message' => $successMessage
                // ],
                'success' => $succesStatus
            ];

            $callback = [
                'callback' => $callback
            ];
            return array_merge( $successResult, $errorResult, $callback );
    }


    public function actionHistories()
    {
        $this->checkLogin();

        $histories = ParticipantRedeem::find()
            ->where( [ '=', 'participant_id', $this->user->id ] )
            ->joinWith( 'product' )
            ->orderBy( 'id DESC' )
            ->all();

        $result = [];
        foreach( $histories as $history )
        {
            $result[] = [
                'date' => date( 'd M Y', $history->created_at ),
                'product' => $history->product->product_name
            ];
        }

        return $result;
    }
    public function actionCouponList()
    {
        $this->checkLogin();

        $histories = ParticipantRedeem::find()
            ->andWhere( [ '=', 'participant_id', $this->user->id ] )
            ->andWhere( [ '=', 'product_redeem.product_name', 'Coupon Bunda Konicare Season 2' ] )
            ->joinWith( 'product' )
            ->orderBy( 'id DESC' )
            ->all();

        $result = [];
        foreach( $histories as $history )
        {
            $result[] = [
                'date' => date( 'd M Y', $history->created_at ),
                'coupon' => strtoupper($history->product_code)
            ];
        }

        return $result;
    }
}
