<?php
namespace api\controllers;

use Yii;

use common\models\Province;
use common\models\Regency;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */

class LocationController extends BaseController
{

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
        ];
    }

    public function actionProvince($id = null)
    {
        if ( !empty( $id ) )
        {
            $query = Province::find()
                ->select( 'id, name' )
                ->andWhere(['=', 'id', $id])
                ->asArray()
                ->one();

        } else {

            $query = Province::find()
                ->orderBy( 'name' )
                ->asArray()
                ->all();
            // $query  = ArrayHelper::map($query, 'id', 'name');
        }

        $result = $query;
        return ['province' => $query];
        
    }

    public function actionRegency($provinceId, $regencyId = null)
    {
        if ( empty( $regencyId ) )
        {
            $regency = Regency::find()->andWhere( ['province_id' => $provinceId] )->all();
        } else {
            $regency = Regency::detail($regencyId);
        }

        return [
            'regency' => $regency
        ];
    }
}
