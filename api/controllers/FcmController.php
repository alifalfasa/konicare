<?php
namespace api\controllers;

use Yii;

use api\models\FcmDevice;

use yii\web\NotFoundHttpException,
    yii\helpers\ArrayHelper;

/**
 * Fcm controller
 */

class FcmController extends BaseController
{


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'register-device'  => ['POST'],
            'remove-device' => ['POST']
        ];
    }


    /**
     * Create new device and store into database
     * 
     * Pseudocode
     * request  ->  get http post
     * checkdevice ->  get device token using device token and participant_id
     * if checkdevice then
     *      return true
     * else
     *      if  save device is success then
     *          return true
     *      else
     *          return false and error message
     *          
     * @return     json
     */
    public function actionRegisterDevice()
    {

        $this->checkLogin();

        $request = Yii::$app->request;

        $checkDevice = FcmDevice::getDetail( 
            $request->post('device_token'), 
            $this->user->id 
        );
        if ( !empty($checkDevice) ) return [ 'status' => true ];

        $model = new FcmDevice();
        $model->device_token    = $request->post('device_token');
        $model->participant_id  = $this->user->id;

        if ( $model->save() ) return [ 'status' => true ];

        Yii::$app->response->statusCode = 422;

        return [ 
            'status' => false,
            'error' => [
                'message' => $model::getError($model) 
            ]
        ];

    }


    /**
     * Remove Device from database
     * 
     * @return     json
     */
    public function actionRemoveDevice()
    {
        $this->checkLogin();
        $request = Yii::$app->request;
        $model = FcmDevice::getDetail( 
            $request->post('device_token'), 
            $this->user->id 
        );

        if ( empty( $model ) )
        {
            return [ 'status' => true, 'no-device' => true ];
        }

        $model->delete();

        return [ 'status' => true ];
    }

}
