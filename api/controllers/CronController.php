<?php
namespace api\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;

use common\models\Article;
use common\models\Immunization;
use common\models\ImmunizationSchedule;
use common\models\Participant;
use common\models\LogActivity;

use api\models\FcmDevice;
use api\models\FcmNotification as Notification;
use api\models\Cronjob;
use api\models\ParticipantRedeem;
use api\services\FcmService;

/**
 * Cronjob controller
 */
class CronController extends ActiveController
{

    public $modelClass = 'api\models\Auth';



    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'fcm-send' => ['post'],
            'reset-participant-point' => ['post'],
            'check-immunization-today' => ['post'],
            'check-immunization-tomorrow' => ['post']
        ];
    }


    public function actionSendLatestArticle()
    {

    	$actionSlug = 'send-latest-article';

		$checkCronjob = Cronjob::find()
			->andWhere( [ 'action' => $actionSlug ] )
			->andWhere( [ 'date' => date('Y-m-d') ] )
			->count();

		// Apakah cronjob sudah dijalankan pada hari ini?
		if ( $checkCronjob == 0 )
		{
	    	$articles = Article::get()
	    		->andWhere( ['>', 'created_at', strtotime('-2 day')] )
	    		->all();

	    	if ( !empty($articles) ) 
	    	{
	    		foreach( $articles as $article ) {

	    			// Store to notification table
	    			$notification = new Notification();
					$notification->post_id = $article->id;
					$notification->type = 'article';
					$notification->title = $article->title;
					$notification->body  = $article->subcontent;
					$notification->message  = 'Latest Article';
					$notification->participant_id = 0; // 0 means is all devices will receive notification
					$notification->created_at = strtotime('now');
					$notification->save();
	    		}

	    		// Send to device
		    	$fcm = new FcmService();
		    	$result = $fcm->send( [
		    		'body' => 'Halo bunda sudahkah anda membaca artikel baru kami?',
		    		'title' => 'Artikel Baru Bundakonicare'
		    	], $fcm->getAllDevices() );
	
				// Save log cronjob
				$cronjob = new Cronjob();
				$cronjob->action = $actionSlug;
				$cronjob->date   = date( 'Y-m-d' );
				$cronjob->save();

	    		return json_decode($result);
	    	} else {
	    		return [
	    			'success' => [
	    				'message' => 'Tidak ada artikel'
	    			]
	    		];
	    	}


	    } else {
			return [ 'error' => [ 'message' => 'Cronjob has been run' ] ];
		}
    }


    /**
     * Jadwal imunisasi hari ini
     * 
     * All of steps:
     * 1. Check cronjob schedule
     * 2. Get All Schedule
     * 3. Get participant Id which have schedule to immunization
     * 4. Get device participant
     * 5. Stode to database for notification
     * 6. Send notification to participant device 
     *
     * Note: 
     * For the sake rapidity development
     * This function not written as good practice, 
     * better you refactor code with DRY style (if you able). 
     * So that can be related with `actionCheckImmunizationTomorrow` function
     * 
     * @return  json
     */
	public function actionCheckImmunizationToday()
	{	
		$actionSlug = 'check-immunization-today';

		$checkCronjob = Cronjob::find()
			->andWhere( [ 'action' => $actionSlug ] )
			->andWhere( [ 'date' => date('Y-m-d') ] )
			->count();

		// Apakah cronjob sudah dijalankan pada hari ini?
		if ( $checkCronjob == 0 )
		{

			// Get Schedule Immunization First
			$now = date('Y-m-d');
			$schedules = \common\models\ImmunizationSchedule::find()
				->joinWith([
					'immunizationCategory',
					'participant'
				])
				->andWhere( ['=', 'date', $now])
				->asArray()
				->all();

			// Get participant id from schedule of immunization
			$participantIds = ArrayHelper::getColumn($schedules, 'participant.id');

			// Get devices of participant using participantId 
			$devices = FcmDevice::find()
				->andWhere( ['participant_id' => $participantIds] )
				->all();
			$devices = ArrayHelper::index($devices, 'participant_id');

			$result = '';
	    	$fcm = new FcmService();
			foreach( $schedules as $schedule )
			{

				// Define data to be sent to participant
				$device = $devices[ $schedule[ 'participant' ][ 'id' ] ];
				$title = 'Jadwal Imunisasi '. $schedule[ 'immunization' ][ 'children_name' ];
				$body  = 'Hari ini ada jadwal imunisasi ' . $schedule[ 'immunizationCategory' ][ 'name' ];

				// Store notification data to database
				$notification = new Notification();
				$notification->post_id = $schedule[ 'immunization' ][ 'id' ];
				$notification->type = 'immunization';
				$notification->title = $title;
				$notification->body  = $body;
				$notification->message  = 'Send to device: ' . $device['device_token'];
				$notification->participant_id = $schedule[ 'participant' ][ 'id' ];
				$notification->created_at = strtotime('now');
				if ( $notification->save() )
				{
					// serialize data
					$data = [
						'title' => $title,
						'body' 	=> $body
					];

					// Send to fcm
					$result = $fcm->send( $data, [$device['device_token']] );
					
				} else {

					// result error
					$result = json_encode($notification->getErrors());
				}

			}

			// Save log cronjob
			$cronjob = new Cronjob();
			$cronjob->action = $actionSlug;
			$cronjob->date   = date( 'Y-m-d' );
			$cronjob->save();

			return json_decode( $result );
		} else {
			return [ 'error' => [ 'message' => 'Cronjob has been run' ] ];
		}
	}


    /**
     * Jadwal imunisasi esok hari
     * 
     * All of steps:
     * 1. Check cronjob schedule
     * 2. Get All Schedule
     * 3. Get participant Id which have schedule to immunization
     * 4. Get device participant
     * 5. Stode to database for notification
     * 6. Send notification to participant device 
     *
     * Note: 
     * For the sake rapidity development
     * This function not written as good practice, 
     * better you refactor code with DRY style (if you able). 
     * So that can be related with `actionCheckImmunizationToday` function
     * 
     * @return  json
     */
	public function actionCheckImmunizationTomorrow()
	{
		$actionSlug = 'check-immunization-tomorrow';

		$checkCronjob = Cronjob::find()
			->andWhere( [ 'action' => $actionSlug ] )
			->andWhere( [ 'date' => date('Y-m-d') ] )
			->count();

		// Apakah cronjob sudah dijalankan pada hari ini?
		if ( $checkCronjob == 0 )
		{
			$tomorrow = date('Y-m-d', strtotime('+1 day'));
			$schedules = \common\models\ImmunizationSchedule::find()
				->joinWith([
					'immunizationCategory',
					'participant'
				])
				->andWhere( ['=', 'date', $tomorrow])
				->asArray()
				->all();

			// Get participant id from schedule of immunization
			$participantIds = ArrayHelper::getColumn($schedules, 'participant.id');

			// Get devices of participant using participantId 
			$devices = FcmDevice::find()
				->andWhere( ['participant_id' => $participantIds] )
				->all();
			$devices = ArrayHelper::index($devices, 'participant_id');

			$result = '';
	    	$fcm = new FcmService();
			foreach( $schedules as $schedule )
			{

				// Define data to be sent to participant
				$device = $devices[ $schedule[ 'participant' ][ 'id' ] ];
				$title = 'Jadwal Imunisasi '. $schedule[ 'immunization' ][ 'children_name' ];
				$body  = 'Esok hari akan ada jadwal imunisasi ' . $schedule[ 'immunizationCategory' ][ 'name' ];

				// Store notification data to database
				$notification = new Notification();
				$notification->post_id 	= $schedule[ 'immunization' ][ 'id' ];
				$notification->type 	= 'immunization';
				$notification->title 	= $title;
				$notification->body 	= $body;
				$notification->message  = 'Send to device: ' . $device['device_token'];
				$notification->participant_id = $schedule[ 'participant' ][ 'id' ];
				$notification->created_at = strtotime('now');
				if ( $notification->save() )
				{
					// serialize data
					$data = [
						'title' => $title,
						'body' 	=> $body
					];

					// Send to fcm
					$result = $fcm->send( $data, [$device['device_token']] );
					
				} else {

					// result error
					$result = json_encode($notification->getErrors());
				}

			}

			return json_decode( $result );

		} else {
			return [ 'error' => [ 'message' => 'Cronjob has been run' ] ];
		}

	}


	/**
	 * Note: Jalankan reset point ini pada tanggal 1 disetiap bulan
	 * 
	 * @return  mixed
	 */
	public function actionResetParticipantPoint()
	{
		$actionSlug = 'reset-participant-point';
		$currentDay = date('j');

		// Aoakah sekarang tanggal 1 ?
		if ( $currentDay == 1 )
		{
			$checkCronjob = Cronjob::find()
				->andWhere( [ 'action' => $actionSlug ] )
				->andWhere( [ 'date' => date('Y-m-d') ] )
				->count();

			// Apakah cronjob sudah dijalankan pada hari ini?
			if ( $checkCronjob == 0 )
			{
				$participants = Participant::find()
					->andWhere( [ '>', 'points', 0 ] )
					->all();

				$result = $errors = [];
				foreach( $participants as $participant )
				{
			
			        $activity = new LogActivity();
			        $activity->type           = 'participant';
			        $activity->action         = 'reset-point';
			        $activity->post_id        = $participant->id;
			        $activity->participant_id = $participant->id;
			        $activity->point          = -$participant->points;
			        
			        if ( $activity->save() )
			        {
			        	$participant->points = 0;
			        	$participant->save();
			        } else {
			        	$errors[] = $activity->getErrors();
			        }
				}

				// Save log cronjob
				$cronjob = new Cronjob();
				$cronjob->action = $actionSlug;
				$cronjob->date   = date( 'Y-m-d' );
				$cronjob->save();

				$success = empty($errors) ? true : false;

				// Result bila cronjob berhasil dijalankan	
				return [ 
					'success' => $success, 
					'error' => [ 
						'message' => $success
					]
				];	

			} else {

				// Result bila cronjob sudah dijalankan
				return [ 
					'success' => false, 
					'error' => [ 
						'message' => 'Cronjob has been run'
					]
				];

			}


		} else {

			// Result bila cronjob dijalankan selain tanggal 1
			return [ 
				'success' => false, 
				'error' => [ 
					'message' => 'Not this day'
				]
			];

		}

	}

	/**
	 * Note: Jalankan decrease point anomaly dan tandai redeem
	 * 
	 * @return  mixed
	 */
	public function actionResetAnomalyPoint()
	{
		$actionSlug = 'reset-anomaly-point';
		$date = strtotime('2019-08-22');
		$anomalyUsers = LogActivity::find()
						->select(['sum(point) as jumlah, participant_id','FROM_UNIXTIME(created_at, \'%d-%m-%y\' ) as tanggal'])
						->andWhere(['>', 'created_at', $date ])
						->andWhere(['<>', 'type', 'product-redeem' ])
						->groupBy('participant_id, tanggal')
						->having('sum(point) > 200')
						->orderBy('created_at desc')
						->asArray()
						->all();
		$userid = [];
		$user_point;
		$aa = [];
		foreach ($anomalyUsers as $user) {
			if(!in_array($user['participant_id'], $userid)) { array_push($userid, $user['participant_id']); }
			$correction = new LogActivity;
			$correction->type           = 'points';
            $correction->action         = 'decrease';
            $correction->post_id        = 0;
            $correction->participant_id = $user['participant_id'];
            $correction->point          = 100-$user['jumlah'];
            $correction->save();
		}
		$redeems;
		foreach ($userid as $id) {
			
			// if($id == '4044'){
	            $participant = Participant::fetch()
	            ->andWhere([ 'participant.id' => $id ])
	            ->one();

	        	// $participantAcitivityPoint = $participant->updatePointFromActivity();
	        	$point2019 = LogActivity::find()
				            ->andWhere(['participant_id' => $id])
				            ->andWhere(['<>', 'type', 'product-redeem'])
							->andWhere(['>', 'created_at', $date ])
				            ->sum('point');
	        	$point2018 = LogActivity::find()
				            ->andWhere(['participant_id' => $id])
							->andWhere(['<', 'created_at', $date ])
				            ->sum('point');
	            $point = intval($point2019) + intval($point2018);

	            $redeems = ParticipantRedeem::find()
	            			->where(['participant_id' => $participant->id])
							->andWhere(['>', 'created_at', $date ])
	            			->orderBy('id')
	            			->all();
	            	foreach ($redeems as $redeem) {
	            		if($point < $redeem->point){
	            			$changeRedeem = ParticipantRedeem::find()
	            							->andWhere(['id' => $redeem->id])
	            							->one();
	            			$changeRedeem->address = $changeRedeem->address . ' (anomaly)';
	            			$changeRedeem->save();

	            			$cancel = new LogActivity;
	            			$cancel->type           = 'cancel-redeem';
				            $cancel->action         = 'decrease';
				            $cancel->post_id        = $redeem->id;
				            $cancel->participant_id = $redeem->participant_id;
				            $cancel->point          = $redeem->point;
				            // $cancel->save();
	            		}
	            		$point = $point - $redeem->point;
	            	}
        		// }
	            	$participant->updatePointFromActivity();
		}

		return [  
			'data' => $userid
		];

	}
	/**
	 * Note: Jalankan decrease point anomaly dan tandai redeem
	 * 
	 * @return  mixed
	 */
	public function actionDecreaseExcessPoints()
	{
		$actionSlug = 'reset-anomaly-point';
		$date = strtotime(date('Y-m-d'));
		$anomalyUsers = LogActivity::find()
						->select(['sum(point) as jumlah, participant_id','FROM_UNIXTIME(created_at, \'%d-%m-%y\' ) as tanggal'])
						->andWhere(['>', 'created_at', $date ])
						->andWhere(['<>', 'type', 'product-redeem' ])
						->groupBy('participant_id, tanggal')
						->having('sum(point) > 200')
						->orderBy('created_at desc')
						->asArray()
						->all();
		$userid = [];
		$user_point;
		$aa = [];
		foreach ($anomalyUsers as $user) {
			if(!in_array($user['participant_id'], $userid)) { array_push($userid, $user['participant_id']); }
			$correction = new LogActivity;
			$correction->type           = 'points';
            $correction->action         = 'decrease';
            $correction->post_id        = 0;
            $correction->participant_id = $user['participant_id'];
            $correction->point          = 100-$user['jumlah'];
            $correction->save();
		}
		$redeems;
		foreach ($userid as $id) {
			
			// if($id == '4044'){
	            $participant = Participant::fetch()
	            ->andWhere([ 'participant.id' => $id ])
	            ->one();

	        	// $participantAcitivityPoint = $participant->updatePointFromActivity();
	        	$point2019 = LogActivity::find()
				            ->andWhere(['participant_id' => $id])
				            ->andWhere(['<>', 'type', 'product-redeem'])
							->andWhere(['>', 'created_at', $date ])
				            ->sum('point');
	        	$point2018 = LogActivity::find()
				            ->andWhere(['participant_id' => $id])
							->andWhere(['<', 'created_at', $date ])
				            ->sum('point');
	            $point = intval($point2019) + intval($point2018);

	            $redeems = ParticipantRedeem::find()
	            			->where(['participant_id' => $participant->id])
							->andWhere(['>', 'created_at', $date ])
	            			->orderBy('id')
	            			->all();
	            	foreach ($redeems as $redeem) {
	            		if($point < $redeem->point){
	            			$changeRedeem = ParticipantRedeem::find()
	            							->andWhere(['id' => $redeem->id])
	            							->one();
	            			$changeRedeem->address = $changeRedeem->address . ' (anomaly)';
	            			$changeRedeem->save();

	            			$cancel = new LogActivity;
	            			$cancel->type           = 'cancel-redeem';
				            $cancel->action         = 'decrease';
				            $cancel->post_id        = $redeem->id;
				            $cancel->participant_id = $redeem->participant_id;
				            $cancel->point          = $redeem->point;
				            // $cancel->save();
	            		}
	            		$point = $point - $redeem->point;
	            	}
        		// }
	            	$participant->updatePointFromActivity();
		}

		return [  
			'data' => $userid
		];

	}

}