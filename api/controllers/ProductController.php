<?php
namespace api\controllers;

use Yii;

use api\models\Product,
    common\components\Functions,
    common\services\ActivityService;

use yii\web\NotFoundHttpException,
    yii\helpers\ArrayHelper;

/**
 * Product controller
 */

class ProductController extends BaseController
{

    public function init()
    {
        parent::init();
    }


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'lists'  => ['GET'],
        ];
    }


    /**
     * Action List
     * 
     * @uses    api/components/BaseComponent/pagination()
     *
     */
    public function actionLists()
    {
        $products = Product::fetch();

        $pagination = $this->component->pagination($products, 20);

        $products   = $products 
            ->offset( $pagination->offset )
            ->limit(  $pagination->limit  )
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $result = [];
        foreach( $products as $key => $product )
        {
            $result[$key] = $product;
            $result[$key][ 'image' ] = BASE_URL . 'media/product/' . $product['id'] . '/' . $product['image'];
        }

        return ['pagination' => $pagination, 'products' => $result ];
    }


    public function actionDetail( $id )
    {
        $product = Product::getOne( $id );

        $image = BASE_URL . 'media/product/' . $id . '/' . $product->image;

        $result = [
            'id' => $id,
            'name' => $product->name,
            'content' => $product->content,
            'image' => $image,
            'store_url' => $product->store_url,
            'tvc' => $product->tvc
        ];
        return $result;
    }
}
