<?php
namespace api\controllers;

use Yii;

use common\models\Immunization;
use common\models\ImmunizationCategory;

use common\services\ImmunizationService;

/**
 * Immunization Controller
 */

class ImmunizationController extends BaseController
{

    private $immunizationService;

    public function init()
    {
        parent::init();
        
        $this->checkLogin();

        $this->immunizationService = new ImmunizationService( $this->user->id );
    }


    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'save' => ['post'], 
            'delete' => ['post'], 
        ];
    }


    public function actionGetChildrens()
    {
        $childrens = Immunization::getChildrens( $this->user->id );

        $order = [ 'Pertama', 'Kedua', 'Ketiga'];
        $result = [];
        foreach( $childrens as $key => $children )
        {
            $result[] = [
                'id' => $children->id,
                'name' => $children->children_name,
                'birthdate' => $children->birthdate,
                'order' => 'Anak ' . $order[ $key ]
            ];
        }
        return $result;
    }


    public function actionSave()
    {
        $datas = json_decode(Yii::$app->request->post('data'));

        $childrenCount = Immunization::getChildrenCount( $this->user->id );

        if ( (count( $datas ) + $childrenCount) > 3 ) 
            return [ 
                'status' => false,
                'error' => [
                    'message' => 'Jumlah imunisasi anak maksimal hanya 3'
                ]
            ];

        $errorMessage = $successMessage = '';
        $result = [];
        $i = 0;
        foreach( $datas as $data )
        {
            ++$i;

            $post = (array) $data;
            $save = $this->immunizationService->save( [ 'Immunization' => $post ] );
            if ( $save['status'] == false ):
                $errorMessage .= 'Anak ' . $i . ":\n" . $save['message'];
            else:
                $successMessage .= 'Anak ' . $i . ": Berhasil\n";
            endif;
        }

        $error = !empty($errorMessage) ? [ 'error' => [ 'message' => $errorMessage] ] : [];
        $result = [
            'status' => true,
            'message' => 'Success',
        ];

        return array_merge( $result, $error );
    }


    public function actionSchedules()
    {
        $post = Yii::$app->request->post();

        if ( empty( $post[ 'year'] ) ) 
            return [ 'error' => [ 'message' => '\'year\' parameter is not defined' ] ];

        if ( empty( $post[ 'month'] ) ) 
            return [ 'error' => [ 'message' => '\'month\' parameter is not defined' ] ];

        $datas = $this->immunizationService->getAllSchedules( 
            $post['year'], 
            $post['month'] 
        );

        return $datas;
    }


    public function actionCategories()
    {
        $data = ImmunizationCategory::get()
            ->select(['id','name','description'])
            ->orderBy('name ASC')
            ->all();

        return $data;
    }

    public function actionCategoryDetail( $id )
    {
        $data = ImmunizationCategory::get()
            ->select(['id','name','subcontent','description'])
            ->andWhere( ['id' => $id] )
            ->one();

        return $data;
    }


    public function actionRemove( $id )
    {
        return $this->immunizationService->remove( $id );
    }

}
