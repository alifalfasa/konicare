<?php
namespace api\services;

use api\models\Auth;

use api\models\Participant;

class AuthService {
	
	
	private $__clientId;
	private $__participant;
	private $__token;

	public function setClientId( $id )
	{
		$this->__clientId = $id;
	}


	public function setParticipant( $participant )
	{
		$this->__participant = $participant;
	}


	public function saveToken()
	{
		if ( empty( $this->__participant ) )
			$participantId = null;
		else
			$participantId = $this->__participant->id;

		$this->__token = Auth::saveAccessToken( $this->__clientId, $participantId );

	}

	public function response()
	{
		if ( empty( $this->__participant ) )
			return [
				'token' => $this->__token
			];

		$participant = $this->__participant;
		$participantSerialized = [
            'username' => $participant->username,
            'email' => $participant->email,
            'fullname' => $participant->fullname,
            'birthdate' => $participant->birthdate,
            'notelp' => $participant->notelp,
            'nohp' => $participant->nohp,
            'address' => $participant->address,
            'regency_id' => $participant->regency_id,
            'province_id' => $participant->province_id,
            'zip' => $participant->zip,
            'avatar' => [ 
                'original' => $participant->getAvatar(),
                'thumbnail' => $participant->getAvatar( 'thumb' ),
            ]
        ];
        return [
            'token' => $this->__token,
            'is_register' => true,
            'participant' => $participantSerialized
        ];   
	}
}