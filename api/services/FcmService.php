<?php 
namespace api\services;

use yii\helpers\ArrayHelper;

use api\models\FcmDevice;

class FcmService 
{
	
	public function getAllDevices()
	{
    	$devices = FcmDevice::getAllTokens();
    	$device_tokens = ArrayHelper::getColumn($devices, 'device_token');

    	return $device_tokens;
	}


	public function broadcast( $data )
	{
		return $this->send( $data, $this->getAllDevices() );
	}
	
	public function send( $data, $ids = null )
	{

		$url = 'https://fcm.googleapis.com/fcm/send';

	    $fields = [
            'registration_ids' => $ids,
            'data' => $data
	    ];
		
	    $encodeFields = json_encode ( $fields );

	    $headers = array (
	            'Authorization: key=' . "AAAArR4cuwI:APA91bHxvUn5PO31w2eOW99LYrlk34iJFiHr-lmA7iaN6-p6Ou-L32vSVc4-jED7CU_xu4ygO7Wg8Txmga3Ki7jnkRyqIFMlSAVn3yku57KaZKdhXf22z_m7gvKf2JOwTxCJ9C1kyQ0f",
	            'Content-Type: application/json'
	    );

	    $ch = curl_init ();
	    curl_setopt ( $ch, CURLOPT_URL, $url );
	    curl_setopt ( $ch, CURLOPT_POST, true );
	    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $encodeFields );

	    $result = curl_exec ( $ch );
	    curl_close ( $ch );
	    return $result;
	}
}