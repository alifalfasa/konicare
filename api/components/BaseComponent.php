<?php

namespace api\components;

use Yii;
use yii\base\Component,
	yii\base\InvalidConfigException,
	yii\base\Application,
	yii\helpers\Url,
	yii\helpers\ArrayHelper,
	yii\helpers\StringHelper;

/**
 * Base Component
 * 
 * @method formFilter()
 * @method updateFilter()
 * 
 */
class BaseComponent extends Component
{

	/**
	 * FormFilter
	 *
	 * @param      array                  $post    data of $_POST method
	 * @param      array                  $allows  parameter allowed
	 *
	 * @throws     \yii\web\HttpException  jika ada parameter yang tidak di izinkan
	 *
	 * @return     array
	 */
	public function formFilter( $post, $allows )
	{
		$result = [];

		foreach( $post as $key => $value )
		{
						
			if (!in_array($key, $allows))
			{
				throw new \yii\web\HttpException(422, 'Paramater "' . $key . '" not allowed');
			}

			$result[$key] = $value;

		}

		return $result;
	}


	/**
	 * Update Filter
	 *
	 * @param      object                  $model  The model
	 *
	 * @throws     \yii\web\HttpException  (description)
	 * 
	 * @uses 		updateFilter() 			method pada model
	 * 
	 * @return     <type>                  ( description_of_the_return_value )
	 */
	public function updateFilter($model)
	{

		$post = Yii::$app->request->post();

		$modelName = StringHelper::basename(get_class($model));
		$result[ $modelName ] = [];

		foreach( $post as $key => $value )
		{
						
			if (!in_array($key, $model->updateFilter()))
			{
				throw new \yii\web\HttpException(422, 'Paramater "' . $key . '" not allowed');
			}

			$result[ $modelName ][$key] = $value;

		}

		return $result;

	}


	/**
	 * Pagination
	 *
	 * @param      object   $model  Model
	 * @param      integer  $limit  The limit
	 *
	 * @return     object
	 */
	public function pagination( $model, $limit = 10 )
	{
        
        $countQuery = clone $model;
        $totalData  = $countQuery->count();
        $page = 1;
        $offset = 0;

        $totalPages = ceil($totalData / $limit);
        if (isset($_GET['page']))
        {
            $page = $_GET['page'];
            $offset = ($page  - 1) * $limit;

        }

        $pagination = [
            'offset' => $offset,
            'limit' => $limit,
            'totalData' => $totalData,
            'maxPage' => $totalPages,
            'currentPage' => $page
        ];

		return (object) $pagination;
	}

	/**
	 * Parse Image 
	 * Untuk mengenerate image harus menggunakan fitur dimedia uploader.
	 * Fungsi ini belom dapat menghandle upload secara manual
	 *
	 * @param      array  $datas   The datas
	 * @param      array  $fields  The fields
	 */
	public function parseImage( &$datas, $fields )
	{
		if ( is_array($datas) ){
			foreach($datas as $key => $data) {

				foreach( $fields as $field ) {

					// Bila menggunakan array atau ngehandle foreach
					if ( isset($data[$field]) )
					{
						$dir   = BASE_URL . $data[$field . '_dir'];
						$image = $data[$field];

						$datas[$key][$field . '_original'] = $dir . $image;
						$datas[$key][$field . '_thumb'] = $dir .'thumb_' . $image;
						$datas[$key][$field . '_normal'] = $dir .'normal_' . $image;

						unset($datas[$key][$field]);
						unset($datas[$key][$field . '_dir']);

					} elseif ( $key == $field ) {

						$dir   = BASE_URL . $datas[$key . '_dir'];
						$image = $datas[$key];

						$datas[$key . '_original'] = $dir . $image;
						$datas[$key . '_thumb']    = $dir .'thumb_' . $image;
						$datas[$key . '_normal']   = $dir .'normal_' . $image;

						unset($datas[$key]);
						unset($datas[$key . '_dir']);

					}					
				} 
			}
		}
	}
}