<?php
namespace frontend\models;

use Yii;
use common\models\Participant;

/**
 * Signin form
 */
class ForgotPassword extends \common\models\BaseModel
{

    public $email, $newPassword, $rePassword;
    private $_user;

    public static function tableName()
    {
        return 'participant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message' => '{attribute} tidak boleh kosong', 'on' => 'forgot-password'],
            ['email', 'email', 'on' => 'forgot-password'],
            ['email', 'checkEmail', 'on' => 'forgot-password'],
            [['newPassword','rePassword'], 'required' , 'on' => 're-activated'],
            ['rePassword', 'passwordCompare', 'on' => 're-activated'],
            [ ['newPassword','rePassword'], 'string', 'min' => 6 ],
        ];
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * 
     * @return bool if password provided is valid for current user
     */
    public function checkEmail($attribute, $password)
    {

        $user = Participant::find()->andWhere(['email' => $this->email, 'row_status' => 1])->one();
        if (empty($user)) {
            $this->addError($attribute, 'Email anda tidak ditemukan.');
            return false;
        }
        return true;

    }


    public function passwordCompare($attribute)
    {
        if ($this->newPassword !== $this->rePassword)
        {
            $this->addError( $attribute, 'Your new password and re-password is not equal');
            return false;
        }
        return true;
    }


}
