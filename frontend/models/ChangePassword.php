<?php
namespace frontend\models;

use Yii;

/**
 * Change Password form
 */
class ChangePassword extends \common\models\Participant
{

    public $newPassword, $oldPassword, $rePassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['oldPassword', 'checkPassword'],
            [['oldPassword', 'newPassword','rePassword'], 'required'],
            ['rePassword', 'passwordCompare'],
            [ ['newPassword','rePassword'], 'string', 'min' => 6 ],
        ];
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * 
     * @return bool if password provided is valid for current user
     */
    public function checkPassword($attribute, $password)
    {
        if ( Yii::$app->security->validatePassword($this->oldPassword, $this->password) )
        {
            return true;
        }

        $this->addError($attribute, 'Password lama yang anda masukan salah');
        return false;

    }


    public function passwordCompare($attribute)
    {
        if ($this->newPassword !== $this->rePassword)
        {
            $this->addError( $attribute, 'Your new password and re-password is not equal');
            return false;
        }
        return true;
    }


}
