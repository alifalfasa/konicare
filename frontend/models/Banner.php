<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $link
 * @property string $image_dir
 * @property string $image
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Banner extends \common\models\BaseModel
{

    public static function home()
    {
        return static::get()->orderBy('id DESC')->limit(5)->all();
    }
}
