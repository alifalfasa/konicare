<?php
namespace frontend\models;

use Yii;
use common\models\Participant;

/**
 * Signin form
 */
class SigninForm extends \common\models\BaseModel
{
    public $id;
    public $authKey;
    public $username;
    public $password;
    public $rememberMe;
    public $oldMember = false;

    private $_user;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participant';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username','password'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword']
            //hint not required
        ];
    }


    /**
     * Logs in a user using the provided username and password.
     * 
     * @used_by     /frontend/controllers/ParticipantController::Login()
     * 
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->_user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
        
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * 
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($attribute, $password)
    {
        if (!$this->hasErrors()) {

            $user = $this->getParticipant();
            if ( !empty($user) && empty($user->password) )
            {
                $this->addError('username', null);
                $this->addError($attribute, 'Password anda harus diubah, silahkan masuk ke forgot password');
                $this->oldMember = true;
                return false;
            }
            
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('username', null);
                $this->addError($attribute, 'Password yang anda masukan salah.');
            }

        }

    }


    /**
     * Finds user by [[username]]
     *
     * @return Participant|null
     */
    public function getParticipant()
    {
        if ($this->_user === null) {
            $this->_user = Participant::get()->andWhere(['or', 
                [ 'username' => $this->username ],
                [ 'email'    => $this->username ],
            ])->one();
        }

        return $this->_user;
    }

}
