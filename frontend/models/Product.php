<?php

namespace frontend\models;

use Yii;

class Product extends \common\models\BaseModel
{

    public static function all()
    {
    	return static::fetch()->orderBy('name')->all();
    }
}
