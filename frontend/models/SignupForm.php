<?php
namespace frontend\models;

use yii\base\Model;
use common\models\Participant;

/**
 * Signup form
 */
class SignupForm extends Participant
{

    public $username,
           $email,
           $password,
           $usertype,
           $confirmPassword,
           $fullname,
           $gender,
           $fbuserid,
           $birthdate,
           $nohp,
           $zip,
           $address,
           $province_id,
           $regency_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'trim'],

            ['username', 'characterValidation'],
            ['username', 'unique', 'targetClass' => '\common\models\Participant', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 3, 'max' => 50],

            ['email', 'email'],
            ['email', 'string', 'max' => 120],
            ['email', 'unique', 'targetClass' => '\common\models\Participant', 'message' => 'This email address has already been taken.'],

            [['email', 'username'], 'required'],
            [['fullname', 'gender', 'birthdate', 'nohp', 'address'], 'required', 'on' => 'step2'],

            [['password','confirmPassword'], 'required', 'on' => 'step1'],
            ['confirmPassword', 'comparePassword', 'on' => 'step1'],
            [['password','confirmPassword'], 'string', 'min' => 6],
        ];
    }


    /**
     * Compare Password
     *
     * @return     boolean      true\false
     */
    public function comparePassword( $attribute )
    {
        if ( $this->confirmPassword !== $this->password )
        {
            $this->addError( $attribute, 'Your password and confirm passwords is not equal' );
            return false;
        }
        return true;
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        $participant = new Participant();
        $participant->fbuserid  = $this->fbuserid;
        $participant->username  = $this->username;
        $participant->usertype  = $this->usertype;
        $participant->email     = $this->email;
        $participant->gender    = $this->gender;
        $participant->fullname  = $this->fullname;
        $participant->birthdate = $this->birthdate;
        $participant->notelp    = $this->notelp;
        $participant->nohp      = $this->nohp;
        $participant->address   = $this->address;
        $participant->regency_id  = $this->regency_id;
        $participant->province_id = $this->province_id;
        $participant->country     = 'Indonesia';
        $participant->zip         = $this->zip;
        $participant->row_status  = 1;

        if ( empty($this->fbuserid) )
        {
            $participant->row_status  = 0;
            $participant->setPassword($this->password);
        }

        $participant->generateAuthKey();
        // var_dump($participant->validate());

        if ( $participant->save() )
        {
            $participant->id = $participant->id;
            
            return $participant;
        } else {
            return $participant->getErrors();
        }

    }
}
