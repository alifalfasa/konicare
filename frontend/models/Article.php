<?php

namespace frontend\models;

use Yii;

class Article extends \common\models\Article
{

	public function getLikeUrl()
	{
		return BASE_URL . 'article/' . $this->id . '/like';
	}
}
