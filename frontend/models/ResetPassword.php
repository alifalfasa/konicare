<?php
namespace frontend\models;

use Yii;

class ResetPassword extends \common\models\Participant
{

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword', 'confirmPassword'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            ['oldPassword', 'oldPasswordValidation'],
            ['oldPassword', 'validatePassword']
        ];
    }


    public function oldPasswordValidation($attribute, $value)
    {
        if ($this->validatePassword($this->oldPassword))
        {
            return true;
        }
        $this->addError( $attribute, 'Your old password is invalid');
        return false;
    }


}
