<?php

namespace frontend\components;

use Yii;
use yii\bootstrap\ActiveForm,
	yii\helpers\Html,
	yii\helpers\BaseStringHelper as StringHelper,
	yii\helpers\Url;

use common\services\SettingService;

class View extends \yii\web\View
{

	public $meta,
		   $title,
		   $instagramUrl,
		   $facebookUrl,
		   $facebookId,
		   $currentMenu,
		   $user,
		   $twitterUrl;

	public function __construct()
	{
		parent::__construct();

		$service = new SettingService;

		/** These variables used by /frontend/views/layouts/main.twig */
		$this->instagramUrl = $service->getValue('instagram');
		$this->twitterUrl 	= $service->getValue('twitter');
		$this->facebookUrl 	= $service->getValue('facebook');
		$this->facebookId 	= $service->getValue('facebookId');
		
		$app = Yii::$app;

        /** Check, apakah ada session user atau tidak */
        if ( !empty($app->user->identity) )
        {
        	$this->user = $app->user->identity;
        }

	}


	/**
	 * Meta function
	 *
	 * @used_by 	/frontend/views/layout/menu.twig
	 * 
	 * @return     string
	 */
	public function meta()
	{
		$service = new SettingService;

		$data = $this->meta;

		$description = StringHelper::byteSubstr( Html::encode(isset($data['description']) ? $data['description'] : $service->getValue('meta-description')), 0, 160 );
		$keywords 	 = isset($data['keywords']) ? $data['keywords'] : $service->getValue('meta-keywords');
		$title 		 = isset($data['title']) ? $data['title'] : $service->getValue('meta-title');
		$image 		 = isset($data['image']) ? $data['image'] : $service->getValue('meta-image');

		$url 		 = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		$result = '<title>' . Html::encode($title) . '</title>' . "\n";
		$result .= '<link rel="canonical" href="'. $url .'">' . "\n";
		$result .= '<meta name="description" content="'. $description .'">' . "\n";
		$result .= '<meta name="keywords" 	 content="'. $keywords .'">' . "\n";
		$result .= '<meta name="robots" 	 content="'. (isset($data['robots']) ? $data['robots'] : 'index, follow') . '">' . "\n";

		$result .= '<meta property="fb:app_id" content="'. $this->facebookId .'">' . "\n";
		$result .= '<meta property="og:title" content="'. $title .'">' . "\n";
		$result .= '<meta property="og:type"  content="'. (isset($data['og:type']) ? $data['og:type'] : 'article') .'">' . "\n";
		$result .= '<meta property="og:image" content="'. $image .'">' . "\n";
		$result .= '<meta property="og:url"   content="'. $url . '">' . "\n";
		$result .= '<meta property="og:description" content="'. $description . '">' . "\n";


		$result .= '<meta property="twitter:title" content="'. $title .'">' . "\n";
		$result .= '<meta property="twitter:card"  content="'. (isset($data['twitter:card']) ? $data['twitter:card'] : 'summary') .'">' . "\n";
		$result .= '<meta property="twitter:image" content="'. $image .'">' . "\n";
		$result .= '<meta property="twitter:url"   content="'. $url . '">' . "\n";
		$result .= '<meta property="twitter:description" content="'. $description . '">';

		return $result;
	}


	/**
	 * article categories
	 * Fungsi ini untuk memberikan value ke pada megamenu di menu artikel
	 * 
	 * @used_by 	/frontend/views/partials/header.twig
	 * 
	 * @return     object
	 */
	public function articleCategories()
	{
		$categories = \common\models\ArticleCategory::get()->select('id, slug, name')->orderBy('name')->all();
		return $categories;
	}
}