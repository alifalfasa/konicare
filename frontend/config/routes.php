<?php

/**
 * FRONTEND routes
 * 
 * @used_by 		/frontend/config/main.php
 * 
 * @filesource 		/frontend/config/routes.php
 * 
 */
return [
    // 'home' => 'site/index',
    'product' => 'site/product',
    'product/<slug:[0-9a-zA-Z\-]+>' => 'site/product-detail',
    
    'facebook/login/callback' => 'participant/facebook-callback',
    'participant/register/step/<step:[1-3]+>' => 'participant/register',
    'login' => 'participant/login',
    'forgot-password' => 'participant/forgot-password',
    'user-profile' => 'participant/user-profile',
    
    'article' => 'article/index',
    'article/comment' => 'article/comment',
    'article/comment-loadmore' => 'article/comment-loadmore',
    'article/comment-like' => 'article/comment-like',
    'article/<id:[0-9]+>/like' => 'article/like',
    'article/<slug:[0-9a-zA-Z\-]+>' => 'article/category',
    'article/<category:[0-9a-zA-Z\-]+>/<slug:[0-9a-zA-Z\-]+>' => 'article/detail',

    'forum/<group:[0-9a-zA-Z\-]+>/<forum:[0-9a-zA-Z\-]+>' => 'forum/group', // ex: /forum/parenting/tips-trick
    'forum/<topic:[0-9a-zA-Z\-]+>' => 'forum/topic', // ex: /forum/parenting/tips-trick/tips-atasi-anak-susah-makan

    'search' => 'site/search',
    'not-found' => 'site/not-found',
    'contact-us' => 'site/contact-us',
    'news-and-events' => 'site/news-events',
    'privacy-policy' => 'site/privacy-policy',
    'term-and-conditions' => 'site/tnc',
    'news' => 'news-activity/news',
    'activity' => 'news-activity/activity',
    '<category:news|activity>/<slug:[0-9a-zA-Z\-]+>' => 'news-activity/detail',



    '<controller:[0-9a-zA-Z\-]+>/<id:\d+>' => '<controller>/view',
    '<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>/<id:\d+>' => '<controller>/<action>',
    '<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>' => '<controller>/<action>',
];