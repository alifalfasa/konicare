<?php
namespace frontend\controllers;

use Yii;

use yii\filters\AccessControl, 
	yii\data\Pagination,
	yii\helpers\ArrayHelper;

use common\models\Forum, 
	common\models\ForumGroup, 
	common\models\ForumTopic, 
	common\models\ForumPost, 
	common\components\Functions;

use frontend\services\ForumService,
	frontend\services\RecaptchaService;

/**
 * Forum controller
 */
class ForumController extends BaseController
{

	public $currentMenu = 'forum';

	/**
	 * Index
	 * Forum overview
	 * 
	 * @link 	http://base.url/forum
	 */
	public function actionIndex()
	{
		$service = ForumService::index();
		return $this->render('index.twig', [ 'groups' => $service['groups'], 'topics' => $service['topics'] ]);
	}


	/**
	 * Group
	 * Forum Thread List
	 *  
	 * @param 	string	$group 		Slug pada table forum_group
	 * @param 	string	$forum 		Slug pada table forum
	 * 
	 * @link 	http://base.url/forum/parenting/tips-trick 		Forum thread List
	 */
	public function actionGroup($group, $forum)
	{
		$forumGroup = ForumGroup::getOne(['slug' => $group]);

		$forum 		= Forum::getOne(['forum_group_id' => $forumGroup->id, 'slug' => $forum]);

		$forumTopics = ForumService::group( $forum->id );

		$formData = [ 'title' => null, 'content' => null ];
		$recaptchaSiteKey = null;

		$recaptcha = new RecaptchaService();
		$optionForum = $recaptcha->getOptionForum();

		$btnCreateForum = strtolower($optionForum) == 'enabled' ? true :  false;

		if ( isset($this->user->id) )
		{

			// Create forum section
			$posts = Yii::$app->request->post();

			$recaptchaSiteKey = $recaptcha->getSiteKey();
			
			if ( !empty($posts) )
			{
				if ( $recaptcha->validate() )
				{

					$saveData = [ 
						'forum_id' => $forum->id,
						'participant_id' => $this->user->id,
						'name' => $posts['title'],
						'content' => $posts['content']
					];

					$create = ForumService::createForum($saveData);
					if ( isset($create['success']) )
					{

						return $this->redirect( $create['route'] );

					} else {

						$this->session->setFlash('warning', $create['message']);

					}

				} else {
					$this->session->setFlash('warning', 'Captcha yang anda masukan tidak valid');
				}


				$formData = [ 
					'title'   => $posts['title'], 
					'content' => $posts['content'] 
				];

				

			}
			
		}


		// Attribute meta tags
		Yii::$app->view->meta = [
			'title' => 		 $forum->name,
			'description' => $forum->description,
		];
		$data = [
			'group' => $forumGroup,
			'forum' => $forum,
			'site_key' => $recaptchaSiteKey,
			'btnCreateForum' => $btnCreateForum
		];
		return $this->render('group.twig', $forumTopics + $data + $formData);
	}


	/**
	 * Topic
	 * Untuk halaman detail forum atau topic forum
	 *
	 * @param 	string	$group 		Slug pada table forum_group
	 * @param 	string	$forum 		Slug pada table forum
	 * @param 	string	$topic 		Slug pada table forum_topic
	 * 
	 * @link       http://base.url/forum/tips-akrabkan-si-ayah-dengan-si-kecil 	Detail/Topic Forum
	 */
	public function actionTopic($topic)
	{

		$topic 	= ForumTopic::detail( $topic )->one();


		// when topic is empty will redirect to not found page
		if ( empty($topic) )
		{
			throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);
		}


		// Set View Count 
		$cookies = Yii::$app->response->cookies;
		$name = md5($topic->slug);
		if( empty(\Yii::$app->getRequest()->getCookies()->getValue($name)) ) {

			$cookies->add(new \yii\web\Cookie([
			    'name' => $name,
			    'value' => true,
			    'expire' => time()+86400
			]));

			$topic->view_count = $topic->view_count + 1;
			$topic->save();

		}


		// Ketika Post Reply
		$posts = Yii::$app->request->post();
		if ( !empty($posts) )
		{
			$create = ForumService::postReply($topic, $posts['content']);
			if ( isset($create['success']) )
			{

				return $this->redirect( $create['route'] );

			} else {

				$this->session->setFlash('warning', $create['message']);

			}
		}


		$topicPost = ForumService::topicPost($topic->id);

		$result = array_merge($topicPost, ['topic' => $topic]);
		
		// Attribute meta tags
		Yii::$app->view->meta = [
			'title' => 		 $topic->name,
			'description' => substr( \yii\helpers\Html::encode($topicPost['posts'][0]['content']), 0, 100),
		];

		return $this->render('topic.twig', $result);
	}

}