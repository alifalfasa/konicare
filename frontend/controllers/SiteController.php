<?php
namespace frontend\controllers;

use yii\web\View;
use Yii;

use common\models\ForumGroup,
    common\models\ForumTopic,
    common\models\Page,
    common\services\ActivityService,
    frontend\models\Article,
    frontend\models\Product,
    frontend\models\Banner;

use backend\models\MediaFolder as Folder;
use backend\models\MediaFile as File;
/**
 * Site controller
 */
class SiteController extends BaseController
{

    public function actionMigrate($part)
    {

        // reject id 2, 10 dan 92 karna itu bukan artikel
        // $categoryFromProd = [
        //     2 => [ 'name' => 'Kehamilan', 'revamp_id' => 2],
        //     3 => [ 'name' => 'Bayi', 'revamp_id' => 1],
        //     4 => [ 'name' => 'Balita', 'revamp_id' => 7],
        //     5 => [ 'name' => 'Anak', 'revamp_id' => 5],
        //     6 => [ 'name' => 'Mitos Fakta', 'revamp_id' => 3],
        //     7 => [ 'name' => 'Playdeia', 'revamp_id' => 8],
        //     8 => [ 'name' => 'Tips', 'revamp_id' => 4],
        //     11 => [ 'name' => 'Masa Kehamilan', 'revamp_id' => 2],
        //     99 => [ 'name' => 'Nature & Health', 'revamp_id' => 6],
        // ];

        // $connection = Yii::$app->db;

        // $connection->createCommand('SET sql_mode = ""')->execute();
        // $command = $connection->createCommand('
        //     SELECT artikel.*, catid FROM bk_artikelcategory ac Inner JOIN bk_artikel as artikel on artikelid = artikel.id where catid not in (2, 10, 92) group by artikelid limit ' . $_GET['start'] .',' . $_GET['end'] . ';');
        // $results = $command->queryAll();
        // // var_dump($results);exit;
        // foreach ($results as $result) {


        //     $folder = Folder::fetchOne(34); // folder id untuk article
        //     $file = new File();
            
        //     $grabUrl = 'http://bundakonicare.com/MFfiles/artikel/' . $result['url_img'];
        //     $fileInfo = pathinfo($grabUrl);
        //     $filename = $fileInfo['basename'];
            
        //     $file->media_folder_id = $folder->id;
        //     $file->name = $filename;
        //     $file->file_type = 'image/' . $fileInfo['extension'];
        //     $file->size = 0;
        //     $file->save();

        //     $folderDir = $folder->directory . $file->id . '/';
        //     $dir = ASSETS_PATH . '../' . $folderDir; 
        //     $upload = new \common\components\Upload;

        //     mkdir($dir);

            
        //     $category = $categoryFromProd[$result['catid']];
        //     $article = new \backend\models\Article;
        //     $article->article_category_id = $category['revamp_id'];
        //     $article->title = $result['title'];
        //     $article->slug = \common\components\Functions::makeSlug($result['post_name']);
        //     $article->keywords = $category['name'];
        //     $article->subcontent = substr( strip_tags($result['content']), 0 , 100);
        //     $article->content = $result['content'];
        //     $article->image = $filename;
        //     $article->image_dir = $folderDir;
        //     $article->shares_count = 0;
        //     $article->views_count = 0;
        //     $article->comments_count = 0;
        //     $article->row_status = 1;
        //     if ($article->save())
        //     {
        //         $article->created_at = strtotime($result['created_date']);
        //         $article->save();

        //         file_put_contents($dir . $filename, file_get_contents($grabUrl));
                
        //         $upload::resize($grabUrl, $dir . 'normal_' . $filename, 600, 600);
        //         $upload::resize($grabUrl, $dir . 'thumb_' . $filename, 200, 200);

        //     } else {
        //         var_dump($article->getErrors());
        //     }

        // }
        // exit;
        // $folder = Folder::fetchOne(29);
        // $file = new File();
        
        // $filename = 'artikel_1491464196.jpg';
        
        // $file->media_folder_id = $folder->id;
        // $file->name = $filename;
        // $file->file_type = 'image/jpg';
        // $file->size = 0;
        // $file->save();

        // $dir = ASSETS_PATH . '../' . $folder->directory . $file->id . '/'; 
        // $upload = new \common\components\Upload;

        // mkdir($dir);

        // $grabUrl = 'http://bundakonicare.com/MFfiles/artikel/artikel_1491464196.jpg';
        // file_put_contents($dir . $filename, file_get_contents($grabUrl));
        
        // $upload::resize($grabUrl, $dir . 'normal_' . $filename, 600, 600);
        // $upload::resize($grabUrl, $dir . 'thumb_' . $filename, 200, 200);
        
        // var_dump($dir);exit;
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $banners = Banner::home();

        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        if ( preg_match( '/index.php/', $actual_link)) {
            header('Location: ' . BASE_URL, 301);
            exit;
        } 

        $articles = Article::get()->joinWith('category')->orderBy('created_at DESC')->limit(5)->all();

        $activityService = new ActivityService( new Article, isset($this->user) ? $this->user->id : null );
        $articleLikesIds = $activityService->getIdsByData( $articles, 'like' );

        $articleResult = [];
        foreach( $articles as $article )
        {
            $article->like_status = isset( $articleLikesIds[ $article['id'] ] ) ? true : false;

            $articleResult[] = $article;
        }


        $forumGroups  = ForumGroup::get()->with([
            'forums' => function($query) {
                $query->andWhere(['row_status' => ForumGroup::STATUS_ACTIVE]);
            } 
        ])->all();
        $groupTopics = [];
        foreach( $forumGroups as $group )
        {
            $forumIds = [];
            foreach($group->forums as $forum)
            {
                $forumIds[] = $forum->id;
            }
            $topics = ForumTopic::get()->andWhere(['forum_id' => $forumIds])->orderBy('updated_at DESC')->limit(3)->all();
            $groupTopics[$group->id] = $topics;
        }

        $productHighlight = Product::getOne(['highlight_status' => 1], false);


        $highlightApps   = Page::getData('bunda-highlight-apps');


        return $this->render('index.twig', compact( 'articles', 'banners', 'forumGroups', 'groupTopics', 'productHighlight', 'highlightApps' ) );
    }

    public function actionAbout()
    {
        $this->view->currentMenu = 'about-us';
        $page = Page::getData('about-us');

    	return $this->render('about.twig', ['page' => $page]);
    }

    public function actionProduct()
    {
        $this->view->currentMenu = 'produk';
        
        $products = Product::all();
        return $this->render('product.twig', compact( 'products' ) );
    }

    public function actionProductDetail($slug)
    {
        $this->view->currentMenu = 'produk';

        $product      = Product::getOne(['slug' => $slug]);
        $productLists = Product::all();
        $anotherProducts = Product::get()
            ->andWhere([ '!=', 'id', $product->id])
            ->orderBy('RAND()')
            ->all();

        // Attribute meta tags
        Yii::$app->view->meta = [
            'title' =>       $product->name,
            'description' => $product->subcontent,
            'image' =>       BASE_URL . 'media/product-main-image/' . $product->id . '/' . $product->main_image,
        ];

        return $this->render('product_detail.twig', compact('product', 'productLists', 'anotherProducts') );
    }

    public function actionError()
    {

        $exception = Yii::$app->errorHandler->exception;
        if ($exception->statusCode == 404 ){
            header('Location: ' . BASE_URL, 301);
            exit;
            // return $this->redirect(['index']);
        }

        // $text      = Yii::$app->response->statusText;
        $layout = $exception->statusCode === 404 ? 'not_found.twig' : 'error.twig';
        return $this->render($layout, [
            'exception'  => $exception, 
            // 'statusText' => $text,
        ]);
    }

    public function actionContactUs()
    {
        $model = new \common\models\Message;
        $this->view->currentMenu = 'contact-us';

        $post = Yii::$app->request->post();
        if ( !empty($post) )
        {
            $model->subject = 'Pesan: Halaman Kami';
            $saveModel = $model::saveData($model, $post);
            if ( $saveModel[ 'status' ] == true )
            {
                $this->session->setFlash('success', 'Pesan berhasil terkirim');
                return $this->refresh();
            } else {
                $this->session->setFlash('danger', $saveModel['message']);
            }
        }

        return $this->render('contact_us.twig', ['model' => $model]);
    }

    /**
     * Search Page
     *
     * @link       http://base.url/search
     */
    public function actionSearch()
    {
        if ( !empty(Yii::$app->request->get('keywords')) )
        {
            $searchService = \frontend\services\SearchService::result(Yii::$app->request->get('keywords'));            
            return $this->render('search.twig', $searchService);
        } else {
            throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);
        }
        
    }

    public function actionNotFound()
    {
        return $this->render('not_found.twig');
    }

    public function actionNewsEvents()
    {
        return $this->render('news_events.twig');
    }

    public function actionPrivacyPolicy()
    {
        $page = Page::getData('privacy-policy');

        return $this->render('privacy_policy.twig', [ 'page' => $page]);
    }

    public function actionTnc()
    {
        $page = Page::getData('term-and-conditions');

        return $this->render('tnc.twig', [ 'page' => $page]);
    }

}
