<?php
namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl, 
	yii\data\Pagination,
	yii\helpers\ArrayHelper;

use frontend\models\Article,
    common\services\ActivityService,
	common\models\ArticleCategory as Category, 
	common\models\PostComment as Comment,
	common\models\Forum;

/**
 * Article controller
 */
class ArticleController extends BaseController
{

	public $currentMenu = 'article';
    
    private $activityService;

    public function init()
    {
        parent::init();
        $this->activityService = new ActivityService( new Article, isset($this->user) ? $this->user->id : null );
    }


    /**
     * Index
     * Halaman ini untuk seluruh data article category
     * 
     * @link 	Ex: http://base.url/article
     */
	public function actionIndex()
	{
		$category = Category::get()->orderBy('name')->all();
		return $this->render('index.twig', ['categories' => $category]);
	}


	/**
	 * Article Category
	 * Article-article yang berdasrakan kategori terpilih
	 * 
	 * @param		string  $slug   Slug article_category
	 * 
	 * @package 	\common\models\ArticleCategory as Category
	 * @package 	\yii\data\Pagination
	 * 
	 * @link        Ex: http://base.url/article/mitos-fakta
	 */
	public function actionCategory($slug)
	{
		$category = Category::getOne(['slug' => $slug]);
		
		$query = Article::get()->andWhere(['article_category_id' => $category->id]);
		$countQuery = clone $query;
		$pages 	    = new Pagination([ 'totalCount' => $countQuery->count(), 'pageSize' => 6 ]);
		$articles 	= $query->offset($pages->offset)
						    ->limit($pages->limit)
						    ->all();
		
		return $this->render('category.twig', ['category' => $category, 'articles' => $articles, 'pages' => $pages]);
	}


	/**
	 * Detail artikel
	 *
	 * @param      	string  $category  Slug article category
	 * @param      	string  $slug      Slug article
	 * 
	 * @package 	\common\models\PostComment as Comment
	 * 
	 * @return     	mixed
	 */
	public function actionDetail($category, $slug)
	{

		// Detail Artikel
		$article = Article::get()->andWhere(['slug' => $slug])->with('category')->one();


        if ( empty($article) )
            throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);
        
		$likeStatus = $this->activityService->getIdsByData( [$article], 'like' );

		// Related Post
		$relatedPosts = Article::get()
			->andWhere(['!=', 'id', $article->id])
			->andWhere(['article_category_id' => $article->article_category_id])
			->with('category')
			->limit(3)
			->orderBy('RAND()')
			->all();
		
        $saveActivity = $this->activityService->read($article->id);

		// Komentar Artikel
		$commentModel  = Comment::data( Comment::ARTICLE, $article->id );
		$commentCounts = clone $commentModel;

		$comments = $commentModel->limit(3)->all();

		$commentLikesIds = $this->activityService->getIdsByData( $comments, 'comment-like' );

		$commentResult = [];
		foreach( $comments as $comment )
		{
            $likes = isset( $commentLikesIds[ $comment->id ] ) ? true : false;
            $commentResult[] = [
            	'id' => $comment->id,
            	'content' => $comment->content,
            	'like_status' => $likes,
            	'participant' => $comment->participant
            ];
		}

		// Attribute meta tags
		Yii::$app->view->meta = [
			'title' => 		 $article->title,
			'description' => $article->subcontent,
			'image' => 		 BASE_URL . $article->image_dir . $article->image,
			'keywords' => 	 $article->keywords,
		];

		return $this->render('detail.twig',
		[
			'article' => $article,
			'comments' => $commentResult,
			'relatedPosts' => $relatedPosts,
			'commentCounts' => $commentCounts->count(),
			'likeStatus' => $likeStatus
		]);
	}


	public function actionLike($id)
	{
		$article = Article::get()->andWhere(['article.id' => $id])->joinWith('category')->one();

        $saveActivity = $this->activityService->like($article->id);

        return $this->redirect( [
        	'article/detail', 
        	'category' => $article->category->slug, 
        	'slug' => $article->slug ] );
	}


	public function actionComment()
	{

		$post = Yii::$app->request->post();
        $post['PostComment']['post_id'] = $post['post_id'];
        $post['PostComment']['content'] = $post['content'];
        $post['PostComment']['type']  = Comment::ARTICLE;
        $post['PostComment']['participant_id'] = $this->user->id;

        $save = Comment::saveData( new Comment(), $post );
        if ( $save['status'] == true )
        {
        	$this->session->setFlash('success', 'Komentar anda berhasil tersimpan');
            $this->activityService->comment($post['PostComment']['post_id']);
        } else {
        	$this->session->setFlash('danger', $save['message']);
        }

		return $this->redirect($post['redirect']);
	}


	/**
	 * Comment Loadmore
	 * Menampilkan data menggunakan ajax
	 *
	 * @throws     \yii\web\HttpException 
	 *
	 * @return     json
	 */
	public function actionCommentLoadmore()
	{
		$req = Yii::$app->request;

		if ( $req->isAjax )
		{
			$post = $req->post();
			$article = Article::getOne($post['id']);
			$comments = Comment::data( Comment::ARTICLE, $post['id'] )
				->joinWith([
					'participant' => function($query){
						$query->select('id, fullname,username,avatarimg');
					}
				])
				->offset($post['start'])
				->limit(3)
				->asArray()
				->all();

			$commentLikesIds = $this->activityService->getIdsByData( $comments, 'comment-like' );

			$commentResult = [];
			foreach( $comments as $comment )
			{
	            $likes = isset( $commentLikesIds[ $comment['id'] ] ) ? true : false;
	            $commentResult[] = [
	            	'id' => $comment['id'],
	            	'content' => $comment['content'],
	            	'like_status' => $likes,
	            	'participant' => $comment['participant']
	            ];
			}

			$count = Comment::data( Comment::ARTICLE, $post['id'] )->count();
			Yii::$app->response->format = 'json';
			return [ 'start' => count($comments) + $post['start'], 'max' => $count, 'comments' => $commentResult ];			
		}
		throw new \yii\web\HttpException(405, 'Method not allowed');
	}


	public function actionCommentLike()
	{
		$req = Yii::$app->request;
		if ( $req->isAjax )
		{
			Yii::$app->response->format = 'json';

			return $this->activityService->commentLike( Yii::$app->request->post('id') );
		}

		return $this->redirect(['index']);
		throw new \yii\web\HttpException(405, 'Method not allowed');
	}

}