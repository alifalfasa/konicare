<?php
namespace frontend\controllers;

use Yii;

use common\models\Participant, 
	common\components\Functions,
	common\models\Province,
	common\models\Regency,
	yii\helpers\ArrayHelper,
	frontend\models\SignupForm,
	frontend\models\SigninForm,
	frontend\models\ForgotPassword,
	frontend\models\ChangePassword,
	frontend\services\FacebookService;

/**
 * Participant controller
 */
class ParticipantController extends BaseController
{

	private $facebookService;

	public function behaviors()
	{
	    $result = ArrayHelper::merge([
	        'access' => [
	            'class' => \yii\filters\AccessControl::className(),
	            'rules' => [
	                [
	                  	'allow' => true,
	                   	'actions' => [ 'detail' ],
	                   	'roles' => ['@']
	                ],
	            ],
	        ],
	    ], parent::behaviors());
	    return $result;
	}

	public function init()
	{
		parent::init();
		$this->facebookService = new FacebookService();
	}

	public function actionFacebookCallback()
	{
		$this->facebookService->callback();

		$session = Yii::$app->session;
		$user = $this->facebookService->getUserByToken($session['facebook_access_token']);
		if ( !empty( $user['email']) )
			$conditions = [ 'email' => $user['email' ] ];
		else
			$conditions = [ 'fbuserid' => $user['id' ] ];

		$participant = Participant::fetch()->andWhere($conditions)->one();

		if ( $participant === null )
		{

			$gender = [
				'male' => 'Pria',
				'female' => 'Wanita'
			];

			Yii::$app->session['Register'] = [
				'email'    => $user['email'],
				'fullname' => $user['name'],
				'fbuserid' => $user['id'],
				'gender'   => $gender[ $user['gender'] ],
			];

			return $this->redirect( ['participant/register', 'step' => 2] );

		} else {

			// Jika facebooknya sudah terdaftar didatabase
			// Akan otomatis mendapatkan hak autologin
			$signIn = new SigninForm;
			$signIn->username = $participant->username;

			Yii::$app->user->login($signIn->getParticipant(), 3600 * 24 * 30);
			return $this->redirect( ['site/index'] );
		}

	}


	/**
	 * Register
	 *
	 * @param      	integer  $step   Step di isi dengan range 1-2
	 * 
	 * @package 	/frontend/models/SignupForm
	 * 
	 * @link 		http://base.url/participant/register/step/1
	 * @link 		http://base.url/participant/register/step/2
	 * 
	 * @return     	mixed
	 */
	public function actionRegister( $step )
	{

		$session = Yii::$app->session;
		$request = Yii::$app->request;
		$post    = $request->post('SignupForm');

		$model = new SignupForm;
		if ( $step == 1 )
		{	
	
			$facebookUrlLogin = $this->facebookService->login(); 

			$model->scenario = 'step1';

			if ( $request->isPost )
			{
				$model->username 	= $post['username'];				
				$model->email 		= $post['email'];				
				$model->password 	= $post['password'];				
				$model->confirmPassword = $post['confirmPassword'];				

				if( $model->validate() )
				{
					$session['Register'] = [
						'email'    => $model->email,
						'password' => $model->password,
						'username' => $model->username,
					];

					// var_dump($request->post());exit;
					return $this->redirect(['participant/register', 'step' => 2]);
				} 

			}

			return $this->render('register-step-1.twig', [ 
				'model' => $model, 
				'facebookUrlLogin' => $facebookUrlLogin 
			]);

		} elseif ( $step == 2 ) {

			if ( !isset( $session['Register']) )
			{
				return $this->redirect(['participant/register', 'step' => 1]);
			}

			$model->scenario = 'step2';

			$register = $session['Register'];
			$model->usertype = isset($register['fbuserid']) ? 'fb' : 'web';
			$model->fullname = isset($register['fullname']) ? $register['fullname'] : null;
			$model->username = isset($register['fbuserid']) ? 
							   Functions::makeSlug($register['fullname'], null, '_') . rand(0,999) : 
							   $register['username'];
			$model->fbuserid = isset($register['fbuserid']) ? $register['fbuserid'] : 0;
			$model->password = isset($register['password']) ? $register['password'] : null;
			$model->email 	 = $register['email'];
			$model->gender   = isset($register['gender'])   ? $register['gender'] : null;


			if ( $request->isPost )
			{
				list($dd,$mm,$yyyy) = explode('/',$post['birthdate']);
				$birthdate = $yyyy.'-'.$mm.'-'.$dd;

				$model->fullname = $post['fullname'];
				$model->gender   = $post['gender'];
				$model->birthdate = date('Y-m-d', strtotime($birthdate));
				$model->nohp 	  = $post['nohp'];
				$model->address   = $post['address'];
				$model->province_id = $post['province_id'];
				$model->regency_id 	= $post['regency_id'];
				// $model->zip 		= $post['zip'];

				$save = $model->signup();

				// var_dump($save);exit;
				if ( $save == false )
				{
					$errorMessage = null;
			        foreach ( $model->getErrors() as $field => $messages ) {
			            foreach ( $messages as $message ) {
			                $errorMessage .= $message . "\n";
			            }
			        }
			        $this->session->setFlash('danger', $errorMessage);
				}

				// Jika daftarnya tidak menggunakan akun facebook
				if ( $model->usertype === 'web')
				{
					if ( $save == false )
						return $this->redirect( [ 'participant/register/step/2' ] );

					Yii::$app->mailer->compose('register-activation-html', [ 'user' => $save ])
					    ->setFrom('no-reply@bundakonicare.com')
					    ->setTo($model->email)
					    ->setSubject('[Bundakonicare] Register Activation')
					    ->send();
		
					unset(Yii::$app->session['Register']);

                	return $this->render('register-step-3.twig', ['email' => $model->email]);

				} else {
	
					if ( $save == false )
						return $this->redirect( [ 'participant/login' ] );

					// Bila data barhasil kesimpan database
					if ( $save != false )
					{
						unset(Yii::$app->session['Register']);
						$signIn = new SigninForm;
						$signIn->username = $model->username;
						Yii::$app->user->login($signIn->getParticipant(), 3600 * 24 * 30);

	                	return $this->render('register-activation-success.twig');
						
					} else {
						unset(Yii::$app->session['Register']);
				        $errorMessage = null;
				        foreach ( $model->getErrors() as $field => $messages ) {
				            foreach ( $messages as $message ) {
				                $errorMessage .= $message . "\n";
				            }
				        }
						$this->session->setFlash('danger', $errorMessage);
						return $this->redirect('/login');

					}
				}

			}

            $provinces = Province::find()
                ->orderBy( 'name' )
                ->asArray()
                ->all();
            $provinces  = [ 0 => '-- Pilih Provinsi --'] + ArrayHelper::map($provinces, 'id', 'name');

			return $this->render('register-step-2.twig', [ 'model' => $model, 'provinces' => $provinces ]);

		} elseif ( $step == 3 ) {

			// Thank you Page!
            return $this->render('register-step-3.twig', ['email' => $model->email]);

        }
	}


	public function actionActivation($key)
	{

		$participant = Participant::fetchOne(['auth_key' => $key, 'row_status' => 0]);

		// Jika keynya belom expired
		if ( $participant->updated_at >= strtotime('-30 minutes'))
		{
			$participant->row_status = 1;
			$participant->generateAuthKey();
			$participant->save();

			Yii::$app->user->login($participant, 0);

			// Thank you Page!
			return $this->render('register-activation-success.twig');

		} else {
			// jika sudah expired

			if ( Yii::$app->request->isPost )
			{
				$participant->generateAuthKey();
				if ($participant->save())
				{
					// Kirim ulang access key
					Yii::$app->mailer->compose('register-activation-html', [ 'user' => $participant ])
					    ->setFrom('no-reply@bundakonicare.com')
					    ->setTo($participant->email)
					    ->setSubject('[Bundakonicare] Register Activation')
					    ->send();

	            	return $this->render('register-step-3.twig', ['email' => $participant->email]);
					
				} else {
					throw new \yii\web\HttpException(422, Paricipant::getError($paricipant));
				}
			}

			return $this->render('register-activation-fail.twig');

		}
		
	}


	public function actionDetail()
	{
		$participant = Participant::getOne($this->user->id);

		$request = Yii::$app->request;
		if ( $request->isPost )
		{
			$post = $request->post();

			$post['Participant']['birthdate'] = $participant->setBirthdate( $post['Participant']['birthdate'] );
			$save = Participant::saveData($participant, $post);

			if ( $save['status'] == true )
			{
        		$this->session->setFlash('success', 'Profile anda berhasil diupdate');
        		return $this->refresh();
	        } else {
	        	$this->session->setFlash('warning', $save['message']);
	        }
		}

		$provinces = Province::find()->all();
		$regencies = Regency::find()->where(['province_id' => $participant->province_id])->all();

		return $this->render( 'detail.twig', compact('participant', 'provinces', 'regencies') );
	}


	public function actionLogin()
	{
		$facebookUrlLogin = $this->facebookService->login(); 
		$model = new SigninForm;
		$goto  = Yii::$app->request->get('goto');

        if ( $model->load( Yii::$app->request->post() ) ) 
        {
            if ( $model->login() )
            {
                return !empty($goto) ? $this->redirect($goto) : $this->goBack();
            } elseif ( $model->oldMember == true ) {
            	$this->session->setFlash('success', 'Silahkan masukan email anda untuk mereset password untuk halaman konicare yang baru ini');
            	return $this->redirect(['participant/forgot-password']);
            }

        } 

		return $this->render('login.twig', [ 
			'model' => $model, 
			'goto' => $goto,
			'facebookUrlLogin' => $facebookUrlLogin 
		]);
	}


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }


    public function actionForgotPassword()
    {
 		
 		$model = new ForgotPassword;  
 		$model->scenario = 'forgot-password';

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) 
        {
        	$participant = Participant::getOne(['email' => $model->email]);
        	$participant->generateAuthKey();
			if ($participant->save())
			{
				// Kirim ulang access key
				Yii::$app->mailer->compose('forgot-password-html', [ 'user' => $participant ])
				    ->setFrom('no-reply@bundakonicare.com')
				    ->setTo($participant->email)
				    ->setSubject('[Bundakonicare] Register Activation')
				    ->send();

				$this->session->setFlash('success', 'Harap membuka email anda untuk verifikasi akun');
				return $this->redirect('login');
			}
        }  	

    	return $this->render('forgot-password.twig', ['model' => $model]);

    }


    public function actionChangePassword()
    {
    	$model = ChangePassword::getOne(['email' => $this->user->email]);  

    	if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) 
        {
        	$model->setPassword($model->newPassword);
        	if ( $model->save(false) )
        	{
				$this->session->setFlash('success', 'Password anda berhasil diubah');
				return $this->redirect('detail');
        	} else {
        		$this->session->setFlash('warning', $model::getError($model));
        	}

        }

    	return $this->render('change-password.twig', ['model' => $model]);
    }


    public function actionReActivated($key)
    {
 		$participant = Participant::fetchOne(['auth_key' => $key, 'row_status' => 1]);

		// Jika keynya belom expired
		if ( $participant->updated_at >= strtotime('-15 minutes'))
		{
			$request = Yii::$app->request;

			$model = new ForgotPassword;
			$model->scenario = 're-activated';

			if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) 
			{
				$participant->generateAuthKey();
				$participant->setPassword($model->rePassword);
				if ( $participant->save() )
				{
					$this->session->setFlash('success', 'Silahkan login menggunakan password baru anda');
					return $this->redirect('login');
				} else {
					$this->session->setFlash('success', $participant->getError());
				}
			}

			// Thank you Page!
			return $this->render('re-activated.twig', [ 'model' => $model ] );

		} else {
			$this->session->setFlash('warning', 'Access key sudah expired harap melakukan forgot password kembali');
			return $this->render('re-activated.twig');
		}
    }

}
