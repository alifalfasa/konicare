<?php
namespace frontend\controllers;

use Yii;

use common\models\Province;
use common\models\Regency;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */

class LocationController extends BaseController
{

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
        ];
    }

    public function actionProvince($id = null)
    {
        if ( !empty( $id ) )
        {
            $query = Province::find()
                ->select( 'id, name' )
                ->andWhere(['=', 'id', $id])
                ->asArray()
                ->one();

        } else {

            $query = Province::find()
                ->orderBy( 'name' )
                ->asArray()
                ->all();
            // $query  = ArrayHelper::map($query, 'id', 'name');
        }

        $result = $query;
        Yii::$app->response->format = 'json';
        return ['province' => $query];
        
    }

    public function actionRegency()
    {
        $post = Yii::$app->request->post();

        $provinceId = $post['provinceId'];

        $regency = Regency::find()->andWhere(['province_id' => $provinceId])->all();

        Yii::$app->response->format = 'json';

        return  $regency;
    }
}
