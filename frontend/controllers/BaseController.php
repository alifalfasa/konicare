<?php
namespace frontend\controllers;

use Yii,
    yii\base\InvalidParamException,
    yii\web\BadRequestHttpException,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\filters\AccessControl;

use frontend\models\PasswordResetRequestForm,
    frontend\models\ResetPasswordForm,
    frontend\models\SignupForm,
    frontend\models\ContactForm,
    frontend\components\AccessRule;

/**
 * Site controller
 */
class BaseController extends Controller
{

    public $user, $session, $currentMenu;

    public $layout = 'main.twig';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        // Semua yang udah login dapat mengakses semua action
                        'allow' => true,
                        'roles' => ['@'] // (@ = login)
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                        'actions' => [ 'detail' ],
                        'controllers' => ['participant']
                    ],
                    [
                        // Semua action yang diperbolehkan masuk (tanpa) memiliki akses login
                        'actions' => [
                            'migrate',
                            'forgot-password', 
                            're-activated', 
                            'login', 
                            'error', 
                            'register', 
                            'index', 
                            'facebook-callback',
                            'category', 
                            'province', 
                            'regency', 
                            'detail', 
                            'group', 
                            'topic', 
                            'product', 
                            'product-detail', 
                            'comment-loadmore', 
                            'activation', 
                            'about',
                            'search',
                            'user-profile',
                            'contact-us',
                            'news-events',
                            'privacy-policy',
                            'tnc',
                            'news',
                            'activity',
                        ],
                        'allow' => true,
                        'roles' => ['?'], // (? = without login)
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'comment' => ['post'],
                    // 'comment-loadmore' => ['post'],
                    // 'comment-like' => ['post'],
                ],
            ],
        ];
    }


    public function init()
    {
        $app = Yii::$app;

        $this->session = $app->session;

        $this->view->currentMenu = $this->currentMenu;
        /** Check, apakah ada session user atau tidak */
        if ( !empty($app->user->identity) )
        {
            $this->user = $app->user->identity;
        }
    	\common\services\VisitorService::record();
    }

}
