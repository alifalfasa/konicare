<?php
namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl, 
    yii\data\Pagination,
    yii\helpers\ArrayHelper;

use common\models\NewsActivity,
    frontend\services\NewsActivityService;

/**
 * NewsActivity controller
 */
class NewsActivityController extends BaseController
{
    
    public $currentMenu = 'news-activity';

    public function actionNews()
    {   

        $service = new NewsActivityService( NewsActivity::TYPE_NEWS );

        $first = $service->getFirstData();
        $data = $service->getDatas();

        $result = array_merge( $data, [ 'firstData' => $first ] );

        return $this->render( 'index.twig', $result );

    }

    public function actionActivity()
    {   

        $service = new NewsActivityService( NewsActivity::TYPE_ACTIVITY );

        $first = $service->getFirstData();
        $data = $service->getDatas();

        $result = array_merge( $data, [ 'firstData' => $first ] );

        return $this->render( 'index.twig', $result );

    }


    /**
     * Detail Post
     *
     * @param      string  $category  Either News or Activity
     * @param      string  $slug      Post Slug
     */
    public function actionDetail( $category, $slug )
    {
        $service = new NewsActivityService( $category );

        $detail  = $service->getDetail($slug);
        $relatedPosts = $service->relatedPosts();
        $prefix  = $service->getType();

        // Attribute meta tags
        Yii::$app->view->meta = [
            'title' =>       $detail->title,
            'description' => $detail->subcontent,
            'image' =>       BASE_URL . $detail->image_dir . $detail->image,
            'keywords' =>    $detail->keywords,
        ];

        return $this->render( 'detail.twig', compact( 'detail', 'relatedPosts', 'prefix') );
    }
}