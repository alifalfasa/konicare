<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.css',
        'css/foundation-icons/foundation-icons.css',
        'css/application.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'foundationize\foundation\FoundationAsset',
    ];
}