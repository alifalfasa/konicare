<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class RegisterAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/foundation-datepicker.css',
        'css/font-awesome.css',
        'css/application.css',
    ];
    public $js = [
        'js/foundation-datepicker.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\widgets\ActiveFormAsset',
        'foundationize\foundation\FoundationAsset',
    ];
}
