<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProductAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bxslider/jquery.bxslider.min.css',
        'css/application.css',
    ];
    public $js = [
        'plugins/bxslider/jquery.bxslider.min.js'
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        'foundationize\foundation\FoundationAsset',
    ];
}
