<?php

namespace frontend\services;

use Yii,
    common\services\SettingService;

class RecaptchaService
{

    private $__setting;

    public function __construct()
    {
        $this->__setting = new SettingService();
    }

    public function validate()
    {

        $post = [
            'secret'   => $this->__setting->getValue('recaptchaSecretKey'),
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => Yii::$app->getRequest()->getUserIP(),
        ];
        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // execute!
        $response = curl_exec($ch);

        // close the connection, release resources used
        curl_close($ch);

        $result = json_decode($response);
        return $result->success;

    }
    
    public function getSiteKey()
    {
        return $this->__setting->getValue('recaptchaSiteKey');
    }

    public function getOptionForum()
    {
        return $this->__setting->getValue('new-forum');
    }
}