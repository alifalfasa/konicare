<?php
namespace frontend\services;

use common\models\Article,
	common\models\ForumTopic,
	common\models\NewsActivity;

class SearchService 
{
	private static $limit = 10;

	public static function result($keyword)
	{
		$articles 		= Article::search($keyword)->limit(self::$limit)->asArray()->all();
		$topics 		= ForumTopic::search($keyword)->limit(self::$limit)->asArray()->all();
		$newsActivities = NewsActivity::search($keyword)->limit(self::$limit)->asArray()->all();

		return compact('articles', 'topics', 'newsActivities');
	}
}