<?php
namespace frontend\services;

use Yii;

use yii\filters\AccessControl, 
	yii\data\Pagination,
	yii\helpers\ArrayHelper;

use common\models\Forum, 
	common\models\ForumGroup, 
	common\models\ForumTopic, 
	common\models\ForumPost, 
	common\components\Functions;

class ForumService {
	
	const MAX_DATA = 10;

	public static function index()
	{
		$groups = ForumGroup::get()->with([
			'forums' => function ($query) { 
				$query->andWhere(['forum.row_status' => 1]); 
			}]
		)->orderBy('forum_group.name')->all();
		
		$forumIds = [];
		foreach ($groups as $group) {
			foreach( $group->forums as $forum ) {
				array_push($forumIds, $forum->id);
			}
		}

		$connection = Yii::$app->db;
		$connection->createCommand('SET sql_mode = ""')->execute();
		
		$subQuery = ForumTopic::get()
			->select('MAX(id)')
			->andWhere(['forum_id' => $forumIds])
			->groupBy('forum_id');

		$topics  = ForumTopic::get()
			->andWhere(['id' => $subQuery])
			->with('participant')
			->groupBy('forum_id')
			->orderBy('id DESC')
			->all();

		$topics  = ArrayHelper::index($topics, 'forum_id');
		return compact('topics','groups');
	}



	public static function group($forumId)
	{
		$query 		= ForumTopic::get()
			->andWhere(['forum_id' => $forumId])
			->joinWith('participant')
			->orderBy( 'forum_topic.updated_at' );
		$countQuery = clone $query;

		$pages  = new Pagination([ 'totalCount' => $countQuery->count(), 'pageSize' => 10 ]);

		$topics = $query->offset($pages->offset)
						->limit($pages->limit)
						->all();
		
		$topicIds  = ArrayHelper::getColumn($topics, 'id');
		
		$subQuery  = ForumPost::get()
			->select('MAX(id)')
			->andWhere(['topic_id' => $topicIds])
			->groupBy('topic_id');

		$lastPosts = ForumPost::get()
			->andWhere(['forum_post.id' => $subQuery])
			->joinWith('participant')
			->orderBy('forum_post.id DESC')
			->all();
		$lastPosts = ArrayHelper::index($lastPosts, 'topic_id');

		return compact('lastPosts','topics', 'pages');
	}


	public static function topicPost($topicId)
	{
		// $firstPost 	= static::__getFirstPost($topicId);
		
		$query 		= ForumPost::get()->andWhere( ['topic_id' => $topicId] )->joinWith('participant')->orderBy('id ASC');
		$countQuery = clone $query;

		$pages  = new Pagination([ 'totalCount' => $countQuery->count(), 'pageSize' => static::MAX_DATA ]);

		$posts = $query->offset($pages->offset)
						->limit($pages->limit)
						->all();

		$participantIds  = ArrayHelper::getColumn($posts, 'participant_id');
		$participantPost = ForumPost::participantPost($participantIds);
		$participantPost = ArrayHelper::index($participantPost, 'participant_id');

		return compact('posts', 'pages', 'participantPost');
	}


	public static function createForum($posts)
	{
		$data['ForumTopic'] = [
			'forum_id' => $posts['forum_id'],
			'participant_id' => $posts['participant_id'],
			'name' => ucwords($posts['name']),
			'slug' => Functions::makeSlug($posts['name']),
		];

		$topic = new ForumTopic;

		$topic->load($data);

		$errors = null;
		if ( $topic->save() === false )
		{

			$errors = ForumTopic::getError($topic);
			return [ 'error' => true, 'message' => $errors ];

		} else {


			$post = new ForumPost;
			$post->topic_id = $topic->id;
			$post->forum_id = $topic->forum_id;
			$post->participant_id = $topic->participant_id;
			$post->content = $posts['content'];
			$post->ip_address = Yii::$app->getRequest()->getUserIP();
			if ( $post->save() === false )
			{
				$topic->delete();
				return [ 'error' => true, 'message' => ForumPost::getError($post) ];

			} else {
				$topic->post_id = $post->id;
				$topic->save();

				return [ 'success' => true, 'route' => [ 'forum/topic', 'topic' => $topic->slug ] ];
			}

		}

	}


	public static function postReply($topic, $content)
	{
		if ( isset(Yii::$app->user->identity->id) )
		{

			$post = new ForumPost;
			$post->topic_id = $topic->id;
			$post->forum_id = $topic->forum_id;
			$post->participant_id = Yii::$app->user->identity->id;
			$post->content = $content;
			$post->ip_address = Yii::$app->getRequest()->getUserIP();

			if ( $post->save() === false )
			{
				return [ 'error' => true, 'message' => ForumPost::getError($post) ];

			} else {
				return [ 'success' => true, 'route' => [ 'forum/topic', 'topic' => $topic->slug ] ];
			}

		}
	}

}