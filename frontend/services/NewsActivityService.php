<?php

namespace frontend\services;

use Yii;
use yii\filters\AccessControl, 
    yii\data\Pagination,
    yii\helpers\ArrayHelper;

use common\models\NewsActivity;

class NewsActivityService 
{

    
    private $__type;
    private $__firstDataId = 0;
    private $__postId = 0;

    public function __construct( $type )
    {
        $this->__type = $type;
    }


    public function getFirstData()
    {
        $data = NewsActivity::getType( $this->__type )
            ->orderBy('id DESC')
            ->one();

        if ( !empty($data) )
            $this->__firstDataId = $data->id;

        return $data;
    }


    public function getType()
    {
        return $this->__type;
    }


    public function getDatas()
    {

        $query = NewsActivity::getType( $this->__type )->andWhere( [
            '!=', 'id', $this->__firstDataId
        ] );

        $countQuery = clone $query;


        $pages      = new Pagination([ 
            'totalCount' => $countQuery->count(), 
            'pageSize' => 6 
        ]);


        $datas   = $query->offset($pages->offset)
                            ->limit($pages->limit)
                            ->all();

        $prefix = $this->__type;

        return compact( 'pages', 'datas', 'prefix' );
    }


    public function relatedPosts()
    {
        $data = NewsActivity::getType( $this->__type )
            ->andWhere( [
                '!=', 'id', $this->__postId
            ] )
            ->orderBy('RAND()')
            ->limit(3)
            ->all();

        return $data;
    }


    public function getDetail( $slug )
    {
        $data = NewsActivity::getType( $this->__type )
            ->andWhere(['slug' => $slug])
            ->one();

        if ( empty($data) )
            throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);


        $this->__postId = $data->id;

        return $data;
    }
}