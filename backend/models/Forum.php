<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "forum".
 *
 * @property integer $id
 * @property integer $forum_group_id
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property integer $seq
 * @property integer $post_count
 * @property integer $topic_count
 * @property integer $last_postid
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ForumGroup $forumGroup
 */
class Forum extends \common\models\Forum
{

    public static $uploadFile = [
        'icon' => [
            'using' => 'manually',
            'path' => 'forum-icon/',
        ]
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['forum_group_id', 'name', 'slug', 'seq'], 'required'],
            [['forum_group_id', 'seq', 'post_count', 'topic_count', 'last_postid', 'row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['description'], 'string'],
            ['slug', 'slugValidation'],
            // ['slug', 'uniquenessValidation'],
            [['icon'], 'required', 'on' => 'create'],
            [['name', 'icon'], 'string', 'max' => 180],
            [['forum_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ForumGroup::className(), 'targetAttribute' => ['forum_group_id' => 'id']],
        ];
    }

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Group',
            'Name',
            'Slug',
            'Description',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'forum_group_id' => [
                'dropDownList' => [ 'list' => ForumGroup::dataOptions('id','name') ],
            ],
            'name' => [
                'textInput' => [
                    'options' => [
                        'class' => 'form-control autoslug', 
                        'slug-target' => '#forum-slug'
                    ],
                ]
            ],
            'slug',
            'description' => ['textarea'],
            'icon' => [ 'fileInput' ],
            'seq',
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }

    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'name', $params['sSearch']])
            ->with('group')
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['forum/create', 'id' => $model->id], 
                'Delete' => ['forum/delete', 'id' => $model->id] ] );

        
            $data[] = [
                $model->id,
                $model->group->name,
                $model->name,
                $model->slug,
                $model->description,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }
}
