<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "forum_post".
 *
 * @property integer $id
 * @property integer $topic_id
 * @property integer $forum_id
 * @property string $content
 * @property string $ip_address
 * @property integer $row_status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property ForumTopic $topic
 * @property Forum $forum
 */
class ForumPost extends \common\models\ForumPost
{

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Topic',
            'Content',
            'Status',
            'Action'
        ];
    } 

    /**
     * Gets the data for ajax.
     *
     * @param      array  $params  Parameter from data table
     *
     * @return     json|string
     */
    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'content', $params['sSearch']])
            ->joinWith('topic')
            ->orderBy( 'forum_post.id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];

        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Delete' => ['forum-post/delete', 'id' => $model->id] ] );

            $data[] = [
                $model->id,
                $model->topic->name,
                substr(trim(htmlentities($model->content)), 0, 120).' ...',
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;

        return json_encode($result);
    }

}
