<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "forum_groups".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property integer $seq
 * @property integer $row_status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Forum[] $forums
 */
class ForumGroup extends \common\models\ForumGroup
{


    public static $uploadFile = [
        'icon' => [
            'path' => 'forum-group-icon/',
            // 'resize' => [
            //     [
            //         'prefix' => 'thumb_',
            //         'size' => [200,200],
            //     ]
            // ]
        ]
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'seq', 'slug'], 'required'],
            [['description'], 'string'],
            [['icon'], 'required', 'on' => 'create'],
            ['slug', 'slugValidation'],
            ['slug', 'uniquenessValidation'],
            [['seq', 'row_status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['name', 'icon'], 'string', 'max' => 180],
        ];
    }
    
    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Name',
            'Slug',
            'Description',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array
     */
    public static function formData()
    {
        return [
            'id',
            'name' => [
                'textInput' => [
                    'options' => [
                        'class' => 'form-control autoslug', 
                        'slug-target' => '#forumgroup-slug'
                    ],
                ]
            ],
            'slug',
            'description' => ['textarea'],
            'icon' => [ 'fileInput' ],
            'seq',
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForums()
    {
        return $this->hasMany(Forum::className(), ['forum_group_id' => 'id']);
    }

    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'name', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['forum-group/create', 'id' => $model->id], 
                'Delete' => ['forum-group/delete', 'id' => $model->id] ] );

            $data[] = [
                $model->id,
                $model->name,
                $model->slug,
                $model->description,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }
}
