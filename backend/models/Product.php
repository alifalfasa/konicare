<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $subcontent
 * @property string $content
 * @property string $image
 * @property string $store_url
 * @property string $tvc
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Product extends \common\models\BaseModel
{

    public static $uploadFile = [
        'image' => [
            'path' => 'product/',
            'using' => 'manually',
            // 'resize' => [
            //     [
            //         'prefix' => 'thumb_',
            //         'size' => [200,200],
            //     ],
            // ]
        ],
        'highlight_image' => [
            'path' => 'product-highlight-image/',
            'using' => 'manually',
        ],
        'main_image' => [
            'path' => 'product-main-image/',
            'using' => 'manually',
        ]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'keyword', 'subcontent', 'store_url'], 'required'],
            [['content', 'highlight_description'], 'string'],
            ['slug', 'slugValidation'],
            ['slug', 'uniquenessValidation'],
            [['store_url', 'tvc'], 'url'],
            [['image', 'main_image'], 'required', 'on' => 'create'],
            [['highlight_image'], 'required', 'enableClientValidation' => false, 'when' => function($model) {
                return $model->highlight_status == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#product-highlight_status').val() == 1;
            }"],
            ['highlight_title', 'required', 'when' => function($model) {
                return $model->highlight_status == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#product-highlight_status').val() == 1;
            }"],
            ['highlight_description', 'required', 'enableClientValidation' => false, 'when' => function($model) {
                return $model->highlight_status == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#product-highlight_status').val() == 1;
            }"],
            [['row_status', 'highlight_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'slug', 'image', 'store_url', 'tvc', 'highlight_title', 'highlight_image'], 'string', 'max' => 100],
            [['subcontent'], 'string', 'max' => 150],
            [['keyword'], 'string', 'max' => 120],
        ];
    }

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Image',
            'Name',
            'Slug',
            'Subcontent',
            'Hightlihgt Status',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'name' => [
                'textInput' => [ 
                    'options' => [
                        'placeholder' => 'Title', 
                        'class' => 'form-control autoslug', 
                        'slug-target' => '#product-slug'
                    ],
                ] 
            ],
            'slug',
            'keyword',
            'subcontent',
            'store_url',
            'tvc',
            'image' => [
                'fileInput'
            ],
            'main_image' => [
                'fileInput'
            ],
            'content' => [
                'textarea' => [ 'options' => ['class' => 'ckeditor'] ]
            ],
            'highlight_status' => [ 
                'dropDownList' => [ 'list' => static::$getStatus ]
            ],
            'highlight_title',
            'highlight_description' => [
                'textarea' => [ 'options' => ['class' => 'ckeditor' ] ]
            ],
            'highlight_image' => [
                'fileInput'
            ],
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }

    public function getHightlightStatus()
    {
        return static::$getStatus[$this->highlight_status];
    }

    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'name', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['product/create', 'id' => $model->id], 
                'Delete' => ['product/delete', 'id' => $model->id] ] );
            
            $path   = '/media/product/' . $model->id . '/';
            $full   = $path . $model->image;
            $thumb  = $path . $model->image;
            $image  = \backend\components\View::getImage($full, $thumb);

            $data[] = [
                $model->id,
                $image,
                $model->name,
                $model->slug,
                $model->subcontent,
                $model->getHightlightStatus(),
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }

}
