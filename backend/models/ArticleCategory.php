<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "article_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $keywords
 * @property string $description
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Article[] $articles
 */
class ArticleCategory extends \common\models\BaseModel
{

    // public static $uploadFile = [
    //     'image' => [
    //         'using' => 'manually',
    //         'path' => 'article-category/',
    //         'resize' => [
    //             [
    //                 'prefix' => 'thumb_',
    //                 'size' => [200,200],
    //             ]
    //         ]
    //     ]
    // ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'keywords', 'description', 'image', 'image_dir', ], 'required'],
            [['description'], 'string'],
            [['row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 100],
            [['keywords'], 'string', 'max' => 150],
        ];
    }

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Image',
            'Name',
            'Slug',
            'Description',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'name' => [
                'textInput' => [
                    'options' => [
                        'class' => 'form-control autoslug', 
                        'slug-target' => '#articlecategory-slug'
                    ],
                ]
            ],
            'slug',
            'keywords' => [
                'textInput' => [
                    'options' => ['placeholder' => 'Ex: tag1, tag2, tag3']
                ]
            ],
            'description' => ['textarea'],
            'image' => [
                'mediaUploader'
            ],
            'thumbnail' => [
                'mediaUploader'
            ],
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }


    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'name', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['article-category/create', 'id' => $model->id], 
                'Delete' => ['article-category/delete', 'id' => $model->id] ] );

            $path   = $model->image_dir;
            $full   = $path . $model->image;
            $thumb  = $path . 'thumb_' . $model->image;
            $image  = \backend\components\View::getImage($full, $thumb);
            $data[] = [
                $model->id,
                $image,
                $model->name,
                $model->slug,
                $model->description,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }
}
