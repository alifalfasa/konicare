<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "immunization".
 *
 * @property integer $id
 * @property integer $participant_id
 * @property string $children_name
 * @property string $birthdate
 * @property integer $gender
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Participant $participant
 * @property ImmunizationSchedule[] $immunizationSchedules
 */
class Immunization extends \common\models\Immunization
{


    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Mother',
            'Children Name',
            'Children Gender',
            'Birthdate',
            'Status',
        ];
    } 


    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'children_name', $params['sSearch']])
            ->with('participant')
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $data[] = [
                $model->id,
                $model->participant->fullname,
                $model->children_name,
                $model->getGender(),
                $model->birthdate,
                $model->getStatus(),
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }
}
