<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "forum_topic".
 *
 * @property integer $id
 * @property integer $forum_id
 * @property string $name
 * @property integer $post_count
 * @property integer $view_count
 * @property integer $is_sticky
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ForumPost[] $forumPosts
 * @property Forum $forum
 */
class ForumTopic extends \common\models\ForumTopic
{

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Forum',
            'Name',
            'Post Count',
            'View Count',
            'Status',
            'Action'
        ];
    } 

    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'name', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Enabled' =>  ['forum-topic/enabled',  'id' => $model->id], 
                'Disabled' => ['forum-topic/disabled', 'id' => $model->id], 
                'Delete' =>   ['forum-topic/delete',   'id' => $model->id] ] );
        
            $data[] = [
                $model->id,
                $model->forum->name,
                $model->name,
                number_format($model->post_count),
                number_format($model->view_count),
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }

}
