<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $link
 * @property string $image_dir
 * @property string $image
 * @property string $mobile_image_dir
 * @property string $mobile_image
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Banner extends \common\models\BaseModel
{

    protected static $mainBannerOptions = [ 'image' => 'Image', 'video' => 'Video' ];

    public static $uploadFile = [
        'video' => [
            'path' => 'page-video/',
            'using' => 'manually'
        ],
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile_image', 'mobile_image_dir', 'link', 'image_dir', 'image', 'row_status'], 'required'],
            [['description', 'video'], 'string'],
            ['video', 'required', 'when' => function($model) {
                return $model->main_banner == 'video';
            }, 'whenClient' => "function (attribute, value) {
                return $('#banner-main_banner').val() == 'video';
            }"],
            [['row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['title', 'image_dir', 'image'], 'string', 'max' => 100],
            [['link_caption'], 'string', 'max' => 130],
            [['link'], 'url'],
        ];
    }

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Image Thumbnail',
            'Title',
            'Description',
            'Main Banner',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'title',
            'description' => ['textarea'],
            'link_caption',
            'link',
            'video' => [ 'fileInput' ],
            'image' => [
                'mediaUploader'
            ],
            'mobile_image' => [
                'mediaUploader'
            ],
            'main_banner' => [
                'dropDownList' => [ 'list' => static::$mainBannerOptions ]
            ],
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }


    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'title', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['banner/create', 'id' => $model->id], 
                'Delete' => ['banner/delete', 'id' => $model->id] ] );

            $path   = $model->image_dir;
            $full   = $path . $model->image;
            $thumb  = $path . 'thumb_' . $model->image;
            $image  = \backend\components\View::getImage($full, $thumb);
            $data[] = [
                $model->id,
                $image,
                $model->title,
                $model->description,
                $model->main_banner,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }
}
