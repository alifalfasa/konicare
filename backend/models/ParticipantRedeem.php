<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "participant_redeem".
 *
 * @property integer $id
 * @property integer $participant_id
 * @property integer $product_redeem_id
 * @property string $address
 * @property integer $created_at
 */
class ParticipantRedeem extends \common\models\BaseModel
{
    public $month, $year;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participant_redeem';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ProductRedeem::className(), ['id' => 'product_redeem_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(Participant::className(), ['id' => 'participant_id']);
    }
}
