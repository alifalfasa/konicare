<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "participant".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $usertype
 * @property string $fbuserid
 * @property string $fbusername
 * @property string $fbtoken
 * @property string $email
 * @property string $fullname
 * @property string $gender
 * @property string $birthdate
 * @property string $notelp
 * @property string $nohp
 * @property string $address
 * @property integer $regency_id
 * @property integer $province_id
 * @property string $country
 * @property string $zip
 * @property string $avatarimg
 * @property integer $user_status
 * @property integer $is_notif
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ForumPost[] $forumPosts
 * @property ForumTopic[] $forumTopics
 */
class Participant extends \common\models\Participant
{

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Personal Info',
            'Username',
            'Gender',
            'Age',
            'Status',
            'Action'
        ];
    } 

    /**
     * Gets the data for ajax.
     *
     * @param      array  $params  Parameter from data table
     *
     * @return     json|string
     */
    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'fullname', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Enable' => ['participant/activated', 'id' => $model->id], 
                'Disable' => ['participant/disactivated', 'id' => $model->id] ] );
            
            $personalInfo = '<h4> <a href="' . BASE_URL . 'backend/participant/detail/' . $model->id . '">' . ucwords($model->fullname) . '</a></h4><small>Email: ' . $model->email . ' &nbsp; / &nbsp; Join date: ' . date( 'd/m/Y H:i:s', $model->created_at ) . '</small>';

            $data[] = [
                $model->id,
                $personalInfo,
                $model->username,
                $model->gender,
                date_diff(date_create($model->birthdate), date_create('now'))->y,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }


    /**
     * Gets the total post.
     * Total post berdasrkan post comment dan forum post
     *  
     * @return     integer  The total post.
     */
    public function getTotalPost()
    {
        $commentTotal   = PostComment::get()->andWhere(['participant_id' => $this->id])->count();
        $forumPostTotal = ForumPost::get()->andWhere(['participant_id' => $this->id])->count();

        return $commentTotal + $forumPostTotal;
    }
}
