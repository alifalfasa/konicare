<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_redeem_date".
 *
 * @property integer $id
 * @property date $redeem_date
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class ProductRedeemDate extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_redeem_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redeem_date'], 'required'],
            [['created_by', 'created_at', 'updated_by', 'updated_at', 'row_status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'redeem_date' => 'Redeem Date',
            'row_status' => 'Row Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\backend\models\User::className(), ['id' => 'created_by']);
    }


    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Redeem Date',
            'Created By',
            'Created At',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'redeem_date' =>  [ 'datePicker' ],
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }

    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'redeem_date', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['product-redeem-date/create', 'id' => $model->id], 
                'Delete' => ['product-redeem-date/delete', 'id' => $model->id] ] );
        
            $data[] = [
                $model->id,
                $model->redeem_date,
                $model->user->fullname,
                date('Y-m-d H:i:s', $model->created_at),
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }

}
