<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "immunization_schedule".
 *
 * @property integer $id
 * @property integer $immunization_id
 * @property integer $immunization_category_id
 * @property string $date
 * @property string $clinic_name
 * @property string $note
 * @property integer $had_done
 * @property integer $row_status
 * @property integer $created_at
 *
 * @property Immunization $immunization
 * @property ImmunizationCategory $immunizationCategory
 */
class ImmunizationSchedule extends \common\models\ImmunizationSchedule
{


    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Category',
            'Mother Name',
            'Children Name',
            'Date',
            'Had Done?',
        ];
    } 

    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'immunization.children_name', $params['sSearch']])
            ->joinWith('immunization')
            ->joinWith('participant')
            ->joinWith('immunizationCategory')
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $data[] = [
                $model->id,
                $model->immunizationCategory->name,
                $model->immunization->participant->fullname,
                $model->immunization->children_name,
                $model->date,
                $model->getHadDone(),
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    } 
}
