<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "news_activity".
 *
 * @property integer $id
 * @property string $type
 * @property string $title
 * @property string $slug
 * @property string $keywords
 * @property string $subcontent
 * @property string $content
 * @property string $image
 * @property string $image_dir
 * @property integer $views_count
 * @property integer $shares_count
 * @property integer $likes_count
 * @property integer $comments_count
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class NewsActivity extends \common\models\BaseModel
{

    static $type_options = [ 'news' => 'News', 'activity' => 'Activity'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'title', 'slug', 'keywords', 'subcontent', 'image', 'image_dir', 'thumbnail', 'thumbnail_dir', 'row_status'], 'required'],
            [['type', 'content'], 'string'],
            ['slug', 'uniquenessValidation'],
            [['views_count', 'shares_count', 'likes_count', 'comments_count', 'row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 100],
            [['keywords', 'subcontent'], 'string', 'max' => 150],
            [['image', 'image_dir'], 'string', 'max' => 120],
        ];
    }

    public function getType()
    {
        return static::$type_options[$this->type];
    }

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Image',
            'Title',
            'Type',
            'Slug',
            'Subcontent',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'type' => [
                'dropDownList' => [
                    'list' => static::$type_options
                ]
            ],
            'title' => [
                'textInput' => [
                    'options' => [
                        'class' => 'form-control autoslug', 
                        'slug-target' => '#newsactivity-slug'
                    ],
                ]
            ],
            'slug',
            'keywords' => [
                'textInput' => [
                    'options' => ['placeholder' => 'Ex: tag1, tag2, tag3']
                ]
            ],
            'subcontent',
            'content' => ['textarea' => ['options' => ['class' => 'form-controller ckeditor']]],
            'image' => [
                'mediaUploader'
            ],
            'thumbnail' => [
                'mediaUploader'
            ],
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }


    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'title', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['news-activity/create', 'id' => $model->id], 
                'Delete' => ['news-activity/delete', 'id' => $model->id] ] );

            $path   = $model->image_dir;
            $full   = $path . $model->image;
            $thumb  = $path . 'thumb_' . $model->image;
            $image  = \backend\components\View::getImage($full, $thumb);
            $data[] = [
                $model->id,
                $image,
                $model->title,
                $model->getType(),
                $model->slug,
                $model->subcontent,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }

}
