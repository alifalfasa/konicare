<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_redeem".
 *
 * @property integer $id
 * @property string $product_name
 * @property integer $volume
 * @property integer $price
 * @property integer $stock
 * @property integer $point
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ProductRedeem extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_redeem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name', 'price', 'stock', 'point'], 'required'],
            [['volume', 'price', 'stock', 'point', 'created_at', 'created_by', 'updated_at', 'updated_by', 'row_status'], 'integer'],
            [['product_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_name' => 'Product Name',
            'volume' => 'Volume (ml)',
            'price' => 'Price',
            'stock' => 'Stock',
            'point' => 'Point',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }


    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Product',
            'Price',
            'Stock',
            'Point',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'product_name',
            'volume',
            'price',
            'stock',
            'point',
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }


    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'product_name', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['product-redeem/create', 'id' => $model->id], 
                'Delete' => ['product-redeem/delete', 'id' => $model->id] ] );

            $volume = !empty($model->volume) ? $model->volume . 'ml' : null;
        
            $data[] = [
                $model->id,
                $model->product_name . ' ' . $volume,
                $model->price,
                $model->stock,
                $model->point,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }
}
