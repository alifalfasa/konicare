<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "immunization_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ImmunizationSchedule[] $immunizationSchedules
 */
class ImmunizationCategory extends \common\models\BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'immunization_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'row_status'], 'required'],
            [['description', 'subcontent'], 'string'],
            [['row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 55],
        ];
    }


    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Name',
            'Description',
            'Status',
            'Action'
        ];
    } 


    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'name',
            'subcontent',
            'description' => ['textarea'],
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }


    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'name', $params['sSearch']])
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['immunization-category/create', 'id' => $model->id] ] );

            $data[] = [
                $model->id,
                $model->name,
                $model->description,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }

}
