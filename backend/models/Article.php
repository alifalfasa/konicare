<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $article_category_id
 * @property string $title
 * @property string $slug
 * @property string $keywords
 * @property string $subcontent
 * @property string $content
 * @property string $image
 * @property string $image_dir
 * @property integer $views_count
 * @property integer $likes_count
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property ArticleCategory $articleCategory
 */
class Article extends \common\models\Article
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_category_id', 'title', 'slug', 'keywords', 'subcontent', 'image', 'image_dir', 'row_status'], 'required'],
            [['article_category_id', 'views_count', 'likes_count', 'shares_count', 'comments_count', 'row_status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['content'], 'string'],
            ['slug', 'slugValidation'],
            ['slug', 'uniquenessValidation'],
            [['title', 'slug'], 'string', 'max' => 100],
            [['keywords', 'subcontent'], 'string', 'max' => 150],
            [['image', 'image_dir'], 'string', 'max' => 120],
            [['article_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['article_category_id' => 'id']],
        ];
    }


    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Image',
            'Category',
            'Title',
            'Slug',
            'Subcontent',
            'Status',
            'Action'
        ];
    } 

    /**
     * Data fields of the form
     *
     * @return     array  ( description of the return value )
     */
    public static function formData()
    {
        return [
            'id',
            'article_category_id' => [
                'dropDownList' => [
                    'list' => ArticleCategory::dataOptions('id','name')
                ]
            ],
            'title' => [
                'textInput' => [
                    'options' => [
                        'class' => 'form-control autoslug', 
                        'slug-target' => '#article-slug'
                    ],
                ]
            ],
            'slug',
            'keywords' => [
                'textInput' => [
                    'options' => ['placeholder' => 'Ex: tag1, tag2, tag3']
                ]
            ],
            'subcontent',
            'content' => ['textarea' => ['options' => ['class' => 'form-controller ckeditor']]],
            'image' => [
                'mediaUploader'
            ],
            'row_status' => [
                'dropDownList' => [ 'list' => static::$getStatus ]
            ]
        ];
    }


    public static function getDataForAjax($params)
    {
        $whereCondition = ['LIKE', 'title', $params['sSearch']];

        $query = static::fetch()
            ->andWhere( $whereCondition )
            ->joinWith('category')
            ->orderBy( 'id DESC' );

        $totalRecords = (new yii\db\Query())
            ->from('article')
            ->where($whereCondition)
            ->count();

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $totalRecords;
        $result[ 'iTotalRecords' ] = $totalRecords;
        $result[ 'iTotalDisplayRecords' ] = $totalRecords;

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Update' => ['article/create', 'id' => $model->id], 
                'Delete' => ['article/delete', 'id' => $model->id] ] );

            $path   = $model->image_dir;
            $full   = $path . $model->image;
            $thumb  = $path . 'thumb_' . $model->image;
            $image  = \backend\components\View::getImage($full, $thumb);
            $data[] = [
                $model->id,
                $image,
                $model->category->name,
                $model->title,
                $model->slug,
                $model->subcontent,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }

}
