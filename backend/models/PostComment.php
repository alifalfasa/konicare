<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "post_comment".
 *
 * @property integer $id
 * @property integer $participant_id
 * @property integer $post_id
 * @property integer $type
 * @property string $content
 * @property integer $total_likes
 * @property integer $row_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Participant $participant
 */
class PostComment extends \common\models\PostComment
{
    

    /**
     * the Header for data table.
     *
     * @return     array  The header.
     */
    public static function getHeader()
    {
        return [
            'Id',
            'Type',
            'Participant',
            'Comment',
            'Likes',
            'Status',
            'Action'
        ];
    } 


    /**
     * Gets the data for ajax.
     *
     * @param      array  $params  Parameter from data table
     *
     * @return     json|string
     */
    public static function getDataForAjax($params)
    {
        $query = static::fetch()
            ->andWhere(['LIKE', 'content', $params['sSearch']])
            ->with('participant')
            ->orderBy( 'id DESC' );
        $countQuery = clone $query;

        $query = $query->offset($params['iDisplayStart'])
                       ->limit($params['iDisplayLength']);

        $result[ 'aEcho' ] = $params['sEcho'];
        $result[ 'total' ] = $countQuery->count();
        $result[ 'iTotalRecords' ] = $countQuery->count();
        $result[ 'iTotalDisplayRecords' ] = $countQuery->count();

        $data = [];
        
        foreach ($query->all() as $model) {

            $action = \backend\components\View::groupButton( [
                'Enabled' => ['post-comment/enabled', 'id' => $model->id],
                'Disabled' => ['post-comment/disabled', 'id' => $model->id],
            ]);

            $data[] = [
                $model->id,
                $model->getType(),
                $model->participant->fullname,
                substr(trim($model->content), 0, 120).' ...',
                $model->total_likes,
                $model->getStatus(),
                $action                
            ];
            

        }
        $result[ 'aaData' ] = $data;
        return json_encode($result);
    }

}
