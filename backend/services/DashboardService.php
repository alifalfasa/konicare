<?php

namespace backend\services;

use Yii;
use common\models\Visitor;
use common\models\LogActivity as Activity;
use backend\models\Participant;
use backend\models\ForumTopic;
use backend\models\ForumPost;
use backend\models\MediaFile;
use backend\models\Message;

use Yii\helpers\ArrayHelper;

class DashboardService {

	public function result()
	{
		$participantTotal = Participant::find()->count();
		$forumTopicTotal  = ForumTopic::find()->count();
		$postTotal  	  = ForumPost::find()->count();
		$messageTotal  	  = Message::find()->count();

		$result = [
			'participantTotal'  => $participantTotal,
			'forumTopicTotal'   => $forumTopicTotal,
			'postTotal'   		=> $postTotal,
			'messageTotal'	   	=> $messageTotal,
			'galleries'   		=> $this->galleries(),
			'visitorGraph'  	=> $this->visitorGraph(),
			'isOnline' 			=> $this->isOnline(),
			'registerToday' 	=> $this->registerToday(),
			'activityToday' 	=> $this->activityToday(),
			'lastParticipantPost' => $this->lastParticipantPost(),
		];

		return $result;
	}	

	private function visitorGraph()
	{
		$visitors = Visitor::find()
			->select(['COUNT(*) AS count', 'date'])
			->groupBy(['date'])
			->limit(7)
			->orderBy('date dESC')
			->asArray()
			->all();
		$visitors = ArrayHelper::index($visitors, 'date');
		// var_dump($visitors);

		$participants = Participant::find()
			->select(['COUNT(*) AS count', "from_unixtime(created_at,'%Y-%m-%d') as date"])
			->groupBy(['date'])
			->limit(7)
			->orderBy('date dESC')
			->asArray()
			->all();

		$participants = ArrayHelper::index($participants, 'date');
		// var_dump($participants);exit;
		
		$activities = Activity::find()
			->select(['COUNT(*) AS count', "from_unixtime(created_at,'%Y-%m-%d') as date"])
			->groupBy(['date'])
			->limit(7)
			->orderBy('date dESC')
			->asArray()
			->all();

		$activities = ArrayHelper::index($activities, 'date');
		// var_dump($activities);exit;

		$count = 0;
		$result = [];
		while( $count < 7 )
		{
			$time = strtotime('-' . $count . ' days');
			$date = date('Y-m-d', $time);
			$result[] = [
				'time' => $time ,
				'date' => date('d F', $time),
				'visitorCount' => array_key_exists( $date, $visitors ) ? $visitors[$date]['count'] : 0,
				'participantCount' => array_key_exists( $date, $participants ) ? $participants[$date]['count'] : 0,
				'activityCount' => array_key_exists( $date, $activities ) ? $activities[$date]['count'] : 0,
			];

			++$count;
		}

		ArrayHelper::multisort($result, ['time', 'count'], [SORT_ASC, SORT_DESC]);
		return json_encode($result);
	}

	private function isOnline() 
	{ 
		return Visitor::find()->where(['>', 'previous_visit', strtotime('-1 minutes')])->count();
	}

	private function registerToday() 
	{
		return Participant::find()
		->where(['=', "from_unixtime(created_at,'%Y-%m-%d')", date('Y-m-d')])
		->count();
	}
	
	private function activityToday() 
	{
		return Activity::find()
			->where(['=', "from_unixtime(created_at,'%Y-%m-%d')", date('Y-m-d')])
			->count();
	}

	private function galleries()
	{
		$files 	= MediaFile::fetch()->with('folder')->limit(5)->all();
		$result = [];
		foreach ( $files as $file ) {

			$dir = BASE_URL . '/' . $file->folder->directory . $file->id . '/';
			$result[] = [
				'id' => $file->id,
				'name' => $file->name,
				'thumbnail' => $dir . 'normal_' . $file->name,
			];
		}
		return $result;
	}

	private function lastParticipantPost()
	{
		$post = ForumPost::fetch()->joinWith(['participant', 'topic'], null, 'INNER JOIN')->orderBy('id DESC')->limit(5)->all();
		return $post;
	}
}