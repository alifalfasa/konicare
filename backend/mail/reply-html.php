
<!-- Email Header : END -->

<!-- Email Body : BEGIN -->
<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">

<!-- Hero Image, Flush : BEGIN -->
<tr>
    <td bgcolor="#ffffff" style="padding: 10px 20px 0; font-family: sans-serif; font-size: 12px; ">
        <p>Dari: <?= $message->name ?> / <?= $message->name ?></p>
        <p>Tanggal & Jam: <?= date('d M Y H:i:s', $message->created_at) ?></p>
    </td>
</tr>
<!-- Hero Image, Flush : END -->
<tr>
    <td bgcolor="#ffffff" >
        <hr>
    </td>
</tr>
<!-- 1 Column Text + Button : BEGIN -->
<tr>
    <td bgcolor="#ffffff" style="padding: 40px 40px 20px; text-align: center;">
        <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #333333; font-weight: normal;">"<?= $message->subject ?>"</h1>
    </td>
</tr>
<tr>
    <td bgcolor="#ffffff" style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;">
        <p style="margin: 0;"><?= $message->body ?></p>
    </td>
</tr>
<!-- 1 Column Text + Button : END -->

<!-- Background Image with Text : BEGIN -->
<tr>
    <!-- Bulletproof Background Images c/o https://backgrounds.cm -->
    <td  bgcolor="#eee" valign="middle" style="text-align: center; background-position: center center !important; background-size: cover !important;">

        <!--[if gte mso 9]>
        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;height:175px; background-position: center center !important;">
        <v:fill type="tile" src="http://placehold.it/600x230/222222/666666" color="#222222" />
        <v:textbox inset="0,0,0,0">
        <![endif]-->
        <div>
            <table role="presentation" aria-hidden="true" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="middle" style="text-align: center; padding: 40px 0; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #666;">
                        <h1 style="margin: 0; text-align: right;">Balasan</h1>
                    </td>
                </tr>
            </table>
            </div>
        <!--[if gte mso 9]>
        </v:textbox>
        </v:rect>
        <![endif]-->
    </td>
</tr>
<!-- Background Image with Text : END -->

<?php foreach( $message->messageReplies as $reply ): ?>
    <tr style="background: #fff">
        <td align="right" valign="center" style="padding: 20px; font-family: sans-serif; font-size: 15px;">
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%" style="text-align: right">
                <tr>
                    <td style="text-align: left;font-size: 12px;color: #666;" valign="top"><?= date('d M Y H:i A', $reply->created_at ) ?></td>
                    <td style="width: 60%;">
                        <?= $reply->user->fullname ?>
                    </td>
                    <td style="width: 10%;">
                        <img src="<?= $reply->user->getAvatar() ?>" width="50em">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="background: #fff">    
        <td><hr></td>
    </tr>
    <tr style="background: #fff">    
        <td style="padding: 20px; font-family: sans-serif; font-size: 15px;border-bottom: 20px solid #eee; color:#555" valign="center">
            <?= $reply->body ?>
        </td>
    </tr>
<?php endforeach; ?>