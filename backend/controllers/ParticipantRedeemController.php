<?php
namespace backend\controllers;

use Yii;
use backend\models\ParticipantRedeem;
use common\models\LogActivity;

/**
 * ParticipantRedeem controller
 */
class ParticipantRedeemController extends BaseController
{

    public $title = 'Participant Redeem';
    public $menu  = 'participant';
    public $code  = 'participant';
    public $menuChild  = 'participant-redeem';
    public $description = '';
    
    public function actionIndex()
    {
        $model = new ParticipantRedeem();
        $month = [
            '01' => "January", 
            '02' => "February", 
            '03' => "March", 
            '04' => "April", 
            '05' => "May", 
            '06' => "June", 
            '07' => "July", 
            '08' => "August", 
            '09' => "September", 
            '10' => "October", 
            '11' => "November", 
            '12' => "December"
        ];
        $year = [];
        for( $i = 2015; $i <= date('Y'); $i++ ) $year[ $i ] = $i;



        $post = Yii::$app->request->post();
        $histories = [];
        if ( !empty($post) )
        {
            $data      = $post['ParticipantRedeem'];
            $condition = $data['year'] . '-' . $data['month'];
            $histories = ParticipantRedeem::find()
                ->joinWith(['participant', 'product'])
                ->andWhere(['=', 'FROM_UNIXTIME(participant_redeem.created_at,\'%Y-%m\')', $condition])
                ->orderBy( 'id ASC' )
                ->all();

            $model->month   = $data['month'];
            $model->year    = $data['year'];
        }


        return $this->render('index.twig', [ 
            'histories' => $histories,
            'model' => $model,
            'month' => $month, 
            'year' => $year 
        ]);
    }

    public function actionDetail($id)
    {

        $model = ParticipantRedeem::find()
            ->andWhere(['=', 'participant.id', $id])
            ->joinWith('province')
            ->joinWith('regency')
            ->one();

        if ( empty($model) ) throw new \yii\web\HttpException(404, 'Page is Not found');

        $activities = LogActivity::find()
            ->where(['=', 'participant_id', $id])
            ->orderBy('id DESC')
            ->all();

        $redeemHistories = ParticipantRedeemRedeem::find()
            ->andWhere(['participant_id' => $id])
            ->joinWith(['product'])
            ->all();

        $regency  = !empty($model->regency) ? $model->regency->name : null;
        $province = !empty($model->province) ? $model->province->name : null;

        $result = [
            'id' => $model->id,
            'status' => $model->row_status,
            'redeemHistories' => $redeemHistories,
            'activities' => $activities,
            'fullname' => $model->fullname,
            'username' => $model->username,
            'point'    => $model->points,
            'totalActivity'    => count($activities),
            'totalPost'    => $model->getTotalPost(),
            'birthdate' => $model->birthdate,
            'gender' => $model->gender,
            'email' => $model->email,
            'phone1' => $model->nohp,
            'phone2' => $model->notelp,
            'avatar' => $model->getAvatar('normal_'),
            'location' => $model->address . ', ' . ucwords(strtolower($regency)) . ', ' . ucwords(strtolower($province)) . ', Indonesia, ' . $model->zip,
        ];
        return $this->render('detail.twig', $result);
    }

    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return Participant::getDataForAjax(Yii::$app->request->get());
    }

}
