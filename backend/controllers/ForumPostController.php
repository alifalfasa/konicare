<?php
namespace backend\controllers;

use Yii;
use backend\models\ForumPost;

/**
 * ForumPost controller
 */
class ForumPostController extends BaseController
{

    public $title = 'Forum Post';
    public $menu  = 'forum';
    public $code  = 'forum-post';
    public $menuChild  = 'forum-post';
    public $description = 'Forum Post of CMS';
    
    public function actionIndex()
    {
    	return $this->render('/templates/ajax-list.twig', [ 'headers' => ForumPost::getHeader() ]);
    }

    public function actionDelete($id)
    {

        $model = ForumPost::deleteData(new ForumPost(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_DELETE_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }
        return $this->goBack();
    }
    
    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return ForumPost::getDataForAjax(Yii::$app->request->get());
    }

}