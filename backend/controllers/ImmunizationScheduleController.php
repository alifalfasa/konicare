<?php
namespace backend\controllers;

use Yii;
use backend\models\ImmunizationSchedule;

/**
 * ImmunizationSchedule controller
 */
class ImmunizationScheduleController extends BaseController
{

    public $title = 'Immunization Schedule of Childrens';
    public $menu  = 'immunization';
    public $menuChild  = 'immunization-schedule';
    public $description = 'Schedule of Children immunization';
    
    public function actionIndex()
    {
    	return $this->render('/templates/ajax-list.twig', [ 'headers' => ImmunizationSchedule::getHeader(), 'disabledInsertNewItem' => true ]);
    }

    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return ImmunizationSchedule::getDataForAjax(Yii::$app->request->get());
    }

}