<?php
namespace backend\controllers;

use Yii;

use backend\models\Analytic,
    common\models\LogActivity,
	common\models\Visitor;

use Yii\helpers\ArrayHelper;

/**
 * Analytic controller
 */
class AnalyticController extends BaseController
{

    public $title = 'Analytic';
    public $menu  = 'analytic';
    public $menuChild  = 'analytic';
    public $description = 'Analytic on Website';
    
    public function actionIndex()
    {
    	$model = new Analytic();

        $activities     = $this->_activityVariant();
    	$visitors       = $this->_visitorAll();
    	$visitorDevices = $this->_visitorVariants();

    	$req = Yii::$app->request;
    	if ( $req->isPost )
    	{
    		$post = $req->post('Analytic');

    		$startDate = new \DateTime($post['startDate']);
    		$endDate   = new \DateTime($post['endDate']);
    		$dateDiff  = $endDate->diff($startDate)->format("%a");
    		$operator  = '+';

    	} else {

    		$startDate = date_create(date('Y-m-d'));
    		$endDate   = date_create(date('Y-m-d'));
    		$dateDiff  = 7;
    		$operator  = '-';
    	}

		$model->startDate = $startDate->format('Y-m-d');
		$model->endDate   = $endDate->format('Y-m-d');

		$count    = 0;
		$result   = [];
		while( $count <= $dateDiff )
		{
			$d = new \DateTime( $startDate->format('Y-m-d') );
			$d->modify( $operator . $count . ' days');
			$date = $d->format('Y-m-d');

            $result['visitors'][] = [
                'date' => $date,
                'visitorCount' => array_key_exists( $date, $visitors ) ? $visitors[$date]['count'] : 0,
            ];

			$result['activities'][] = [
				'date' => $date,
                'activityReadCount' => array_key_exists( $date, $activities['activityRead'] ) ? $activities['activityRead'][$date]['count'] : 0,
                'activityCommentCount' => array_key_exists( $date, $activities['activityComment'] ) ? $activities['activityComment'][$date]['count'] : 0,
				'activityLikeCount' => array_key_exists( $date, $activities['activityLike'] ) ? $activities['activityLike'][$date]['count'] : 0,
			];

			++$count;
		}

        ArrayHelper::multisort( $result['visitors'],   ['date', 'count'], [SORT_ASC, SORT_DESC] );
		ArrayHelper::multisort( $result['activities'], ['date', 'count'], [SORT_ASC, SORT_DESC] );
		
		// var_dump($result['visitorDevices']);
		// var_dump($startDate);
		// var_dump($endDate);
		// var_dump($dateDiff);
		// exit;

    	return $this->render( 'index.twig', [ 
    		'model' => $model, 
    		'activityGraph' => json_encode($result['activities']),
            'visitorGraph' => json_encode($result['visitors']),
    		'visitorDevicesGraph' => json_encode($visitorDevices),
    	] );
    }

    private function _activity()
    {
        $post = Yii::$app->request->post('Analytic');
        $activities = LogActivity::find()
            ->select(['COUNT(*) AS count', "from_unixtime(created_at,'%Y-%m-%d') as date"])
            ->groupBy(['date'])
            ->orderBy('date dESC');
        if ( $post != null )
        {   
            $activities = $activities
                ->where( ['BETWEEN', "from_unixtime(created_at,'%Y-%m-%d')", $post['startDate'], $post['endDate']] );
        }
        return $activities;

    }

    private function _activityVariant()
    {
        $activity = $this->_activity();

        $activityRead    = ArrayHelper::index( $activity->andWhere([ 'action' => 'read'])->asArray()->all(),    'date' );
        $activityComment = ArrayHelper::index( $activity->andWhere([ 'action' => 'comment'])->asArray()->all(), 'date' );
        $activityLike    = ArrayHelper::index( $activity->andWhere([ 'action' => 'like'])->asArray()->all(),    'date' );

        return [
            'activityRead' => $activityRead,
            'activityComment' => $activityComment,
            'activityLike' => $activityLike,
        ];

    }

    private function _visitorAll()
    {
        $post = Yii::$app->request->post('Analytic');
        $visitors = Visitor::find()
            ->select(['COUNT(*) AS count', 'date'])
            ->groupBy(['date'])
            ->orderBy('date dESC');
        if ( $post != null )
        {   
            $visitors = $visitors
                ->where( ['BETWEEN', 'date', $post['startDate'], $post['endDate']] );
        }
		$visitors = ArrayHelper::index($visitors->asArray()->all(), 'date');
		return $visitors;
    }

    private function _visitorVariants()
    {
    	$visitors = Visitor::find();

        $visitorCount = $visitors->andWhere([ '=', 'updated_by', null])->count();
        $participantCount = $visitors->andWhere([ '>', 'updated_by', 0])->count();
    	$mobileCount = $visitors->andWhere(['via' => 'mobile'])->count();
    	$webCount    = $visitors->andWhere(['via' => 'web'])->count();

    	return [
            [ 'label' => 'Participant', 'value' => $visitorCount ],
            [ 'label' => 'Visitor', 'value' => $participantCount ],
    		[ 'label' => 'Mobile', 'value' => $mobileCount ],
    		[ 'label' => 'Website', 'value' => $webCount ]
    	];
    }

}