<?php
namespace backend\controllers;

use Yii;
use backend\models\Participant;
use backend\models\ParticipantRedeem;
use common\models\LogActivity;

/**
 * Participant controller
 */
class ParticipantController extends BaseController
{

    public $title = 'Participant';
    public $menu  = 'participant';
    public $code  = 'participant';
    public $menuChild  = 'participant';
    public $description = '';
    
    public function actionIndex()
    {
        return $this->render('/templates/ajax-list.twig', [ 'headers' => Participant::getHeader(), 'disabledInsertNewItem' => true ]);
    }

    public function actionDetail($id)
    {

        $model = Participant::find()
            ->andWhere(['=', 'participant.id', $id])
            ->joinWith('province')
            ->joinWith('regency')
            ->one();

        if ( empty($model) ) throw new \yii\web\HttpException(404, 'Page is Not found');

        $activities = LogActivity::find()
            ->where(['=', 'participant_id', $id])
            ->orderBy('id DESC')
            ->all();

        $redeemHistories = ParticipantRedeem::find()
            ->andWhere(['participant_id' => $id])
            ->joinWith(['product'])
            ->all();

        $regency  = !empty($model->regency) ? $model->regency->name : null;
        $province = !empty($model->province) ? $model->province->name : null;

        $result = [
            'id' => $model->id,
            'status' => $model->row_status,
            'redeemHistories' => $redeemHistories,
            'activities' => $activities,
            'fullname' => $model->fullname,
            'username' => $model->username,
            'point'    => $model->points,
            'totalActivity'    => count($activities),
            'totalPost'    => $model->getTotalPost(),
            'birthdate' => $model->birthdate,
            'gender' => $model->gender,
            'email' => $model->email,
            'phone1' => $model->nohp,
            'phone2' => $model->notelp,
            'avatar' => $model->getAvatar('normal_'),
            'location' => $model->address . ', ' . ucwords(strtolower($regency)) . ', ' . ucwords(strtolower($province)) . ', Indonesia, ' . $model->zip,
        ];
        return $this->render('detail.twig', $result);
    }

    public function actionDelete($id)
    {

        $model = Participant::deleteData(new Participant(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_DELETE_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }
        return $this->redirect([ Yii::$app->controller->id . '/index']);
    }
    
    public function actionActivated($id)
    {
        $model = Participant::findOne($id);

        if ( empty($model) ) throw new \yii\web\HttpException(404, 'Page is Not found');

        $data['Participant']['row_status'] = 1;
        
        Participant::saveData( $model, $data );
        
        $this->session->setFlash('success', MSG_DATA_EDIT_SUCCESS);

        return $this->redirect([ Yii::$app->controller->id . '/detail/' . $id]);
    }
    
    
    public function actionDisactivated($id)
    {
        $model = Participant::findOne($id);

        if ( empty($model) ) throw new \yii\web\HttpException(404, 'Page is Not found');

        $data['Participant']['row_status'] = 0;
        
        Participant::saveData( $model, $data );
        
        $this->session->setFlash('success', MSG_DATA_EDIT_SUCCESS);

        return $this->redirect([ Yii::$app->controller->id . '/detail/' . $id]);
    }
    
    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
        return Participant::getDataForAjax(Yii::$app->request->get());
    }

}
