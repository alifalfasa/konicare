<?php
namespace backend\controllers;

use Yii;
use backend\models\ForumTopic;

/**
 * ForumTopic controller
 */
class ForumTopicController extends BaseController
{

    public $title = 'Forum Topic';
    public $menu  = 'forum';
    public $code  = 'forum-topic';
    public $menuChild  = 'forum-topic';
    public $description = 'Forum Topic of CMS';
    
    public function actionIndex()
    {
    	return $this->render('/templates/ajax-list.twig', [ 'headers' => ForumTopic::getHeader(), 'disabledInsertNewItem' => true ]);
    }

    public function actionDelete($id)
    {

        $model = ForumTopic::deleteData(new ForumTopic(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_DELETE_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }
        return $this->redirect([ Yii::$app->controller->id . '/index']);
    }
    
    public function actionEnabled($id)
    {

        $model = ForumTopic::enabledRow(new ForumTopic(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_UPDATE_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }
        return $this->redirect([ Yii::$app->controller->id . '/index']);
    }    

    public function actionDisabled($id)
    {

        $model = ForumTopic::disabledRow(new ForumTopic(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_UPDATE_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }
        return $this->redirect([ Yii::$app->controller->id . '/index']);
    }
    
    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return ForumTopic::getDataForAjax(Yii::$app->request->get());
    }

}