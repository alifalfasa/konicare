<?php
namespace backend\controllers;

use Yii;
use backend\models\PostComment;

/**
 * PostComment controller
 */
class PostCommentController extends BaseController
{

    public $title = 'Post Comment';
    public $menu  = 'participant';
    public $code  = 'post-comment';
    public $menuChild  = 'post-comment';
    public $description = 'Post Comment of participants';
    
    public function actionIndex()
    {
    	return $this->render('/templates/ajax-list.twig', [ 'headers' => PostComment::getHeader(), 'disabledInsertNewItem' => true ]);
    }

    public function actionEnabled($id)
    {

        $model = PostComment::enabledRow(new PostComment(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_EDIT_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }

        return $this->redirect([ Yii::$app->controller->id . '/index']);
        
    }


    public function actionDisabled($id)
    {

        $model = PostComment::disabledRow(new PostComment(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_EDIT_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }

        return $this->redirect([ Yii::$app->controller->id . '/index']);

    }
    

    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return PostComment::getDataForAjax(Yii::$app->request->get());
    }

}