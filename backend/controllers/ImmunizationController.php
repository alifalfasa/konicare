<?php
namespace backend\controllers;

use Yii;
use backend\models\Immunization;

/**
 * Immunization controller
 */
class ImmunizationController extends BaseController
{

    public $title = 'Immunization Children';
    public $menu  = 'immunization';
    public $menuChild  = 'immunization-children';
    public $description = 'Children of immunization';
    
    public function actionIndex()
    {
    	return $this->render('/templates/ajax-list.twig', [ 'headers' => Immunization::getHeader(), 'disabledInsertNewItem' => true ]);
    }

    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return Immunization::getDataForAjax(Yii::$app->request->get());
    }

}