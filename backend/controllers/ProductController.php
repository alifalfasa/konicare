<?php
namespace backend\controllers;

use Yii;
use backend\models\Product;

/**
 * Product controller
 */
class ProductController extends BaseController
{

    public $title = 'Product';
    public $menu  = 'product';
    public $menuChild  = 'product-list';
    public $description = 'Product Konicare';
    
    public function actionIndex()
    {
    	return $this->render('/templates/ajax-list.twig', [ 'headers' => Product::getHeader() ]);
    }

    /**
     * [actionCreate] 
     * Function ini bisa digunakan untuk CREATE and UPDATE
     * 
     * @param  integer $id [Ini sebagai paramerter data tersebut baru atau tidak, 
     *                      bila baru maka tidak perlu di isi dan data akan di CREATE/INSERT]
     */
    public function actionCreate( $id = null )
    {
        if ($id == null){

            $model = new Product();
            $model->scenario = 'create';
        } else {

            $model = Product::findOne($id);
            if ( empty( $model ) ) throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);

        }

        if ( Yii::$app->request->post() )
        {
            $saveModel = Product::saveData($model, Yii::$app->request->post());
            if ( $saveModel[ 'status' ] == true )
            {
                if ( $model->highlight_status == 1 )
                {
                    Product::updateAll(['highlight_status' => 0], 'id != ' . $model->id);
                }
                $this->session->setFlash('success', MSG_DATA_SAVE_SUCCESS);
                return $this->redirect([ Yii::$app->controller->id . '/index']);

            } else {
                // var_dump($saveModel['message']);exit;
                $this->session->setFlash('danger', $saveModel['message']);
            }
        }
        return $this->render( '/templates/form.twig', [ 'model' => $model, 'fields' => Product::formData() ] );
    }

    public function actionDelete($id)
    {

        $model = Product::deleteData(new Product(), $id, true);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_DELETE_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }
        return $this->redirect([ Yii::$app->controller->id . '/index']);
    }
    
    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return Product::getDataForAjax(Yii::$app->request->get());
    }

}