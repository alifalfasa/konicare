<?php
namespace backend\controllers;

use Yii;
use backend\models\ImmunizationCategory;

/**
 * ImmunizationCategory controller
 */
class ImmunizationCategoryController extends BaseController
{

    public $title = 'Immunization Category';
    public $menu  = 'immunization';
    public $menuChild  = 'immunization-category';
    public $description = 'Category of immunization';
    
    public function actionIndex()
    {
    	return $this->render('/templates/ajax-list.twig', [ 'headers' => ImmunizationCategory::getHeader(), 'disabledInsertNewItem' => true ]);
    }

    /**
     * [actionCreate] 
     * Function ini bisa digunakan untuk CREATE and UPDATE
     * 
     * @param  integer $id [Ini sebagai paramerter data tersebut baru atau tidak, 
     *                      bila baru maka tidak perlu di isi dan data akan di CREATE/INSERT]
     */
    public function actionCreate( $id = null )
    {
        if ($id == null){

            $model = new ImmunizationCategory();

        } else {

            $model = ImmunizationCategory::findOne($id);
            if ( empty( $model ) ) throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);

        }

        if ( Yii::$app->request->post() )
        {
            $saveModel = ImmunizationCategory::saveData($model, Yii::$app->request->post());
            if ( $saveModel[ 'status' ] == true )
            {
                
                $this->session->setFlash('success', MSG_DATA_SAVE_SUCCESS);
                return $this->redirect([ Yii::$app->controller->id . '/index']);

            } else {
                // var_dump($saveModel['message']);exit;
                $this->session->setFlash('danger', $saveModel['message']);
            }
        }
        return $this->render( '/templates/form.twig', [ 'model' => $model, 'fields' => ImmunizationCategory::formData() ] );
    }
    
    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return ImmunizationCategory::getDataForAjax(Yii::$app->request->get());
    }

}