<?php
namespace backend\controllers;

use Yii;
use backend\models\Forum;

/**
 * Forum controller
 */
class ForumController extends BaseController
{

    public $title = 'Forum';
    public $menu  = 'forum';
    public $code  = 'forum-group';
    public $menuChild  = 'forum';
    public $description = 'Forum Konicare';
    
    public function actionIndex()
    {

    	return $this->render('/templates/ajax-list.twig', [ 'headers' => Forum::getHeader() ]);
    }

    /**
     * [actionCreate] 
     * Function ini bisa digunakan untuk CREATE and UPDATE
     * 
     * @param  integer $id [Ini sebagai paramerter data tersebut baru atau tidak, 
     *                      bila baru maka tidak perlu di isi dan data akan di CREATE/INSERT]
     */
    public function actionCreate( $id = null )
    {
        if ($id == null){

            $model = new Forum();

        } else {

            $model = Forum::findOne($id);
            if ( empty( $model ) ) throw new \yii\web\HttpException(404, MSG_DATA_NOT_FOUND);

        }

        if ( Yii::$app->request->post() )
        {
            $saveModel = Forum::saveData($model, Yii::$app->request->post());
            if ( $saveModel[ 'status' ] == true )
            {
                
                $this->session->setFlash('success', MSG_DATA_SAVE_SUCCESS);
                return $this->redirect([ Yii::$app->controller->id . '/index']);

            } else {
                // var_dump($saveModel['message']);exit;
                $this->session->setFlash('danger', $saveModel['message']);
            }
        }
        return $this->render( '/templates/form.twig', [ 'model' => $model, 'fields' => Forum::formData() ] );
    }

    public function actionDelete($id)
    {

        $model = Forum::deleteData(new Forum(), $id);

        if ( $model['status'] == true  )
        {
            $this->session->setFlash('success', MSG_DATA_DELETE_SUCCESS);
        } else {
            $this->session->setFlash('danger', $model['message']);
        }
        return $this->redirect([ Yii::$app->controller->id . '/index']);
    }
    
    /**
     * listOfData function adalah sebuah mandatori untuk 
     * membuat data table dengan serverside
     * 
     * @param HTTP Get
     * 
     * @return     json
     */
    public function actionListOfData()
    {
    	return Forum::getDataForAjax(Yii::$app->request->get());
    }

}